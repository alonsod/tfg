
Bitmex exchange interface
Miguel, June 2018


#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789



In mbot, margin_balance and check_margins are the two balance
related methods that all exchange objects must include.

But bitmex exchange object includes also wallet_balance() method,
it is explained in the source, at method definition.

-- API rest --

As any HTTP REST interface, there are 6 items to deal with:

- The verb: GET, PUT, POST,etc
- The base URL. ie   https://www.bitmex.com
- The path. e.g.   /api/v1/instrument
  Must include a leading slash
- The parameters. aka payload. They are concatenated to the base URL 
  and the path,  e.g.    ?key1=value1&key2=value2
- In some verbs (PUT, POST...) the data, JSON coded.
- The http headers


We are using requests library for python, it is almost a standard library.
The use of verb, base url, path, parameters and data is straightforward.

For the authentitacion used in Bitmex we must compose a message formed
by
verb + path + str(expires) + data

Where this path includes the parameters
e.g.     /api/v1/instrument=key1=value1&key2=value2

And expires is a UNIX timestamp after which the request is no longer valid.
 e.g.  int(round(time.time()) + 5)
This is to prevent replay attacks.

If there is no data, data is just an empty string.

This message must be signed  hex(HMAC_SHA256(apiSecret, message ))
and included in the headers

The headers needed are:

api-expires:  (expires )

api-key: (our public API key)

api-signature: (the message, signed) 


Everything is explained here:
https://www.bitmex.com/app/apiKeysUsage



-- Isolating bots in the same account --

To try to have several bots sharing the bitmex account, we used the 
"clOrdID" field when placing orders.

Every order will have an clOrdId following this format:
        t = "{}.{}.{}.{}.{}.{}.{}"
        r = t.format(
            self.botid,
            year,
            month,
            day,
            hour,
            minutes,
            secs
        )


But leverage is shared for all trades in the account, so isolating bots
it's not possible.

-- Value of the position --

https://www.bitmex.com/app/pnlGuide



Bitmex is a futures exchange, even though the contracts are perpetual,
do not expire. So it mimics margin-based spot trading, the price considered
is the present price of bitcoin. 
We can consider it almost spot trading, with some peculiarities in the way 
position value is computed.


The value of the position depens on 3 items:

-1- Fees or rebates corresponding to the opening of the position.
  Fees for taker orders, rebates for maker orders.

-2- Funding costs


https://futuresbit.com/bitmex-funding-explained/

"Every 8 hours, all traders who have open positions in the perpetual swap pair, will either receive or pay a funding fee. Whether you have to pay or receive a funding fee, depends whether you are long or short. Positive funding fee would mean that the contract is trading at a premium (or small enough discount), and those who are long will have to pay to their contract counterparties with the short positions. Negative funding fee would indicate that the contract is trading at a discount and those who are short will have to pay to those who are long at the opposite side of the futures."


In other words:
If "the market is long" (the whole amount of long trades in the market is 
bigger than the amount of short trades), and you are also long, you pay
funding cost, the "interest" for the bc you borrowed.

If market is long and you are short, you earn funding cost, as you are
borrowing bc to the longs.

If the market is short and you are also short, you pay funding cost.
If the market is short and you are long, you earn funding cost.

This funding costs are charged / credited to your account 
every 8 hours. We call them "costs", even though is money you earn
if your trade has the opposite direction than the average of the
market.


-3-
Main profits and losses.
The main part of your activity, the market value of your position,
minus the market value of your position when you opened it, minus
the value of the funds you borrowed from the exchange.

If you buy 1 btc @ 11 000 USD, with no leverage (leverage = 1), 
and its current price is 11 500 USD, your PNL is 500 USD.
Besides that, you pay or earn fees and costs.

This term, "main profits and losses" is coined by me. Bitmex
calls it "unrealized profits and losses" (when the position still 
opened), but when you close the position, this amount is part of
"realized PNL", added to fees, rebates and funding costs.


When your position is still opened, main profits and losses is a floating
PNL, an "unrealized PNL". When you close the position (or exchange liquidates
it), this unrealized PNL is charged / credited to your realized PNL, added 
to fees and rebates. It stays there until next day at 12:00 UTC, when it 
is charged / credited to your wallet.

Margin balance is the main indicator of "the real money you have".
It is equivalent to your wallet balance, plus unrealized PNL.


All this terms are a little tricky, be careful. For instance,
most of times the "realized PNL" is the sum of fees, rebates and costs
of your position. But if you have closed one (or several) position the 
same day, it also includes the "main" profit and loss of today
previous trades. Where "day" starts at 12:00 UTC.


Also realize that the result of your current trade / last trade can
be computed in three different places:
1. Unrealized PNL, when the trade still opened.
2. Realized PNL, when the trade has been closed "today"
3. Wallet, if it was closed "not today".

Where "today" started at 12:00 UTC (not 00:00 UTC). Corresponds to 
14:00 spanish summer time or 13:00 spanish winter time.


--wallet history--

Everyday, Bitmex credits or charges our account with the realised profits
and losses of our position. This includes fees/rebates for opening position
and credits/charges for funding.


Excludes profits and losses due to the price movement of positions that
still opened.

English word "credit" means "loan", but also to pay a (positive) amount.
The same that spanish word  "abono"

The bank credited the amount to my account. El banco abonó la cantidad en mi cuenta.




Example:

timestamp    : 2018-06-30 12:00:00+00:00
transactID   : 5195e58b-7301-2b4b-f30e-8d53561e3cbf
transactType : RealisedPNL transactStatus: Completed
marginBalance:  0.00000000 BTC (      0.00 USD)
amount       :  0.00334691 BTC (     22.17 USD)
walletBalance:  1.31087616 BTC (   8683.24 USD)

timestamp    : 2018-06-29 12:00:00+00:00
transactID   : 419d4152-88f5-396e-b015-c773702accaa
transactType : RealisedPNL transactStatus: Completed
marginBalance:  0.00000000 BTC (      0.00 USD)
amount       : -0.00112694 BTC (     -7.46 USD)
walletBalance:  1.30752925 BTC (   8661.07 USD)



On june 29th, our wallet balance is 
1.30752925 BTC 

The amount       : -0.00112694 BTC means that we have paid 
0.00112694 BTC , so previous day we had 1.30752925 + 0.00112694

On june 30th the amount of realised PNL is 0.00334691, so
now our wallet contains 

1.30752925  + 0.00334691 =  1.31087616


In other words:
walletBalance is our balance for that day, amount is the delta from
previuous dat.




