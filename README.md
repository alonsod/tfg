# tfg-alonsod

- [07-11-19](#07-11-19)
- [10-11-19](#10-11-19)
- [11-11-19](#11-11-19)
- [12-11-19](#12-11-19)
- [13-11-19](#13-11-19)
- [21-11-19](#21-11-19)
- [06-12-19](#06-12-19)
- [07-12-19](#07-12-19)
- [10-12-19](#10-12-19)
- [11-12-19](#11-12-19)
- [21-12-19](#21-12-19)
- [22-12-19](#22-12-19)
- [25-12-19](#25-12-19)
- [18-01-20](#18-01-20)
- [23-01-20](#23-01-20)
- [25-01-20](#25-01-20)
- [27-01-20](#27-01-20)
- [29-01-20](#29-01-20)
- [03-02-20](#03-02-20)
- [12-02-20](#12-02-20)
- [16-02-20](#16-02-20)
- [29-02-20](#29-02-20)
- [03-03-20](#03-03-20)
- [11-03-20](#11-03-20)
- [16-03-20](#16-03-20)
- [17-03-20](#17-03-20)
- [23-03-20](#23-03-20)
- [24-03-20](#24-03-20)
- [25-03-20](#25-03-20)
- [28-03-20](#28-03-20)
- [29-03-20](#29-03-20)
- [30-03-20](#30-03-20)
- [14-04-20](#14-04-20)
- [21-04-20](#21-04-20)
- [29-04-20](#29-04-20)
- [11-05-20](#11-05-20)
- [13-05-20](#13-05-20)

***

## 13-05-20

- Hablo por un grupo oficial de dudas de Telegram de Bybit y me resuelven la duda del price. Tambien me responde IT (el dia 18-05-20) diciendome lo mismo. Por tanto, implemento las condiciones del price y termino **_place_limit_order**:

```python
def _place_limit_order(self, side, symbol, qty, price, time_in_force):
        # print(side)
        # print(symbol)
        # print(qty)
        # print(time_in_force)
        # print(price)
        r = {}
        if self._are_limit_params_ok(side, qty, price):
            verb = "POST"
            path = "/v2/private/order/create"
            params = {
                "api_key": self.key,
                "side": side,
                "timestamp": int(time.time()*1000),  # in ms
                "price": price,
                "symbol": symbol,
                "order_type": "Limit",
                "qty": qty,
                "time_in_force": time_in_force,
            }
            params = self._get_sorted_dict(params, "key") # get sorted dict by key
            params.update({"sign": self.get_signature(self.secret, params)})
            r = self._send_request(
                verb = verb,
                path = path,
                params = params
            )
            # checking reponse status. If error, status = ret_msg
            if r["msg"]["ret_msg"].lower() != "ok":
                r["status"] = r["msg"]["ret_msg"]
        else:
            r["status"] = "error_params"
            msg = "Limit order params: side = "+str(side)
            msg += ", symbol = "+str(symbol)
            msg += ", qty = "+str(qty)
            msg += ", price = "+str(price)
            msg += ", time_in_force = "+str(time_in_force)+"\n"
            r["msg"] = msg
            self.log.write(msg)
            mprint(msg)
        return r
```

El checkeo de parametros:

```python
    def _are_limit_params_ok(self, side, qty, price):
        if price is None:
            msg = "Error in place_active_order: "
            msg += "price is not defined\n"
            self.log.write(msg)
            mprint(msg)
            return False
        cond1 = self._are_market_params_ok(side, qty)
        cond2 = self._is_good_price(price)
        return cond1 and cond2

    def _is_good_price(self, price):
        if not (isinstance(price, int) or isinstance(price, float)):
            msg = "Error: price is "+str(price)+": must be int or float\n"
            mprint(msg)
            self.log.write(msg)
            return False
        if price % self.tick_size != 0:
            msg = "Error: price % tick_size != 0\n"
            mprint(msg)
            self.log.write(msg)
            return False
        if price >= 1000000 or price < 0:
            msg = "Error: price value is "+str(price)+": must be between 0 and 1000000\n"
            mprint(msg)
            self.log.write(msg)
            return False
        # print(self.get_market_price())
        if price <= (self.get_market_price() * 0.1):
            msg = "Error: price value is "+str(price)+": must be 0.1 * market_price\n"
            mprint(msg)
            self.log.write(msg)
            return False
        return True
```

Si nos fijamos, tambien se utiliza la comprobacion de parametros de una order de tipo market. Esto es porque al fin y al cabo, una limit order es como una market order pero indicandole cuando comprar o vender, en vez de que compre a precio actual.

***

## 11-05-20

- A partir del dia 29 de abril, se empieza a implementar **_place_limit_order**, pero el  API no queda muy claro con el _price_ y tenemos que enviar un correo a IT de Bybit (enviado el 07-05). A dia de hoy
hemos recibido respuesta.
- Se añade en la mayoria de ejemplos un par de lineas que comprueban si la peticion al servidor ha
sido satisfactoria o no. Se añaden esas lineas nada mas recibir la respuesta devuelta por el metodo
**send_request**. Se pone de ejemplo _\_ll\_orderbook_:

```python
def _ll_orderbook(self):
    """
    returns raw orderbook (low level)
    """
    verb = "GET"
    path = "/v2/public/orderBook/L2"
    params = {}
    params["symbol"] = self.symbol

    r = self._send_request(
        verb = verb,
        path = path,
        params = params
    )
    # checking reponse status. If error, status = ret_msg
    if r["msg"]["ret_msg"].lower() != "ok":
        r["status"] = r["msg"]["ret_msg"]
    return r
```

Como podemos ver si el mensaje que retorna el server es distinto de "ok", cambiamos el status de salida
de la peticion. El nivel superior verá que se trata de una peticion con un error y en funcion del tipo
de error o de metodo, reintentara o no.
***

## 29-04-20

- Pongo una _market\_order_. El metodo es llamado desde **place_active_order**, el cual, decide si
poner una _market_ o _limit_ order. En este caso, al ser una de tipo _market_, se llama a **_place_market_order**:

```python
def _place_market_order(self, side, symbol, qty, time_in_force):
    # print(side)
    # print(symbol)
    # print(qty)
    # print(time_in_force)
    r = {}
    if self._are_market_params_ok(side, qty):
        verb = "POST"
        path = "/v2/private/order/create"
        params = {
            "api_key": self.key,
            "side": side,
            "timestamp": int(time.time()*1000),  # in ms
            "symbol": symbol,
            "order_type": "Market",
            "qty": qty,
            "time_in_force": time_in_force,
        }
        params = self._get_sorted_dict(params, "key") # get sorted dict by key
        params.update({"sign": self.get_signature(self.secret, params)})
        print(params)
        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
        # falta mirar si el status que da el server es distinto de "ok"
    else:
        r["status"] = "error_params"
        msg = "Market order params: side = "+str(side)
        msg += ", symbol = "+str(symbol)
        msg += ", qty = "+str(qty)
        msg += ", time_in_force = "+str(time_in_force)+"\n"
        r["msg"] = msg
        self.log.write(msg)
        mprint(msg)
    return r
```

Comprobacion de parametros:

```python
def _are_market_params_ok(self, side, qty):
    if side != "Buy" and side != "Sell":
        msg = "Side must be \"Buy\" or \"Sell\", not \""+str(side)+"\"\n"
        self.log.write(msg)
        mprint(msg)
        return False
    if type(qty) != int or qty > 1000000 or qty < 0:
        msg = "qty must be int, and 0 < qty <= 1000000, "
        msg += "not "+str(qty)+"\n"
        self.log.write(msg)
        mprint(msg)
        return False
    return True
```

La salida (tapo de api_key que es lo que se usa para autenticarse con el server):

![place_market_order](bybit/images/place_market_order.png)
***

## 21-04-20

- Durante esta semana he ido mirando el codigo y viendo las cosas que son necesarias
cambiar:
  - Comprobar que el timestamp cumple la regla de sincronizacion con el servidor
  - Si la  libreria recibe una peticion de orden a limite y el precio no es correcto, responder con error
  - loggear la libreria de bybit con lo que usaba Miguel en Bitmex y Bitfinex
  - Implementar bot que nos diga si hay cambios en el API
  - Implementar el sistema del sleep y reintentar en las funciones que llevo hechas

Este último punto es el que he ido haciendo estos dias. Lo que he tenido que cambiar
es la manera que tiene un metodo de hacer una petición. Pongamos de ejemplo al orderbook:

1. **get_orderbook**: Hace un bucle "infinito", llamando a **\_ll\_orderbook** hasta que este responde con "ok", con sleeps para dar tiempo
entre peticiones.
2. **\_ll\_orderbook**: lo que se hace es asignar los parametros
necesarios para hacer la peticion. Se llama a **\_send\_request** que es
 el encargado de hacer la peticion al servidor.
3. **\_send\_request**: hace otro bucle "infinito" haciendo
peticiones al servidor con la llamada **\_ll\_send\_request** hasta que responda con un "ok", con sleeps para dar tiempo entre peticiones.
4. **\_ll\_send\_request**: es el mas bajo nivel. Es el encargado de hacer la petición al servidor. Devuelve un diccionario como sigue (pseudocodigo):

```python
response = {
    "status": "ok", # toma valores "ok" o "error"
    "msg": {}        # si status = "ok", msg suele ser un diccionario. Sino es un mensaje de error
}
```

Para verlo mas en detalle, mirar el codigo de [bybit3.py](bybit/bybit3.py), donde
poco a poco voy añadiendo los logs de la libreria de Miguel.
***

## 14-04-20

- info sobre [order types, time_in_force, etc](<https://medium.com/bybit/order-types-definition-differences-use-f114d609ab03>) en Bybit.
- Empiezo a hacer el metodo **place_active_order**. Primero he de hacer metodos
intermedios, ya que requiere de valores como _tick\_size_ o comprobaciones
sobre el _price_. Para obtener el _tick\_size_, se usa el metodo **get_symbol_tick_size**, que a su vez llama al metodo **_get_query_symbol**.
Este ultimo metodo obtiene la informacion de todos los symbols (BTCUSD, ETHUSD, EOSUSD, XRPUSD) de la casa de cambio y **get_symbol_tick_size** lo llama obteniendo el valor _tick\_size_. A continuacion, ambos metodos:

```python
def get_symbol_tick_size(self):
        """
        calls _get_query_symbol to get self.symbol's tick_size
        all tick_sizes are float
        return float tick_size or -1.0 if error ocurred
        """
        r = self._get_query_symbol()
        if type(r) == requests.models.Response:
            r = r.json()
            for x in r["result"]:
                if x["name"] == self.symbol:
                    return float(x["price_filter"]["tick_size"])
        return(float(-1))

def _get_query_symbol(self):
    """
    return all symbols info
    """
    reponse = {}
    verb = "GET"
    path = "/v2/public/symbols"
    params = {
        "api_key": self.key,
        "timestamp": int(time.time()*1000),  # in ms
    }
    params = self._get_sorted_dict(params, "key")   # get sorted dict by key
    params.update({"sign": self.get_signature(self.secret, params)})
    reponse = self._ll_send_request(
    verb = verb,
    path = path,
    params = params
    )
    return reponse
```

Hace falta hacer otros metodos intermedios para construir el metodo **place_active_order**.
Luego solamente sera unir las piezas del puzle.
***

## 30-03-20

- Consigo hacer dos métodos nuevos,
[cancel_active_order](<https://bybit-exchange.github.io/docs/inverse/#t-cancelv2active>) y [cancel_all_active_orders](<https://bybit-exchange.github.io/docs/inverse/#t-cancelallactive>):

```python
def cancel_active_order(self, **kwargs):
    reponse = {}
    verb = "POST"
    path = "/v2/private/order/cancel"
    params = {
        "api_key": self.key,
        "symbol": self.symbol,
        "timestamp": int(time.time()*1000),  # in ms
    }
    if self._are_cancel_active_order_kwargs_ok(kwargs):
        for key, value in kwargs.items():
            if key == "order_id":
                params[key] = value
                break
            elif key == "order_link_id":
                params[key] = value
                break
        params = self._get_sorted_dict(params, "key")   # get sorted dict by key
        params.update({"sign": self.get_signature(self.secret, params)})
        reponse = self._ll_send_request(
        verb = verb,
        path = path,
        params = params
        )
    else:
        #TODO: poner un warning en el log
        reponse["status"] = "error"
        reponse["msg"] = str("Error: bad args pased in function calcel active order: "+str(kwargs))
    return reponse
```

```python
def cancel_all_active_orders(self):
        reponse = {}
        verb = "POST"
        path = "/v2/private/order/cancel"
        params = {
            "api_key": self.key,
            "symbol": self.symbol,
            "timestamp": int(time.time()*1000),  # in ms
        }
        params = self._get_sorted_dict(params, "key")   # get sorted dict by key
        params.update({"sign": self.get_signature(self.secret, params)})
        reponse = self._ll_send_request(
        verb = verb,
        path = path,
        params = params
        )
        return reponse
```

Ambos son métodos para cancelar _active orders_. Quería primero tener estos metodos implementados antes que los metodos de **place_active_order**. La salida de ambos comandos de momento es la misma porque no hay _active orders_ ahora:

![alt text](bybit/images/cancel_orders.png)

***

## 29-03-20

- Consigo hacer una peticion con autenticacion de las posiciones abiertas (información [aqui](<https://bybit-exchange.github.io/docs/inverse/#t-queryactive>))

- Para todo el tema de autenticación (información [aqui](<https://bybit-exchange.github.io/docs/inverse/#t-authentication>)), se nos piden los siguientes parametros:
  - **api_key**
  - **timestamp** - UTC timestamp in **milliseconds**
  - **sign** - a signature derived from the request's parameters

- A la hora de hacer una query, los parametros han de ser ordenados **alfabeticamente**, excepto la firma que debe ir **al final.**. Pongo un ejemplo de cómo se ha hecho la peticion, con el método
[get_positions](<https://bybit-exchange.github.io/docs/inverse/#t-position>):

```python

def get_positions(self):
        verb = "GET"
        path = "/v2/private/position/list"
        params = {
            "api_key": self.key,
            "symbol": self.symbol,
            "timestamp": int(time.time()*1000)  # in ms
        }
        params = self._get_sorted_dict(params, "key")    # get sorted dict by key
        params.update({"sign": self.get_signature(self.secret, params)})
        reponse = self._ll_send_request(
            verb = verb,
            path = path,
            params = params
        )
        return reponse
```

Devuelve mi lista de posiciones. La salida de la función es la siguiente:

![alt text](bybit/images/get_positions.png)

Hay que tener en cuenta que en el momento de hacer la peticion no habia ninguna posicion abierta.
***

## 28-03-20

- Hago un metodo que representa la salida del orderbook de manera que lo entiendan los humanos. Metodo **print_orderbook**:

```python
def print_orderbook(self, raw_orderbook):
        '''
        receives raw_orderbook where raw_orderbook = get_orderbook()
        raw_orderbook dict fields are:
        'ret_code'
        'ret_msg'
        'ext_code'
        'ext_info'
        'result': dicts list: {'symbol', 'price', 'size', 'side'}
        print pretty ordebook
        '''
        print("Symbol: "+self.symbol)
        print("price | size")
        prev_side = ""
        r_json = raw_orderbook.json()
        for elem in r_json["result"]:
            if len(prev_side) <= 0:
                prev_side = elem['side']
            if prev_side != elem['side']:
                print(">--------------<")
                prev_side = elem['side']
            prize = float(elem['price'])
            size = float(elem['size'])
            print("{:.1f} {:8.5f}".format(prize, size))
```

La salida de la funcion es la siguiente:

![alt text](bybit/images/pretty_orderbook.png)
***

## 25-03-20

- Después de leer la clave de la testnet, consigo obtener el Orderbook en crudo (raw).
Para ello hay que irse a la pagina del API e irse a la seccion de [orderbook](<https://bybit-exchange.github.io/docs/inverse/#t-orderbook>):

- La peticion por **curl** para asegurarnos de que nuestra salida esta bien:

```sh
curl https://api-testnet.bybit.com/v2/public/orderBook/L2?symbol=BTCUSD
```

Lo que hecho es basicamente seguir el patron de diseño de Miguel de Bitmex,
cambiando algunas cosas porque en Bybit hay diferencias, como por ejemplo la manera de autenticar, que es distinta. Por ejemplo, a la hora de hacer una peticion GET, lo que se hace es construir la url con las cabeceras y demas campos que se necesiten para recibir una respuesta. La funcion es la siguiente:

```python
def get_orderbook(self):
        verb = "GET"
        path = "/v2/public/orderBook/L2"
        params = {}
        params["symbol"] = self.symbol

        reponse = self._ll_send_request(
            verb = verb,
            path = path,
            params = params
        )
        return reponse
```

La salida de la funcion es la siguiente:

![alt text](bybit/images/orderbook-alonsod.jpg)

Si lo comparamos con la salida que da el comando de shell, podemos ver que la salida es la misma (cambiando el orden de algunas cosas):

![alt text](bybit/images/orderbook-bybit.jpg)

Falta poner la salida en un formato que sea legible por humanos, pero el primer paso ya está dado.

El bajo nivel de peticiones REST es el siguiente:

```python
def _ll_send_request(self, verb, path, params, data=""):
        params_encoded = urllib.parse.urlencode(params)
        path_with_params = path
        if len(params_encoded) != 0:
            path_with_params += "?" + params_encoded

        full_url = self.base_url + path_with_params
        # print(full_url)
        # print(REQUEST_TIMEOUT)
        try:
            if verb == "GET":
                r = requests.get(
                    full_url,
                    timeout = REQUEST_TIMEOUT
                )
        except Exception as e:
            r = {}
            r["status"] = "error"
            r["msg"] = str(e)

        return r
```

No está terminado ni mucho menos pero para la petición del orderbook al menos nos sirve.
***

## 24-03-20

- Consigo leer mi clave de la testnet de Bybit (key y secret)

***

## 23-03-20

- Compruebo que funciona el codigo de Python3 de Bitmex y Bitfinex en Ubuntu 18.04.3 LTS
- Me creo una cuenta en Bybit y Bybit testnet.
  - El api esta  [aqui](<https://bybit-exchange.github.io/docs/inverse/#t-introduction>)
  - Me creo una clave para la **testnet** de momento.

***

## 17-03-20

- Consigo que funcione:
  - test_cancel_all_orders(bf)
  - test_place_order(bf)
  - test_active_orders(bf)
  - test_order_status(bf)
  - test_transfer_wallets(bf)
  - test_smart_order(bf)
  - test_open_position(bf)
  - test_close_position(bf)
  - test_buy_all_in_trading(bf)

La solución pasaba por el problema de que un par de funciones (base64.standard_b64encode(j_encoded) y hmac.new(secret_encoded, data, hashlib.sha384)) en python2 recibían strings como argumentos pero en python3 eso cambia, necesitan bytes-like object por lo que para solucionarlo, codificamos los strings a utf-8. Todas las funciones que he puesto, pasaban por el metodo **_payloadPacker**, el cual ha sido modificado y se muestra a continuación:

```python
def _payloadPacker(self, payload):
    # packs and signs the payload of the request.
    # API v1

    j = json.dumps(payload)

    # codificamos el json a utf-8 (bytes-like object)
    j_encoded = j.encode()
    data = base64.standard_b64encode(j_encoded)

    # codificamos el secret a utf-8 (bytes-like object)
    secret_encoded = self.secret.encode()
    h = hmac.new(secret_encoded, data, hashlib.sha384)
    signature = h.hexdigest()

    return {
        "X-BFX-APIKEY": self.key,
        "X-BFX-SIGNATURE": signature,
        "X-BFX-PAYLOAD": data
    }
```

- Falta por probar la funcion **trading_qc_to_bc(bf)**

***

## 16-03-20

- Termino de probar el resto de las funciones. Funcionan:
  - test_positions(bf)
  - test_transfer_all(bf)
- El resto falla. Parece que tengan el mismo fallo siempre. Por ejemplo, ejecutamos test_cancel_all_orders(bf):

```sh
File "/vagrant/codigo/codigo_2020/codigo_bitfinex-python3/bitfinex.py", line 194, in _payloadPacker
    data = base64.standard_b64encode(j)
  File "/usr/lib/python3.5/base64.py", line 96, in standard_b64encode
    return b64encode(s)
  File "/usr/lib/python3.5/base64.py", line 59, in b64encode
    encoded = binascii.b2a_base64(s)[:-1]
TypeError: a bytes-like object is required, not 'str'

```

Supongo que se llama a lo mismo desde las otras funciones. Tocando solamente eso deberia arreglarse el error general.
***

## 11-03-20

- Función **trading_qc_to_bc(bf)** no funciona. Pensaba que si pero porque tiene un sleep y pensaba que era por falta de bc:

```sh
vagrant@ubuntu-xenial:/vagrant/codigo/codigo_2020/codigo_bitfinex-python2$ python2.7 test_bitfinex.py No BTC to transfer from trading wallet to exchange wallet
No USD to transfer from trading wallet to exchange wallet
2020-03-11 17:27:46+00:00 Cancelling all orders
Traceback (most recent call last):
  File "test_bitfinex.py", line 302, in <module>
    main()
  File "test_bitfinex.py", line 56, in main
    trading_qc_to_bc(bf)
  File "test_bitfinex.py", line 134, in trading_qc_to_bc
    bf.place_smart_order(wallet, order_amount, side, purpose)
NameError: global name 'side' is not defined

```

- Hago la conversión a Python3 y pruebo algunas funciones que ya funcionan (valga la redundancia):
  - test_wallets(bf) # (p)
  - test_margin_symbol(bf) # (p)
  - test_margin_base(bf)
  - test_sleep(bf)
  - test_raw_orderbook(bf)
  - test_orderbook(bf)
  - test_ticker(bf)
- En las funciones que pone **(p)** es porque falta probarlas con bc ya que solo son probadas parcialmente.

***

## 03-03-20

- Pruebo todas las funciones de Bitfinex en python2. Funcionan todas estas:
  - test_cancel_all_orders(bf)
  - test_place_order(bf)
  - test_active_orders(bf)
  - test_positions(bf)
  - test_order_status(bf)
  - test_wallets(bf)
  - test_margin_symbol(bf)
  - test_margin_base(bf)
  - test_sleep(bf)
  - test_raw_orderbook(bf)
  - test_orderbook(bf)
  - test_ticker(bf)
  - test_transfer_wallets(bf)
  - test_transfer_all(bf)
  - test_smart_order(bf)
  - trading_qc_to_bc(bf)
  - test_open_position(bf)
  - test_close_position(bf)
  - test_buy_all_in_trading(bf)
- Las que no funcionan son:
  - test_get_wallet(bf)

```sh
bitfinex get_wallet()
Traceback (most recent call last):
File "test_bitfinex.py", line 302, in <module>
    main()
File "test_bitfinex.py", line 59, in main
    test_get_wallet(bf)
File "test_bitfinex.py", line 72, in test_get_wallet
    print bf.get_wallet()
TypeError: get_wallet() takes exactly 4 arguments (1 given)
```

- test_sell_all_in_trading(bf)

```sh
Traceback (most recent call last):
File "test_bitfinex.py", line 302, in <module>
    main()
File "test_bitfinex.py", line 60, in main
    test_sell_all_in_trading(bf)
File "test_bitfinex.py", line 80, in test_sell_all_in_trading
    bf.sell_all_in_trading()
AttributeError: 'Bitfinex' object has no attribute 'sell_all_in_trading'
```

***

## 02-03-20

- Consigo leer la clave necesaria para Bitfinex en Python2

***

## 29-02-20

- Creo cuenta en **Bitfinex**
- Creo clave para autenticarme a la hora de hacer peticiones REST

***

## 16-02-20

- Sigo probando funciones en Python3. Ya funcionan:
  - test_orderbook(bm)
  - test_margin_balance(bm)
  - test_wallet_value(bm)
  - test_current_trade_status(bm)
  - test_ticker(bm)
  - test_get_wallet(bm)
  - test_wallet_summary(bm)
  - test_get_leverage(bm)
  - test_positions(bm)
  - test_get_position(bm)
  - test_get_position_as_qc(bm)
  - test_wallet_history(bm)
  - test_trade_history(bm)
  - test_execution(bm)
  - test_open_position(bm)
  - test_close_position(bm)
  - test_cancel_all_orders(bm)
  - test_fetch_trades(bm)
  - test_place_basic_order(bm)
  - test_get_orders(bm)
  - test_get_my_orders(bm)
  - test_order_status(bm)
  - test_trailing_stop(bm)
- Me ha fallado solamente **test_interval_realized_pl(bm)**

```sh
Traceback (most recent call last):
  File "dra_test_bitmex.py", line 528, in <module>
    main()
  File "dra_test_bitmex.py", line 98, in main
    test_interval_realized_pl(bm) # NOT OK
  File "dra_test_bitmex.py", line 207, in test_interval_realized_pl
    verbose=verbose
  File "/vagrant/codigo/codigo_2020/codigo_bitmex-python3/mbitmex.py", line 563, in interval_realized_pl
    r1 = t1 <= t and t <= t2
TypeError: unorderable types: float() <= NoneType()

```

***

## 12-02-20

- Consigo que funcione el **orderbook** en Python3.
- He mirado las diferencias entre Python2 y Python3 de la funcion sort. Recomiendan usar un wrapper( [info aqui](<https://docs.python.org/2/howto/sorting.html>)).
- Se ha añadido lo siguiente en mbitmex.py:
  - **cmp_to_key** es el wrapper
- He modificado las lineas donde pone:
  - prices.sort(key=self.cmp_to_key(self._ascending_criterium))
  - prices.sort(key=self.cmp_to_key(self._descending_criterium))

```python
def cmp_to_key(self, mycmp):
    'Convert a cmp= function into a key= function'
    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K

    def _sort_prices(self,  prices, ascending):
        if ascending:
            prices.sort(key=self.cmp_to_key(self._ascending_criterium))
        else:
            prices.sort(key=self.cmp_to_key(self._descending_criterium))
        return prices
```

***

## 03-02-20

- Consigo petición sencilla a bitmex en python3:

```python
def _get_signature(self, verb, path_with_params, expires, data):

        secret = self.secret
        if isinstance(data, (bytes, bytearray)):
            data = data.decode('utf8')
        message = verb + path_with_params + str(expires) + data
        secret_bytes = secret.encode('utf-8')
        message_bytes = message.encode('utf-8')
        signature = hmac.new(secret_bytes, message_bytes, digestmod=hashlib.sha256)
        signature = signature.hexdigest()
        return signature
```

**secret_bytes** y **message_bytes** son las modificadas frente al código original en python2.
***

## 29-01-20

- Problema con pip y python3. No estaba instalado en vagrant. Seguir los comandos:

```sh
sudo apt-get install python3-pip
python3 -m pip install pytz
```

***

## 27-01-20

- Miro el README que explica como hacer para enviar las cabeceras cifradas para peticiones REST mas complicadas.
- Consigo hacer una peticion sencilla del usuario en python2 de mi user usando el metodo del objeto ll_send_request
- Paso el 2to3 a mbitmex.py para que me ponga bien los print pero me da error de librerias

***

## 25-01-20

- Poner trazas en el codigo de python2, luego ejecutarlo en python3, cambiar los fallos (unicode y 8bits) y dejar el codigo funcionando en python3 al igual que se haria en python2.

***

## 23-01-20

- Probar ticker peticion REST con python3

***

## 18-01-20

- Sigo probando las funciones de test_bitmex.py:
  - test_add_trades_to_file(bm): NOT OK
  - test_get_leverage(bm) ## OK
  - test_place_order_almost_immediate(bm) ## OK
  - test_get_position_as_qc(bm) ## NOT OK
  - test_place_basic_order(bm) ## OK
  - test_close_residual(bm) ## NOT OK
  - test_place_order(bm) ## OK
  - test_get_orders(bm) ## NOT OK
  - test_cancel_absolute_all_orders(bm) ## NOT OK
  - test_wallet_history(bm) ## OK
  - test_trade_history(bm) ## OK
  - test_execution(bm) ## OK
  - test_get_my_orders(bm) ## OK
  - test_order_status(bm) ## OK (Para probarlo hay que poner el identificador de una orden. Ejecutar test_get_my_orders para mirar los ids)
  - test_order_status_ok(bm) ## OK (No entiendo lo que hace)
  - test_interval_realized_pl(bm) ## OK
  - test_trailing_stop(bm) ## OK

***

## 25-12-19

- Sigo probando las funciones de test_bitmex.py:
  - test_wallet_value(bm): OK
  - test_wallet_value(bm): OK
  - test_current_trade_status(bm): OK
  - test_max_position_size(bm): NOT OK
  - test_ticker(bm): OK
  - test_get_wallet(bm): OK
  - test_wallet_summary(bm): OK
  - test_get_position(bm): OK
  - test_get_trades(bm): NOT OK

***

## 22-12-19

- Sigo probando las funciones de test_bitmex.py:
  - test_orderbook(bm): OK
  - test_margin_balance(bm): OK
  - test_wallet_balance(bm) : NOT OK
  - test_open_position(bm) : OK
  - test_close_position(bm) : OK
  - test_cancel_all_orders(bm): NOT OK
  - test_positions(bm) : OK

***

## 21-12-19

- Miro como hacer un buen README [aqui](<https://daringfireball.net/projects/markdown/>)
- Cuidado a la hora de asignar variables de entorno. No olvidar el 'export'

***

## 11-12-19

- La direccion del fichero .key para probarlo en Vagrant debe ser /vagrant/codigo_bitmex/bitmex_alonsod_testnet.key
- Hay que instalar muchas cosas antes de poder probar el fichero de test de Miguel. Todo estara en /home/alonsod/tfg/vagrant/needed-packages.sh:

```sh
#!/usr/bin/env bash
apt-get update -y
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo dpkg-reconfigure locales
apt-get install -y python-minimal
apt-get install -y python3
apt-get install -y python-pip
pip install requests
pip install pytz
pip install six
mkdir $HOME/mbot_data
export MBOT_DATA_DIR=$HOME/mbot_data
```

- Cuando hacemos sudo dpkg-reconfigure locales, salta un menu en el terminal que de momento no se como hacer para que acepte todo

***

## 10-12-19

- Pruebo codigo de Miguel. Hago pruebas en da-pruebas.py para ver el funcionamiento de:
  - misc.expand_tilde (cambia ~ por el home en una ruta dada)
- En test_bitmex.py hago una prueba para leer la key y la secret key (dra_test_readkey)
Para instalar en el vagrantfile lo que queramos:
  - <https://www.sitepoint.com/vagrantfile-explained-setting-provisioning-shell/>

***

## 07-12-19

- Creo la API key de la testnet (LastPass)
- Me descargo el codigo de Miguel y lo pruebo con vagrant en ubuntu/xenial64

***

## 06-12-19

- Para probar codigo de Miguel necesito usar Vagrant. Me tengo que meter en UEFI (disable secure boot)
- Pruebo a que funcione Vagrant y todo correcto (Los ficheros estaran en /vagrant)

***

## 21-11-19

- Repasar interfaze manual
- Probar todas las funciones del fichero de pruebas
- Pedirle un holamundo a bitmex en python3 (ticker, posicion, etc)
- Reescribir todas las funciones \_ll\_

***

## 13-11-19

- Consigo bitcoins ficticios para la testnet de BitMex:
  - <https://coinfaucet.eu/en/btc-testnet/>
  En esa pagina tenemos que meter la direccion de nuestra cartera de la testnet,
  lo de debajo del codigo QR (account>deposit): 2NBMEX7M5N9j8zG4QzyDjCGJ3ZcJ422NjAG
- Pruebo a hacer alguna operacion (abrir una posicion) Aqui viene el API REST para la plataforma de BitMex:
  - <https://testnet.bitmex.com/api/explorer/>
Video de trading y bitmex para ver como funciona el tema:
  - <https://www.youtube.com/channel/UCs_taZ4J7evBnxiIuyzhGHg>

***

## 12-11-19

- Me copio los pdfs de rest, html, json y xml en el directorio de teoria. Tambien un par de pdfs de Miguel sobre bitcoin.
- Me miro el pdf de rest

***

## 11-11-19

- Sigo mirando los conceptos del trading (las URLs del apartado anterior)

***

## 10-11-19

- Miro API BitMex:
  - <https://testnet.bitmex.com/api/explorer/#/>
  - <https://testnet.bitmex.com/app/restAPIMessages>
  - <https://github.com/OAI/OpenAPI-Specification>
  - <https://swagger.io/specification/>
  - <https://en.wikipedia.org/wiki/Representational_state_transfer> (REST)
  - <https://testnet.bitmex.com/app/wsAPI#Reference-Implementation>

  - <https://www.ig.com/es/bitcoin/como-hacer-bitcoin-trading>
  - <https://www.ig.com/es/trading-de-criptomonedas/que-es-el-trading-de-criptomonedas>
  - <https://www.ig.com/es/estrategias-de-trading/que-es-el-apalancamiento-financiero-190306#information-banner-dismiss>
  - <https://www.ig.com/es/glosario-trading/definicion-de-slipagge>
  - <https://www.ig.com/es/trading-de-cfd/como-operar-con-cfd-y-ejemplos>
  - <https://www.ig.com/es/gestion-del-riesgo>
  - <https://www.ig.com/es/bitcoin>

***

## 07-11-19

- Miramos diferencias entre Python 2 y Python 3:
  - <https://learntocodewith.me/programming/python/python-2-vs-python-3/>
  - <https://www.digitalocean.com/community/tutorials/how-to-port-python-2-code-to-python-3>
  - <https://www.digitalocean.com/community/tutorials/python-2-vs-python-3-practical-considerations-2>
  - <https://docs.python.org/3/>
  - <https://www.digitalocean.com/community/tutorials/how-to-use-string-formatters-in-python-3>
  - <https://pypi.org/project/tox/>
  - <https://pypi.org/project/six/>
  - <https://pypi.org/project/pylint/>

  - Python 3 no es compatible hacia atras con Python 2
- Encuentro un parseador de Python 2 a Python 3:
  - <https://docs.python.org/2/library/2to3.html>
