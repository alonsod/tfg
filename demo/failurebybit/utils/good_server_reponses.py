#!/usr/bin/env python3

TIMESTAMP_SERVER_RESPONSE = {
    'ret_code': 0, 
    'ret_msg': 'OK', 
    'ext_code': '', 
    'ext_info': '', 
    'result': {}, 
    'time_now': '1590424868.424636'
    }

ORDERBOOK_SERVER_RESPONSE = {
    "ret_code":0,"ret_msg":"OK",
    "ext_code":"",
    "ext_info":"",
    "result":[{"symbol":"BTCUSD","price":"9181","size":319471.1,"side":"Buy"}],
    "time_now":"1590250286.261263"
    }

MARKET_ORDER_EXAMPLE = {
    'ret_code': 0, 
    'ret_msg': 'OK', 
    'ext_code': '', 
    'ext_info': '', 
    'result': {
        'user_id': 113224, 
        'order_id': 'dfbdeed7-76c2-410e-8cd8-a46d5305e524', 
        'symbol': 'BTCUSD', 
        'side': 'Sell', 
        'order_type': 'Market', 
        'price': 9151.2, 
        'qty': 35, 
        'time_in_force': 'ImmediateOrCancel', 
        'order_status': 'Created', 
        'last_exec_time': 0, 
        'last_exec_price': 0, 
        'leaves_qty': 35, 
        'cum_exec_qty': 0, 
        'cum_exec_value': 0, 
        'cum_exec_fee': 0, 
        'reject_reason': '', 
        'order_link_id': '', 
        'created_at': '2020-05-27T15:38:30.526Z', 
        'updated_at': '2020-05-27T15:38:30.529Z'
    }, 
    'time_now': '1590593910.529688', 
    'rate_limit_status': 99, 
    'rate_limit_reset_ms': 1590593910523, 
    'rate_limit': 100
    }

LIMIT_ORDER_EXAMPLE = {
    'ret_code': 0, 
    'ret_msg': 'OK', 
    'ext_code': '', 
    'ext_info': '', 
    'result': {
        'user_id': 113224, 
        'order_id': '1a37f8e8-3832-42d8-9059-cdc3056bef85', 
        'symbol': 'BTCUSD', 
        'side': 'Buy', 
        'order_type': 'Limit', 
        'price': 9000, 
        'qty': 20, 
        'time_in_force': 'GoodTillCancel', 
        'order_status': 'Created', 
        'last_exec_time': 0, 
        'last_exec_price': 0, 
        'leaves_qty': 20, 
        'cum_exec_qty': 0, 
        'cum_exec_value': 0, 
        'cum_exec_fee': 0, 
        'reject_reason': '', 
        'order_link_id': '', 
        'created_at': '2020-05-27T16:14:44.810Z', 
        'updated_at': '2020-05-27T16:14:44.813Z'
    }, 
    'time_now': '1590596084.813486', 
    'rate_limit_status': 99, 
    'rate_limit_reset_ms': 1590596084807, 
    'rate_limit': 100
    }

TICKER_EXAMPLE = {
    "ret_code": 0,
    "ret_msg": "OK",
    "ext_code": "",
    "ext_info": "",
    "result": [
        {
            "symbol": "BTCUSD",
            "bid_price": "7230",
            "ask_price": "7230.5",
            "last_price": "7230.00",
            "last_tick_direction": "ZeroMinusTick",
            "prev_price_24h": "7163.00",
            "price_24h_pcnt": "0.009353",
            "high_price_24h": "7267.50",
            "low_price_24h": "7067.00",
            "prev_price_1h": "7209.50",
            "price_1h_pcnt": "0.002843",
            "mark_price": "7230.31",
            "index_price": "7230.14",
            "open_interest": 117860186,
            "open_value": "16157.26",
            "total_turnover": "3412874.21",
            "turnover_24h": "10864.63",
            "total_volume": 28291403954,
            "volume_24h": 78053288,
            "funding_rate": "0.0001",
            "predicted_funding_rate": "0.0001",
            "next_funding_time": "2019-12-28T00:00:00Z",
            "countdown_hour": 2
        }
    ],
    "time_now": "1577484619.817968"
}

CANCEL_ALL_ACT_ORD = {
    'ret_code': 0,
    'ret_msg': 'OK', 
    'ext_code': '', 
    'ext_info': '', 
    'result': [
                {
                    'clOrdID': '085c8ec8-f0cf-4d79-8d80-6b3bb8427c21', 
                    'user_id': 113224, 
                    'symbol': 'BTCUSD', 
                    'side': 'Sell', 
                    'order_type': 'Limit', 
                    'price': '8800', 
                    'qty': 44, 
                    'time_in_force': 'GoodTillCancel', 
                    'create_type': 'CreateByUser', 
                    'cancel_type': 'CancelByUser', 
                    'order_status': '', 
                    'leaves_qty': 44, 
                    'leaves_value': '0', 
                    'created_at': '2020-05-26T16:53:28.364587Z', 
                    'updated_at': '2020-05-26T16:53:58.232653Z', 
                    'cross_status': 'PendingCancel', 
                    'cross_seq': 607777374
                }
                ], 
    'time_now': '1590512038.233125', 
    'rate_limit_status': 90, 
    'rate_limit_reset_ms': 1590512038228, 
    'rate_limit': 100}

# when cancel all orders, if there is nothing to cancel, result = None, is not an error!
CANCEL_ALL_ACT_ORD2 = {
    'ret_code': 0,
    'ret_msg': 'OK', 
    'ext_code': '', 
    'ext_info': '', 
    'result': None,
    'time_now': '1590512038.233125', 
    'rate_limit_status': 90, 
    'rate_limit_reset_ms': 1590512038228, 
    'rate_limit': 100}

SYMBOLS_EXAMPLE = {
    "ret_code": 0,
    "ret_msg": "OK",
    "ext_code": "",
    "ext_info": "",
    "result": [
    {
        "name": "BTCUSD",
        "base_currency": "BTC",
        "quote_currency": "USD",
        "price_scale": 2,
        "taker_fee": "0.00075",
        "maker_fee": "-0.00025",
        "leverage_filter": {
        "min_leverage": 1,
        "max_leverage": 100,
        "leverage_step": "0.01"
        },
        "price_filter": {
            "min_price": "0.5",
            "max_price": "999999.5",
            "tick_size": "0.5"
        },
        "lot_size_filter": {
            "max_trading_qty": 1000000,
            "min_trading_qty": 1,
            "qty_step": 1
        }
    },
    {
        "name": "ETHUSD",
        "base_currency": "ETH",
        "quote_currency": "USD",
        "price_scale": 2,
        "taker_fee": "0.00075",
        "maker_fee": "-0.00025",
        "leverage_filter": {
            "min_leverage": 1,
            "max_leverage": 50,
            "leverage_step": "0.01"
        },
        "price_filter": {
            "min_price": "0.05",
            "max_price": "99999.95",
            "tick_size": "0.05"
        },
        "lot_size_filter": {
            "max_trading_qty": 1000000,
            "min_trading_qty": 1,
            "qty_step": 1
        }
    },
    {
        "name": "EOSUSD",
        "base_currency": "EOS",
        "quote_currency": "USD",
        "price_scale": 3,
        "taker_fee": "0.00075",
        "maker_fee": "-0.00025",
        "leverage_filter": {
        "min_leverage": 1,
        "max_leverage": 50,
        "leverage_step": "0.01"
        },
        "price_filter": {
            "min_price": "0.001",
            "max_price": "1999.999",
            "tick_size": "0.001"
        },
        "lot_size_filter": {
            "max_trading_qty": 1000000,
            "min_trading_qty": 1,
            "qty_step": 1
        }
    },
    {
        "name": "XRPUSD",
        "base_currency": "XRP",
        "quote_currency": "USD",
        "price_scale": 4,
        "taker_fee": "0.00075",
        "maker_fee": "-0.00025",
        "leverage_filter": {
            "min_leverage": 1,
            "max_leverage": 50,
            "leverage_step": "0.01"
        },
        "price_filter": {
            "min_price": "0.0001",
            "max_price": "199.9999",
            "tick_size": "0.0001"
        },
        "lot_size_filter": {
            "max_trading_qty": 1000000,
            "min_trading_qty": 1,
            "qty_step": 1
        }
    }
    ],
    "time_now": "1581411225.414179"
}

ALL_ORDERS_LIST = {
    "ret_code": 0,
    "ret_msg": "ok",
    "ext_code": "",
    "result": {
        "current_page": 1,
        "last_page": 6,
        "data": [
            {
                "user_id": 1,
                "symbol": "BTCUSD",
                "side": "Sell",
                "order_type": "Market",
                "price": 7074.1,
                "qty": 2,
                "time_in_force": "ImmediateOrCancel",
                "order_status": "Filled",
                "ext_fields": {
                    "close_on_trigger": True,
                    "orig_order_type": "BLimit",
                    "prior_x_req_price": 5898.5,
                    "op_from": "pc",
                    "remark": "127.0.0.1",
                    "o_req_num": -34799032763,
                    "xreq_type": "x_create"
                },
                "last_exec_time": "1577448481.696421",
                "last_exec_price": 7070.5,
                "leaves_qty": 0,
                "leaves_value": 0,
                "cum_exec_qty": 2,
                "cum_exec_value": 0.00028283,
                "cum_exec_fee": 0.00002,
                "reject_reason": "NoError",
                "order_link_id": "",
                "created_at": "2019-12-27T12:08:01.000Z",
                "updated_at": "2019-12-27T12:08:01.000Z",
                "order_id": "f185806b-b801-40ff-adec-52289370ed62"
            },
            {
                "user_id": 113224,
                "symbol": "BTCUSD",
                "side": "Sell",
                "order_type": "Market",
                "price": 8955.5,
                "qty": 35,
                "time_in_force": "ImmediateOrCancel",
                "order_status": "Filled",
                "ext_fields": {
                    "op_from": "api",
                    "remark": "82.159.121.240",
                    "o_req_num": -5019828573,
                    "xreq_type": "x_create"
                },
                "last_exec_time": "1590085609.437966",
                "last_exec_price": 8955.5,
                "leaves_qty": 0,
                "leaves_value": 0,
                "cum_exec_qty": 35,
                "cum_exec_value": 0.00390821,
                "cum_exec_fee": 2.94e-06,
                "reject_reason": "NoError",
                "order_link_id": "",
                "created_at": "2020-05-21T18:26:49.000Z",
                "updated_at": "2020-05-21T18:26:49.000Z",
                "order_id": "55ab4733-495f-4ae2-a131-41c797aa73c3"
            },
            {
                "user_id": 113224,
                "symbol": "BTCUSD",
                "side": "Buy",
                "order_type": "Limit",
                "price": 8000,
                "qty": 20,
                "time_in_force": "GoodTillCancel",
                "order_status": "Cancelled",
                "ext_fields": {
                    "op_from": "api",
                    "o_req_num": -2680004409,
                    "xreq_type": "x_create",
                    "cross_status": "Canceled"
                },
                "last_exec_time": "0.000000",
                "last_exec_price": 0,
                "leaves_qty": 0,
                "leaves_value": 0,
                "cum_exec_qty": 0,
                "cum_exec_value": 0,
                "cum_exec_fee": 0,
                "reject_reason": "EC_PerCancelRequest",
                "order_link_id": "",
                "created_at": "2020-05-13T19:03:52.000Z",
                "updated_at": "2020-05-13T19:04:08.000Z",
                "order_id": "2ab18547-d81b-4c44-9f53-7942a4f3232a"
            },
            {
                "user_id": 113224,
                "symbol": "BTCUSD",
                "side": "Sell",
                "order_type": "Limit",
                "price": 9510,
                "qty": 20,
                "time_in_force": "GoodTillCancel",
                "order_status": "Cancelled",
                "ext_fields": {
                    "op_from": "api",
                    "o_req_num": -1305309514,
                    "xreq_type": "x_create",
                    "cross_status": "Canceled"
                },
                "last_exec_time": "0.000000",
                "last_exec_price": 0,
                "leaves_qty": 0,
                "leaves_value": 0,
                "cum_exec_qty": 0,
                "cum_exec_value": 0,
                "cum_exec_fee": 0,
                "reject_reason": "EC_PerCancelRequest",
                "order_link_id": "",
                "created_at": "2020-05-07T14:52:06.000Z",
                "updated_at": "2020-05-07T14:52:51.000Z",
                "order_id": "292ad58d-81d6-499c-8fcc-b4e0db4617ae"
            },
        ]
    },
    "ext_info": None,
    "time_now": "1577448922.437871",
    "rate_limit_status": 98,
    "rate_limit_reset_ms": 1580885703683,
    "rate_limit": 100
    }

TEST1 = {
        "var_entero": 1,
        "var_dict": {
            "a":0,
            "b":2,
        },
        "var_list": [
            {
                "esp": "jjj",
                "foo": True
            },
        ]
    }

TEST2 = {
        "var_entero": 1,
        "var_dict": {
            "a":0,
            "b":2,
        },
        "var_list": [
            {
                "esp": "jjj",
                "foo": True,
                "another":[
                    {"a":False},
                    "b",
                ]
            },
        ]
    }

