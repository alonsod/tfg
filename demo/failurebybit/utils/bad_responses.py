#!/usr/bin/env python3

from random import random

HUNDRED_PER = 100

def get_bad_response(prob):
    """
    return True if random float between [0, 1)
    is lower than prob.
    If prob is expressed in percent format (int), is
    converted to float type.
    If prob is not number, return False
    """
    if not isinstance(prob, float) and not isinstance(prob, int):
        return False
    if isinstance(prob, int):
        return random() <= (prob / HUNDRED_PER)
    return random() <= prob

def main():
    print(get_bad_response(50))

if __name__ == "__main__":
    main()