#!/usr/bin/env python3

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

# python3 #
import time
import hmac
import requests
import urllib
import json
import sys

# my packages #
from misc import expand_tilde
from misc import mprint
from misc import timedelta
from misc import human
import iso8601
from utils.check_server_resp import is_similar
from utils.good_server_reponses import *
from utils.bad_responses import get_bad_response

TESTNET = True  # if operating with mainnet -> TESTNET = False

REQUEST_EXPIRATION = 1000   # milliseconds
RECV_WINDOW = 5000  # milliseconds

REQUEST_TIMEOUT = 1.2       # seconds
'''
# This value could change depending latency or server time response:
# If REQUEST_TIMEOUT = 0.7 seconds ó or lower, sometimes we get timeout:
HTTPSConnectionPool(host='api-testnet.bybit.com', port=443):Read timed
out. (read timeout=0.7)
So this value should be bigger but not too big to avoid security issues.
'''

INITIAL_ATTEMPTS = 12
# number of attempts trying to do a request to server. sleep between attempts

MAX_SLEEP = 60 * 15
# When we get an error from the exchange, we wait 3, 6, 12, 24...
# seconds until sending a new request. Doubling the sleep time.
# Until this sleep time reaches this maximum, after that we always
# sleep the maximum.

SYMBOLS = ["BTCUSD", "ETHUSD", "EOSUSD", "XRPUSD"]

PROB_ERROR = 0.60   # can be int value [0, 100) or float value [0, 1)

class ByBit(object):
    def __init__(self, keyfile, log):
        self.log = log
        self.key, self.secret = self._read_keyfile(keyfile)
        self.symbol = SYMBOLS[0]
        self.base_url = "https://api-testnet.bybit.com"

        if not TESTNET:
            self.base_url = "https://api.bybit.com"

        tick_size = self.get_tick_size_from_symbol()
        if tick_size < 0:
            msg = "Error: cannot get tick_size\n"
            self.log.write(msg)
            mprint(msg)
            sys.exit(1)

        self.tick_size = tick_size

    def _read_keyfile(self, key_file):
        key_file = expand_tilde(key_file)
        try:
            with open(key_file, 'r') as f:
                lines = f.readlines()
                key = lines[0]
                secret = lines[1]
                key = key.strip()
                secret = secret.strip()
                t = "Using key_file {}\n"
                msg = t.format(key_file)
                self.log.write(msg)

                return key, secret
        except IOError:
            msg = "Can not open "+str(key_file)+"\n"
            mprint(msg)
            self.log.write(msg)
            raise Exception(msg)

    def are_we_synced_w_server(self):
        """
        return True if we are synced with server timestamp
        RECV_WINDOW by default is 5000 ms but can be edited
        """
        r = {}
        r = self.get_server_timestamp()
        if r["status"] != "ok":
            self.print_error(r)
            return False
        server_time = r["msg"]["time_now"]
        server_time = float(server_time)*1000
        mytime = float(time.time()*1000)
        if server_time - RECV_WINDOW <= mytime < server_time + 1000:
            msg = "We are synced with server\n"
            self.log.write(msg)
            mprint(msg)
            return True
        r["status"] = "Syncronization_error"
        msg = "Warning: we are not synced with server. "
        msg += "server_time = "+str(server_time)+", "
        msg += "mytime = "+str(mytime)
        r["msg"] = msg
        self.print_error(r)
        return False

    def get_signature(self, secret: str, req_params: dict):
        """
        https://github.com/bybit-exchange/api-connectors/blob/master/encryption_example/Encryption.py
        :param secret    : str, your api-secret
        :param req_params: dict, your request params
        :return: signature
        """
        _val = '&'.join([str(k)+"="+str(v) for k, v in \
            sorted(req_params.items()) if (k != 'sign') and (v is not None)])

        bytes_secret = bytes(secret, "utf-8")
        bytes_val = bytes(_val, "utf-8")
        myhmac = hmac.new(bytes_secret, bytes_val, digestmod="sha256")
        return str(myhmac.hexdigest())

    def place_active_order(self, **kwargs):
        r = {}
        side = kwargs.get("side", None)
        symbol = kwargs.get("symbol", self.symbol)
        order_type = kwargs.get("order_type", None)
        qty = kwargs.get("qty", None)
        time_in_force = kwargs.get("time_in_force", "GoodTillCancel")

        if order_type is None:
            r["status"] = "error_params"
            msg = "order_type not defined\n"
            r["msg"] = msg
            self.print_error(r)
            return r

        if order_type == "Market":
            r = self._place_market_order(side, symbol, qty,
                                     time_in_force)
        elif order_type == "Limit":
            price = kwargs.get("price", None)
            r = self._place_limit_order(side, symbol, qty,
                                     price, time_in_force)
        else:
            r["status"] = "error_params"
            msg = "order_type must be \"Market\" or \"Limit\", "
            msg += "not "+str(order_type)+"\n"
            r["msg"] = msg
            self.print_error(r)
        return r

    def _place_limit_order(self, side, symbol, qty, price, time_in_force):
        r = {}
        if self._are_limit_params_ok(side, qty, price):
            verb = "POST"
            path = "/v2/private/order/create"
            params = {
                "api_key": self.key,
                "side": side,
                "timestamp": int(time.time()*1000),  # in ms
                "price": price,
                "symbol": symbol,
                "order_type": "Limit",
                "qty": qty,
                "time_in_force": time_in_force,
            }
            params = self._get_sorted_dict(params, "key")
            params.update({"sign": self.get_signature(self.secret, params)})
            r = self._send_request(
                verb=verb,
                path=path,
                params=params
            )
            msg = is_similar(LIMIT_ORDER_EXAMPLE, r["msg"])
            if msg != "ok":
                r["status"] = "Server_reply_error"
                r["msg"] = msg
                self.print_error(r)
                return r
            # checking reponse status. If error, status = ret_msg
            if r["msg"]["ret_msg"].lower() != "ok":
                r["status"] = r["msg"]["ret_msg"]
        else:
            r["status"] = "error_params"
            msg = "Limit order params: side = "+str(side)
            msg += ", symbol = "+str(symbol)
            msg += ", qty = "+str(qty)
            msg += ", price = "+str(price)
            msg += ", time_in_force = "+str(time_in_force)+"\n"
            r["msg"] = msg
            self.print_error(r)
        return r

    def _are_limit_params_ok(self, side, qty, price):
        """
        return True if price is not None and
        fills market params and is good price
        """
        if price is None:
            msg = "Error in place_active_order: "
            msg += "price is not defined\n"
            self.log.write(msg)
            mprint(msg)
            return False
        cond1 = self._are_market_params_ok(side, qty)
        cond2 = self._is_good_price(price)
        return cond1 and cond2

    def _is_good_price(self, price):
        """
        return True if price:
        - is int or float
        - price % tick_size = 0
        - 0 < price < 10^6
        - price > market_price * 0.1
        False in other case
        """
        if not (isinstance(price, int) or isinstance(price, float)):
            msg = "Error: price is "+str(price)+": must be int or float\n"
            mprint(msg)
            self.log.write(msg)
            return False
        if price % self.tick_size != 0:
            msg = "Error: price % tick_size != 0\n"
            mprint(msg)
            self.log.write(msg)
            return False
        if price >= 1000000 or price < 0:
            msg = "Error: price value is "+str(price)
            msg += ": must be between 0 and 1000000\n"
            mprint(msg)
            self.log.write(msg)
            return False
        market_price = self.get_market_price()
        if market_price > 0 and price <= (market_price * 0.1):
            msg = "Error: price value is "+str(price)
            msg += ": must be bigger than 0.1 * market_price\n"
            mprint(msg)
            self.log.write(msg)
            return False
        return True

    def _place_market_order(self, side, symbol, qty, time_in_force):
        r = {}
        if self._are_market_params_ok(side, qty):
            verb = "POST"
            path = "/v2/private/order/create"
            params = {
                "api_key": self.key,
                "side": side,
                "timestamp": int(time.time()*1000),  # in ms
                "symbol": symbol,
                "order_type": "Market",
                "qty": qty,
                "time_in_force": time_in_force,
            }
            params = self._get_sorted_dict(params, "key")
            params.update({"sign": self.get_signature(self.secret, params)})
            # print(params)
            r = self._send_request(
                verb=verb,
                path=path,
                params=params
            )
            msg = is_similar(MARKET_ORDER_EXAMPLE, r["msg"])
            if msg != "ok":
                r["status"] = "Server_reply_error"
                r["msg"] = msg
                return r
            # checking reponse status. If error, status = ret_msg
            if r["msg"]["ret_msg"].lower() != "ok":
                r["status"] = r["msg"]["ret_msg"]
        else:
            r["status"] = "error_params"
            msg = "Market order params: side = "+str(side)
            msg += ", symbol = "+str(symbol)
            msg += ", qty = "+str(qty)
            msg += ", time_in_force = "+str(time_in_force)+"\n"
            r["msg"] = msg
            self.print_error(r)
        return r

    def _are_market_params_ok(self, side, qty):
        if side != "Buy" and side != "Sell":
            msg = "Side must be \"Buy\" or \"Sell\", not \""+str(side)+"\"\n"
            self.log.write(msg)
            mprint(msg)
            return False
        if type(qty) != int or qty > 1000000 or qty < 0:
            msg = "qty must be int, and 0 < qty <= 1000000, "
            msg += "not "+str(qty)+"\n"
            self.log.write(msg)
            mprint(msg)
            return False
        return True

    def get_market_price(self):
        """
        calls get_ticker
        return actual market_price (float)
        if not found in server's reponse, returns -1 (int)
        if another error ocurred try again
        """
        i = 0
        while True:
            r = self.get_ticker()
            if r["status"] == "ok":
                for result in r["msg"]["result"]:
                    price = result["mark_price"]
                    return float(price)
            else:
                self.print_error(r)
                self.sleep(i)
                i += 1
        return r

    def get_ticker(self):
        """
        return ticker from symbol
        """
        r = {}
        verb = "GET"
        path = "/v2/public/tickers"
        params = {
            "api_key": self.key,
            "symbol": self.symbol,
            "timestamp": int(time.time()*1000),  # in ms
        }
        params = self._get_sorted_dict(params, "key")
        params.update({"sign": self.get_signature(self.secret, params)})
        r = self._send_request(
            verb=verb,
            path=path,
            params=params
        )
        msg = is_similar(TICKER_EXAMPLE, r["msg"])
        if msg != "ok":
            r["status"] = "Server_reply_error"
            r["msg"] = msg
            return r
        # checking reponse status. If error, status = ret_msg
        if r["msg"]["ret_msg"].lower() != "ok":
            r["status"] = r["msg"]["ret_msg"]
        return r

    def get_tick_size_from_symbol(self):
        """
        calls _get_query_symbol to get self.symbol's tick_size
        all tick_sizes are float
        return float tick_size or -1.0 if error ocurred
        """
        r = self._get_query_symbol()
        if r["status"] == "ok":
            msg = r["msg"]
            for x in msg["result"]:
                if x["name"] == self.symbol:
                    return float(x["price_filter"]["tick_size"])
                msg = "Error in get_tick_size_from_symbol: Not found tick_size"
                msg += "\n"
                self.log.write(msg)
                mprint(msg)
        self.print_error(r)
        return(float(-1))

    def _get_query_symbol(self):
        """
        return all symbols info
        """
        r = {}
        verb = "GET"
        path = "/v2/public/symbols"
        params = {
            "api_key": self.key,
            "timestamp": int(time.time()*1000),  # in ms
        }
        params = self._get_sorted_dict(params, "key")
        params.update({"sign": self.get_signature(self.secret, params)})
        r = self._send_request(
            verb=verb,
            path=path,
            params=params
        )
        msg = is_similar(SYMBOLS_EXAMPLE, r["msg"])
        if msg != "ok":
            r["status"] = "Server_reply_error"
            r["msg"] = msg
            return r
        # checking reponse status. If error, status = ret_msg
        if r["msg"]["ret_msg"].lower() != "ok":
            r["status"] = r["msg"]["ret_msg"]
        return r

    def cancel_all_active_orders(self):
        """
        Cancel all active orders that are unfilled
        or partially filled. Fully filled
        orders cannot be cancelled
        """
        i = 0
        while True:
            r = self._ll_cancel_all_active_orders()
            if r["status"] == "ok":
                break
            else:
                self.print_error(r)
                self.sleep(i)
                i += 1
        return r

    def _ll_cancel_all_active_orders(self):
        r = {}
        verb = "POST"
        path = "/v2/private/order/cancelAll"
        params = {
            "api_key": self.key,
            "symbol": self.symbol,
            "timestamp": int(time.time()*1000),  # in ms
        }
        params = self._get_sorted_dict(params, "key")
        params.update({"sign": self.get_signature(self.secret, params)})
        r = self._ll_send_request(
            verb=verb,
            path=path,
            params=params
        )
        msg1 = is_similar(CANCEL_ALL_ACT_ORD, r["msg"])
        msg2 = is_similar(CANCEL_ALL_ACT_ORD2, r["msg"])
        if msg1 != "ok" and msg2 != "ok":
            r["status"] = "Server_reply_error"
            r["msg"] = msg
            return r
        # checking reponse status. If error, status = ret_msg
        if r["msg"]["ret_msg"].lower() != "ok":
            r["status"] = r["msg"]["ret_msg"]
        return r

    def _are_dict_values_str(self, **kwargs):
        """
        return True only if all values in dict are string
        """
        for key, value in kwargs.items():
            if type(value) != str:
                msg = "Value "+str(value)+" in key "+str(key)+" must be string"
                msg += ", not "+str(type(value))+"\n"
                self.log.write(msg)
                mprint(msg)
                return False
        self.log.write("Values are ok!\n")
        return True

    def _are_cancel_active_order_kwargs_ok(self, **kwargs):
        '''
        return True if following conditions are fulfilled:
        kwargs = {symbol="BTCUSD", order_id="string"}
        or
        kwargs = {symbol="BTCUSD", order_link_id="string"}
        '''
        is_set_symbol = "symbol" in kwargs
        is_set_oid = "order_id" in kwargs
        is_set_olid = "order_link_id" in kwargs
        cond1 = is_set_symbol and (is_set_oid or is_set_olid)
        cond2 = not (is_set_oid and is_set_olid)
        if cond1 and cond2:
            msg = "Checking if cancel_active_order values are string\n"
            self.log.write(msg)
            return self._are_dict_values_str(**kwargs)
        error = "Bad args in cancel_active_order. "
        error += "We need kwargs = {symbol=\"BTCUSD\", order_id=\"\"} or "
        error += "kwargs = {symbol=\"BTCUSD\", order_link_id=\"\"}. "
        error += "We have kwargs = "+str(kwargs)+"\n"
        self.log.write(error)
        mprint(error)
        return False

    def cancel_active_order(self, **kwargs):
        """
        cancel an active order by order_id or order_link_id
        Either order_id or order_link_id are required for cancelling active
        orders. order_id - this unique 36 characters order ID was returned
        to you when the active order was created successfully.

        You may cancel active orders that are unfilled or partially filled.
        Fully filled orders cannot be cancelled.
        """
        i = 0
        while True:
            r = self._ll_cancel_active_order(**kwargs)
            if r["status"] == "ok":
                break
            if r["status"] == "error_bad_args":
                self.print_error(r)
                sys.exit(1)
            if r["status"] == "order not exists or Too late to cancel":
                self.print_error(r)
                break
            if r["status"] == "Order already cancelled":
                self.print_error(r)
                break
            else:
                self.print_error(r)
                self.sleep(i)
                i += 1
        return r

    def _ll_cancel_active_order(self, **kwargs):
        r = {}
        verb = "POST"
        path = "/v2/private/order/cancel"
        params = {
            "api_key": self.key,
            "symbol": self.symbol,
            "timestamp": int(time.time()*1000),  # in ms
        }
        if self._are_cancel_active_order_kwargs_ok(**kwargs):
            for key, value in kwargs.items():
                if key == "order_id" or key == "order_link_id":
                    msg = "Canceling order "+str(value)+"\n"
                    mprint(msg)
                    self.log.write(msg)
                    params[key] = value
                    break
            params = self._get_sorted_dict(params, "key")
            params.update({"sign": self.get_signature(self.secret, params)})
            r = self._send_request(
                verb=verb,
                path=path,
                params=params
            )
            # checking reponse status. If error, status = ret_msg
            if r["msg"]["ret_msg"].lower() != "ok":
                r["status"] = r["msg"]["ret_msg"]
        else:
            r["status"] = "error_bad_args"
            r["msg"] = str("Error in cancel_active_order: bad args: " +\
                          str(kwargs)+"\n")
        return r

    def get_positions(self):
        i = 0
        while True:
            r = self._ll_get_positions()
            if r["status"] == "ok":
                break
            else:
                self.print_error(r)
                self.sleep(i)
                i += 1
        return r

    def _ll_get_positions(self):
        verb = "GET"
        path = "/open-api/order/list"
        params = {
            "api_key": self.key,
            "timestamp": int(time.time()*1000)  # in ms
        }
        params = self._get_sorted_dict(params, "key")
        params.update({"sign": self.get_signature(self.secret, params)})
        r = self._send_request(
            verb=verb,
            path=path,
            params=params
        )
        msg = is_similar(ALL_ORDERS_LIST, r["msg"])
        if msg != "ok":
            r["status"] = "Server_reply_error"
            r["msg"] = msg
            return r
        # checking reponse status. If error, status = ret_msg
        if r["msg"]["ret_msg"].lower() != "ok":
            r["status"] = r["msg"]["ret_msg"]
        return r

    def _get_sorted_dict(self, mydict, sortby):
        '''
        by default sorts dict by key. If sortby == "value", sorts by value
        return sorted dict
        '''
        item_pos = 0
        if sortby == "value":
            item_pos = 1
        return {k: v for k, v in sorted(mydict.items(),
                                    key=lambda item: item[item_pos])}

    def get_orderbook(self):
        i = 0
        while True:
            r = self._ll_orderbook()
            if r["status"] == "ok":
                break
            else:
                self.print_error(r)
                self.sleep(i)
                i += 1
        return r

    def _ll_orderbook(self):
        """
        returns raw orderbook (low level)
        """
        verb = "GET"
        path = "/v2/public/orderBook/L2"
        params = {}
        params["symbol"] = self.symbol

        r = self._send_request(
            verb=verb,
            path=path,
            params=params
        )
        if get_bad_response(PROB_ERROR):
            r["msg"] = {}
        msg = is_similar(ORDERBOOK_SERVER_RESPONSE, r["msg"])
        if msg != "ok":
            r["status"] = "Server_reply_error"
            r["msg"] = msg
            return r
        # checking reponse status. If error, status = ret_msg
        if r["msg"]["ret_msg"].lower() != "ok":
            r["status"] = r["msg"]["ret_msg"]
        return r

    def print_orderbook(self, raw_orderbook):
        '''
        receives raw_orderbook where raw_orderbook = get_orderbook()
        raw_orderbook dict fields are:
        'ret_code'
        'ret_msg'
        'ext_code'
        'ext_info'
        'result': dicts list: {'symbol', 'price', 'size', 'side'}

        print pretty ordebook
        '''
        if raw_orderbook["status"] != "ok":
            self.log.write("Cannot print orderbook")
            return
        info = ""
        info += "Symbol: "+self.symbol+"\n"
        info += "price | size\n"
        prev_side = ""
        for elem in raw_orderbook["msg"]["result"]:
            if len(prev_side) <= 0:
                prev_side = elem['side']
            if prev_side != elem['side']:
                info += ">--------------<\n"
                prev_side = elem['side']
            prize = float(elem['price'])
            size = float(elem['size'])
            info += "{:.1f} {:8.5f}".format(prize, size)+"\n"
        print(info)
        return

    def get_server_timestamp(self):
        '''
        return reponse object with server timestamp
        timestamp = r["msg"]["time_now"])*1000
        r["status"] -> if request is succesfully or not
        r["msg"] -> server data info or descriptive error
        '''
        r = {}
        verb = "GET"
        path = "/v2/public/time"
        params = {}

        r = self._send_request(
            verb=verb,
            path=path,
            params=params
        )
        msg = is_similar(TIMESTAMP_SERVER_RESPONSE, r["msg"])
        if msg != "ok":
            r["status"] = "Server_reply_error"
            r["msg"] = msg
            return r

        if r["msg"]["ret_msg"].lower() != "ok":
            r["status"] = r["msg"]["ret_msg"]

        return r

    def logarithmic_sleep(self, i):
        s = min(MAX_SLEEP, 2 * (2 ** i))
        t = "I will retry in {}\n"
        msg = t.format(timedelta(s))
        mprint(msg)
        time.sleep(s)
        return

    def arithmetical_sleep(self, i):
        t = "I will retry in {}\n"
        msg = t.format(timedelta(i))
        mprint(msg)
        time.sleep(i)
        return

    def sleep(self, i):
        if i < INITIAL_ATTEMPTS:
            self.arithmetical_sleep(i)
        else:
            self.logarithmic_sleep(i-INITIAL_ATTEMPTS)
        return

    def _send_request(self, verb, path, params, data=""):
        i = 0
        while True:
            r = self._ll_send_request(
                verb=verb,
                path=path,
                params=params,
                data=data
            )

            if r["status"] == "ok":
                break
            # elif r["msg"].find("insufficient") != -1:
            #     r["status"] = "no_balance"
            #     break
            else:
                r["status"] = "Error sending request"
                self.print_error(r)
                self.sleep(i)
                i += 1
        return r

    def print_error(self, r):
        t = "[bybit] {} {}\n"
        msg = t.format(human(time.time()), r["status"])
        msg += str(r["msg"])
        msg += "\n"
        mprint(msg)
        self.log.write(msg)

    def _ll_send_request(self, verb, path, params, data=""):
        params_encoded = urllib.parse.urlencode(params)
        path_with_params = path
        if len(params_encoded) != 0:
            path_with_params += "?" + params_encoded

        full_url = self.base_url + path_with_params

        try:
            if verb == "GET":
                r = requests.get(
                    full_url,
                    timeout=REQUEST_TIMEOUT
                )
            elif verb == "POST":
                r = requests.post(
                    full_url,
                    timeout=REQUEST_TIMEOUT
                )
            else:
                t = "Wrong verb: {}\n"
                msg = t.format(verb)
                self.log.write(msg)
                raise Exception(msg)

            r = self._check_response(r)
        except Exception as e:
            r = {}
            r["status"] = "error"
            r["msg"] = str(e)

        return r

    def _check_response(self, resp):
        try:
            msg = json.loads(resp.text)
            json_decoded = True
        except ValueError:
            msg = "No JSON object could be decoded\n"
            json_decoded = False
            self.log.write(msg)

        if not json_decoded:
            status = "error"
        elif resp.status_code == 200 or resp.status_code == 201:
            status = "ok"
        else:
            status = "error"
            t = "{} http error {} {}\n"
            msg = t.format(
                human(time.time()),
                resp.status_code,
                self._decode_error_text(resp.text)
            )

            mprint(msg)
            self.log.write(msg)
        r = {}
        r["status"] = status
        r["msg"] = msg

        return r

    def _decode_error_text(self, text):
        try:
            as_dict = json.loads(text)
            r = as_dict["ret_msg"]
        except:
            r = text
        return r
