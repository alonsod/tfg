#!/usr/bin/env python3

from os.path import join

KEY_PATH = "."
KEY_FILE = join(KEY_PATH, "alonsod-bybit-testnet.key")