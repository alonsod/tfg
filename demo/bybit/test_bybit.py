#!/usr/bin/env python3

## python3 ##
import time
import json
import requests
from os import getpid
from os.path import join

## my packages ##
import bybit3
import mlogs
from config import KEY_FILE


def main():

    bb = bybit3.ByBit(
    keyfile = KEY_FILE,
    log=get_log()
    )
    
    input("Obtener el timestamp del servidor:")
    test_get_server_timestamp(bb)

    
    input("\nSaber si estamos sincronizados:")
    test_are_we_synced_w_server(bb)

    
    input("\nObtener una firma con la clave de usuario:")
    test_get_signature(bb)
    time.sleep(0.5)
    input("\nDonde cada firma es unica, como podemos ver (pulsa intro de nuevo):")
    test_get_signature(bb)
    
    input("\nPeticion sin autenticación (orderbook):")
    test_get_orderbook(bb)
    
    input("\nSi queremos el orderbook de forma \"readable\":")
    test_print_orderbook(bb)
    
    input("\nPeticion con autenticacion (Obtener el histórico de posiciones):")
    test_get_positions(bb)
    
    input("\nPoner una orden al limite:")
    test_place_limit_order(bb)
    
    input("\nSi ponemos mal los parámetros en la peticion de orden al limite:")
    test_bad_place_limit_order1(bb)

    input("\nPulsa intro para otra peticion de orden al limite invalida:")
    test_bad_place_limit_order2(bb)
    input("\nPara una informacion mas detallada se puede consultar el fichero de logs correspondiente.")
    
    input("\nPara cancelar todas las ordenes activas:")
    test_cancel_all_active_orders(bb)
    
    input("\nEn el caso de querer cancelar una en concreto, es posible pasarle el id de la orden:")
    print("Para ello, ponemos una orden al limite, cogemos el id y la cancelamos\n")
    test_place_limit_order(bb)
    oid = input("\nIntroduce el id:")
    cancelled = test_cancel_active_order(bb, oid)
    while not cancelled:
        oid = input("\nIntroduce el id:")
        cancelled = test_cancel_active_order(bb, oid)
    input("\nObtener el precio de mercado actual:")
    test_get_ticker(bb)
    input("\nPoner una orden de mercado:")
    test_place_market_order(bb)
    print("\nEste tipo de órdenes no se pueden cancelar, habría que ir a la páginda del exchange directamente")
    return 0

def test_bad_place_limit_order2(bb):
    r = bb.place_active_order(
        side="Buy",
        symbol="BTCUSD",
        order_type="Limit",
        qty=2000000000,
        price=9000,
        time_in_force="GoodTillCancel",
    )
    print(r)

def test_bad_place_limit_order1(bb):
    r = bb.place_active_order(
        side="Buy",
        symbol="BTCUSD",
        qty=20,
        price=9000,
        time_in_force="GoodTillCancel",
    )
    print(r)

def test_place_limit_order(bb):
    r = bb.place_active_order(
        side="Buy",
        symbol="BTCUSD",
        order_type="Limit",
        qty=20,
        price=9000,
        time_in_force="GoodTillCancel",
    )
    print(r)

def test_place_market_order(bb):
    r = bb.place_active_order(
        side="Sell", # "Buy" or "Sell"
        symbol="BTCUSD", # "BTCUSD", "ETHUSD", "EOSUSD", "XRPUSD"
        order_type="Market", # "Market" or "Limit"
        qty=22, # Must be integer and Max quantity = 1000000 (1 million)
        time_in_force="GoodTillCancel"
    )
    print(r)

def _decode_error_text(bb):
    # r = {'ret_code': 0, 'ret_msg': {'OK':1, "eee":True}}
    r = {'ret_code': 0, 'ret_msg': 'OK'}
    r = json.dumps(r)
    print(bb._decode_error_text(r))

def test_is_good_price(bb):
    price = 960
    print(bb._is_good_price(price))

def test_get_market_price(bb):
    price = bb.get_market_price()
    print(type(price))
    print(price)

def test_get_ticker(bb):
    r = bb.get_ticker()
    print(r)

def test_get_tick_size_from_symbol(bb):
    tick_size = bb.get_tick_size_from_symbol()
    print("tick_size: "+str(type(tick_size))+", value = "+str(tick_size))

def test_get_query_symbol(bb):
    r = bb._get_query_symbol()
    print(type(r))
    print(r)

def test_cancel_all_active_orders(bb):
    r = bb.cancel_all_active_orders()
    print(r)

def test_ll_cancel_all_active_orders(bb):
    r = bb._ll_cancel_all_active_orders()
    print(r)

def test_cancel_active_order(bb, oid):
    # r = bb.cancel_active_order(order_id="hola")
    # r = bb.cancel_active_order(order_id="hola", order_link_id="jeje")   # bad
    # r = bb.cancel_active_order(aaaa="BTCUSD", order_id="22222")
    # r = bb.cancel_active_order()
    # r = bb.cancel_active_order(symbol="BTCUSD", order_link_id=22222)
    # r = bb.cancel_active_order(symbol="BTCUSD", order_id="22b351af-e026-4db6-8266-c29b8fbec173")
    # r = bb.cancel_active_order(symbol="BTCUSD", order_id="2793d4d6-966d-4bcd-9a4c-8281a917e3cf")
    r = bb.cancel_active_order(symbol="BTCUSD", order_id=str(oid))
    print(r)
    if r["status"] != "ok":
        return False
    return True

def test_check_params_cancel_active_order(bb):
    # print(bb._are_cancel_active_order_kwargs_ok(order_id="hola"))
    # print(bb._are_cancel_active_order_kwargs_ok(symbol="BTCUSD", order_id="hola", order_link_id="pepe"))
    # print(bb._are_cancel_active_order_kwargs_ok(symbol="BTCUSD", order_id=2222))
    # print(bb._are_cancel_active_order_kwargs_ok(aaaa="BTCUSD", order_id="22222"))
    # print(bb._are_cancel_active_order_kwargs_ok(order_id="22222"))
    print(bb._are_cancel_active_order_kwargs_ok(symbol="BTCUSD", order_id="22222"))

def test_get_positions(bb):
    r = bb.get_positions()
    # print(r)
    print(type(r["msg"]))
    print(json.dumps(r["msg"], indent=2))

def test_print_orderbook(bb):
    r = bb.get_orderbook()
    bb.print_orderbook(r)
        

def test_get_orderbook(bb):
    r = bb.get_orderbook()
    print(r)

def test_ll_orderbook(bb):
    r = bb._ll_orderbook()
    print(r)

def test_get_server_timestamp(bb):
    r = bb.get_server_timestamp()
    print(r)

def test_send_request(bb):
    verb = "GET"
    path = "/v2/public/orderBook/L2"
    params = {}
    params["symbol"] = "BTCUSD"
    reponse = bb._send_request(
        verb = verb,
        path = path,
        params = params
        )
    print(reponse)

def test_sleep(bb):
    bb.sleep(20)

def test_readkey(bb, key_file):
    key, secret = bb._read_keyfile(key_file)
    print("Key: "+str(key))
    print("Secret: "+str(secret))

def test_are_we_synced_w_server(bb):
    print(bb.are_we_synced_w_server())

def test_get_signature(bb):
    params = {}
    params['api_key'] = bb.key
    params['leverage'] = 100
    params['symbol'] = bb.symbol
    params['timestamp'] = int(time.time()*1000)
    # print(params)
    signature = bb.get_signature(
        secret = bb.secret,
        req_params = params
    )
    print(signature)

def get_log():
    t= "/tmp/log.test_bybit.{}.txt"
    dlogname= t.format(getpid())
    return mlogs.log(dlogname)
    
if __name__ == "__main__":
    main()