#!/bin/bash
apt-get update -y
export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"
sudo dpkg-reconfigure locales
apt-get install -y python-minimal
apt-get install -y python3
apt-get install -y python-pip
pip install requests
pip install pytz
pip install six
mkdir $HOME/mbot_data
export MBOT_DATA_DIR=$HOME/mbot_data
