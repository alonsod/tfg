#!/usr/bin/env python 

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

# check_time_limits.py  Miguel April 2015 - Ag 2018
# Checks first and last date in a file with prices

import sys
import optparse
import os
import datetime
from misc import mprint
from misc import mvprint
import misc 
from misc import human
from misc import mprint
import time


def reset_glo():
    glo={}
    glo['input_file']="october.csv"
    glo['input_file']="btcnCNY.csv"

    home=os.path.expanduser("~")
    path=os.path.join(home,"bot_backtesting")
    glo['log_file']=os.path.join(path,"log.backtester.txt")
    glo["linux_data_dir"]="/home/store/data"
    glo["macos_data_dir"]="/Users/store/data"
    glo["gsyc_lab_data_dir"]="/home/profes/gsyc/mortuno/data"

    # When the age of data is >= this value, it is presented red, not
    # green
    
    glo["platform"]=misc.check_platform()
    if glo["platform"]=="linux":
        glo["data_dir"]=glo["linux_data_dir"]
    elif glo["platform"]=="macos":
        glo["data_dir"]=glo["macos_data_dir"]
    else:
         raise Exception("wrong platform")
    return glo


def parse_args(glo):
    usage = " %prog prices_file"
    parser = optparse.OptionParser(usage)
    (options, arguments) = parser.parse_args()
    if len(arguments) == 0:
        glo["input_file"]=os.path.join(glo["data_dir"],glo["input_file"])
    else:
        glo["input_file"]=arguments[0]
    return


def parse_line(line):
    """
    FIXME!
    this function is duplicated, a cythonized version is found also in 
    backtester_core  (this one is standard python)
    """
    items = line.split(',')
    if len(items) != 3:
        msg = "Wrong line format in csv line\n" + line
        raise ValueError(msg)

    try:
        date = float(items[0])
    except ValueError:
        template = "Wrong format:\n{}\nexpecting date, bid, volume"
        msg = template.format(line)
        raise ValueError(msg)
    try:
        bid = float(items[1])
        volume = float(items[2])
    except ValueError:
        template = "Wrong format:\n{}\nexpecting date, bid, volume"
        msg = template.format(line)
        raise ValueError(msg)

    return date, bid, volume


def get_time_from_line(line):
    date,_,_=parse_line(line)
    return date


def file_extremes(name):
    # Returns first and last line of file f, excluding lines starting with
    # hash symbol
  
    f = open(name, "r")
    first_time = True
    for  line in f:
        if line[0] == "#":
            continue 
        if first_time:
            first_line = line
            first_time = False
        last_line = line

    f.close()

    return first_line,last_line


def first_data_line(name):
    # Returns first line of file f, excluding lines starting with hash symbol
    # Does not consider the case that all lines ares hashes
  
    f = open(name, "r")
    for line in f:
        if line[0] != "#":
            break

    f.close()
    return line


def temporal_limits(f):
    first_line,last_line=file_extremes(f)
    t1= get_time_from_line(first_line) 
    t2= get_time_from_line(last_line) 
    return t1,t2


def starting_time(f):
    first_line=first_data_line(f)
    t1= get_time_from_line(first_line) 
    return t1


def data_age_formatted(f):

    t1, t2 = temporal_limits(f)
    t = "{} contains data\nfrom {} to {}\n"
    msg = t.format(f, human(t1), human(t2))
    
    age = time.time() - t2

    t = "Age of data: {}\n"
    msg2 = t.format( misc.timedelta(age))
    msg2 = misc.age_add_color(msg2, age)
    msg += msg2

    return age, msg


def main():
    glo=reset_glo()
    parse_args(glo)
    
    f=glo["input_file"]
    print("input file:"+f)

    if not os.path.exists(f):
        msg=str(f)+" does not exist"
        raise Exception(msg)

    t1,t2=temporal_limits(f)
    if t1==None or t2==None:
        msg="Wrong format in file "+str(f)
        raise Exception(msg)
    else:
        print(misc.human(t1))
        print(misc.human(t2))


if __name__ == "__main__":
    main()



