#!/usr/bin/env python 
# -*- coding: utf-8 -*-

#!/usr/bin/python -tt

# bitmex.py   Miguel Jun 2018 - Ag 2019

# See bitmex.readme.txt for details

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

import sys
import requests
import json
import misc
import time
import base64
import hashlib
import hmac
import urllib
import os
#import random
from misc import superprint
from misc import human
from misc import mprint
import check_time_limits

import smart_ex
import mlogs
import check_bitmex_obook

SECS_IN_A_DAY = 86400

TIME_INTER_CHECKS_MARGIN = 60 * 5  # seconds. 
# This value is dynamically updated, check the source

TIME_INTER_CHECK_TICKER = 60
TIME_INTER_CHECK_WALLET = 30
# To avoind asking the same thing too ofter


MAX_SLEEP = 60 * 15
# When we get an error from the exchange, we wait 3, 6, 12, 24...
# seconds until sending a new request. Doubling the sleep time.
# Until this sleep time reaches this maximum, after that we always
# sleep the maximum.

JUST_IN_CASE = 0.998
# We apply this factor to computed max_position_size, to avoid
# different rounding making our order too big.
# This was usefull in easly version of this code. But then, we added a
# feature that reduces order size if it is too big, until size is ok.
# So, it is not needed anymore, but does not harm, so we still use it, 
# with very small reduction.

REDUCE_SOMETHING = 0.9
# If we get a "Account has insufficient Available Balance" error, we try 
# again, multiplying the order by this factor

# params for smart_ex
P1_DEFAULT = 15
P2_DEFAULT = 0.2
P3_DEFAULT = 25
P4_DEFAULT = 0

P5_DEFAULT = 1500


DEFAULT_TRAILING = 7
# default value for trailing stop orders, as percentage


class Trade(object):
    direction = 0
    # 0: no trade, out of market
    # 1:  going long, bull trade, buying
    # -1: going short, bear trade, selling


class Bitmex(object):
    def __init__(
            self,
            sg, 
            key_file,
            log, 
            leverage,
            symbol='BTCUSD', 
            testnet = False,
            botid = "not_set",
            p1 = P1_DEFAULT,
            p2 = P2_DEFAULT,
            p3 = P3_DEFAULT,
            p4 = P4_DEFAULT,
            p5 = P5_DEFAULT
        ):

        self.sg = sg
        self.leverage = leverage
        self.name = "bitmex"
        self.exchange = self.name
        self.log = log

        self.ck = check_bitmex_obook.CheckBitmexObook()
       
        if botid.find(".") != -1:
            t = "botid ({}) is not allowed to include dot character"
            msg = t.format(botid)
            self.log.write(msg)
            raise Exception(msg)
       
        if len(botid) > 12:
            t = "botid ({}) length ({}) is > 12"
            msg = t.format(botid, len(botid))
            self.log.write(msg)
            raise Exception(msg)
        else:
            self.botid = botid
        self.key, self.secret = self._read_key(key_file)

        self.symbol = symbol
        if symbol == "BTCUSD":
            self.bc = "BTC"  # Base Currency
            self.qc = "USD"  # Quote Currency
            self.symbol_bitmex_notation = "XBTUSD"
        else:
            t = "Unknown symbol:{}\n"
            msg = t.format(symbol)
            self.log.write(msg)
            raise Exception(msg)

        self.decimals_bc = 8
        self.decimals_qc = 8

        if testnet == "auto":
            testnet = key_file.find("testnet") != -1

        if testnet :
            self.base_url ="https://testnet.bitmex.com"
            print "Testnet bitmex"
        else:
            self.base_url ="https://www.bitmex.com"
            print "Real bitmex, no testnet"

        self.trade = Trade()
        self.trade.direction = 0

        self.last_time_checked_margin = 0
        self.time_inter_checks_margin = TIME_INTER_CHECKS_MARGIN

        self.last_time_checked_wallet = 0
        self.last_wallet_seen = None


        # We do not check our trade margins continuously, it would mean to
        # ask the server the status of our account each few seconds,
        # it would have no sense. 


        self.last_ticker_seen  = None
        self.last_time_checked_ticker = 0

        self.smart_ex = smart_ex.SmartEx(
            self,
            p1 = p1,
            p2 = p2,
            p3 = p3,
            p4 = p4,
            p5 = p5
        )

        if leverage == None:
            print "Not changing leverage"
        else:
            print "Setting leverage to ",leverage
            self.set_leverage(leverage)
            
        # if the object is creatted for a ticker, it musn't change the leverage
        # set by a real trading bot

        variable_name = "MBOT_DATA_DIR"
        self.data_dir = os.getenv(variable_name)
        if self.data_dir == None:
            t = "{} environment variable not set"
            msg = t.format(variable_name)
            self.log.write(msg)
            raise Exception(msg)

        self.min_order_size_as_qc = 10  #  USD contracts
        # For bitmex, the real min value is 1 USD, but we will not waste time
        # with such small sizes

        self.min_order_size = None
        tic = self.ticker() #  We force a ticker here to init min_order_size
        price = tic["bid"]
        t = "Price: {}. Min order size: {:.6f} {} ({} {})\n"
        msg = t.format(
            price,
            self.min_order_size,
            self.bc,
            self.min_order_size_as_qc,
            self.qc
        )
        self.log.write(msg)

        return


    def _update_min_order_size(self, price):
        """
        Bitmex express min order size as qc, but our smart order class expects
        it as bc
        """

        self.min_order_size = float(self.min_order_size_as_qc) / price

        return


    def _read_key(self, key_file):
        """
        Reads exchange API secret key from a local file. For obvious security
        reasons, secret keys must not be included in source code.
        """
        key_file = misc.expand_tilde(key_file)
        try:
            f = open(key_file,'r')
        except IOError:
            msg = "Can not open "+key_file
            self.log.write(msg)
            raise Exception("Can not open "+key_file)
        lines  = f.readlines()
        key = lines[0]
        secret = lines[1]
        key = key.strip()
        secret = secret.strip()
        f.close()
        
        t = "Using key {}\n"
        msg = t.format(key_file)
        self.log.write(msg)

        return key, secret


    def _get_wallet_cache_name(self):
        """
        We use a file to cache wallet history. This function gets its name.
        """
        x = self.get_wallet_summary()
        if x ["status"] != "ok":
            return x
        account = x["account"]
        date = misc.unix_time_to_my_date_format(time.time())
        t = "bitmex.{}.{}.dat"
        r = t.format(
            account,
            date
        )
        r = os.path.join(self.data_dir, r)
        return r


    def _check_response(self,resp):
        try:
            msg = json.loads(resp.text)
            json_decoded = True
        except ValueError:
            msg =  "No JSON object could be decoded\n"
            json_decoded = False
            self.log.write(msg)
       
        if not json_decoded:
            status = "error"
        elif resp.status_code == 200 or resp.status_code == 201:
            status = "ok"
        else:  
            status = "error"
            t = "{} http error {} {}\n"
            msg = t.format(
                human(time.time()), 
                resp.status_code, 
                self._decode_error_text(resp.text)
            ) 

            mprint(msg)
            self.log.write(msg)
        r = {}
        r["status"] = status
        r["msg"] = msg
       
        return r


    def _decode_error_text(self, text):
        try:
            as_dict  = json.loads(text)
            r = as_dict["error"]["message"]
        except:
            r = text
        return r


    def _headers(self, verb, path, payload, expires, data):
        r = {}

        # path example: '/api/v1/instrument'
        # expires = int(round(time.time()) + 5)

        signature = self._generate_signature(
            secret=self.secret,
            verb=verb,
            path=path,
            expires=expires,
            data=data
         )

        r["api-expires"] = time.time()+10
        r["api-key"] = self.key
        r["api-signature"] = signature

        return r


    def sleep(self,i):
        s = min( MAX_SLEEP,  3 * (2 ** i) )
        t = "I will retry in {}\n"
        msg = t.format(misc.timedelta(s))
        mprint(msg)
        time.sleep(s)
        return


    def _extract_message(self, p):
        try:
            msg = p["message"]
        except:
            msg = p
        return msg


    def _get_signature(self, verb, path_with_params, expires, data):

        secret = self.secret
        if isinstance(data, (bytes, bytearray)):
            data = data.decode('utf8')
        message = verb + path_with_params + str(expires) + data 
        #print "Computing HMAC:" + message
    
        # original python3 version
        #signature = hmac.new(bytes(secret, 'utf8'), bytes(message, 'utf8'),\
        #    digestmod=hashlib.sha256).hexdigest()

        signature = hmac.new(secret, message, digestmod=hashlib.sha256)
        signature = signature.hexdigest() 
        return signature


    def _get_headers(self, expires, signature):
        r = {}
        r["api-expires"] = str(expires)
        r["api-key"] = self.key 
        r["api-signature"] = signature
        return r


    def _ll_orderbook(self, depth=10, normalized=True):
        """
        low level orderbook
        """

        symbol = self.symbol_bitmex_notation
 
        verb = "GET"
        path = "/api/v1/quote"
        path = "/api/v1/user"
        path = "/api/v1/orderBook/L2"
        params = {}
        params["symbol"] = symbol
        params["depth"] = depth

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )

        if r["status"] == "ok":
            right_syntax = self.ck.check(r["msg"], depth)
            if not right_syntax:
                r["status"] = "Wrong syntax in orderbook"

        if r["status"] == "ok" and normalized:
            r = self._normalize_orderbook(r["msg"])

        #if random.random() > 0.5:
        #    r["status"] = "Injecting test error"
        return r


    def orderbook(self, depth=10, normalized=True):

        i = 0
        while True:
            r = self._ll_orderbook( depth, normalized)
       
            if r["status"] == "ok":
                break
            else:
                t = "[bitmex      ]{} {}\n"
                msg = t.format( human(time.time()), r["status"])
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        return r


    def get_user_margin(self):

        symbol = self.symbol_bitmex_notation
 
        verb = "GET"
        path = "/api/v1/user/margin"
        params = {}
        params["symbol"] = symbol

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )

        return r


    def get_leverage(self):
        um = self.get_user_margin()

        if um["status"] == "ok":
            r = um["msg"]["marginLeverage"]
        else:
            print r["status"]
            r = "unknown"

        return r


    def get_trade_history(self):

        symbol = self.symbol_bitmex_notation
 
        verb = "GET"
        path = "/api/v1/execution/tradeHistory"
        params = {}

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
        if r["status"] == "ok":
            for item in r["msg"]:
                item["transactTime"] = \
                    misc.iso_to_unix(item["transactTime"])

        return r


    def get_execution(self):

        symbol = self.symbol_bitmex_notation
 
        verb = "GET"
        path = "/api/v1/execution"
        params = {}

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
        if r["status"] == "ok":
            for item in r["msg"]:
                item["transactTime"] = \
                    misc.iso_to_unix(item["transactTime"])
        return r


    def get_uncached_wallet_history(self):
        symbol = self.symbol_bitmex_notation
 
        verb = "GET"
        path = "/api/v1/user/walletHistory"
        params = {}
        params["symbol"] = symbol

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )

        if r["status"] != "ok":
            return r

        for item in r["msg"]:
            if item["timestamp"] != None:
                item["timestamp"] = misc.iso_to_unix(item["timestamp"])
            if item["transactTime"] != None:
                item["transactTime"] = misc.iso_to_unix(item["transactTime"])
      
        return r


    def get_wallet_history(self):
        cachename = self._get_wallet_cache_name()
        if os.path.isfile(cachename):
            try:
                f = open(cachename, "r")
                wh = json.load(f)
                f.close()
                t = "Reading wallet history from {}\n"
                msg = t.format(cachename)
                mprint(msg)
            except:
                t = "Can not read cache file {}\n"
                msg = t.format(cachename)
                mprint(msg)
                wh = self.get_uncached_wallet_history()
        else:
            print "Getting wallet history from Bitmex"
            wh = self.get_uncached_wallet_history()
            try:
                f = open(cachename, "w")
                json.dump(wh, f)
                f.close()
                print "Creating cache file "+cachename
            except:
                t = "Can not create cache file {}\n"
                msg = t.format(cachename)
                mprint(msg)
        return wh


    def interval_realized_pl(self, t1, t2, verbose = False):
        """
        Returns the summatory of realized PL in wallet history for the 
        specified time interval.
        
        Expressed as USD
        """
        r = 0
        wh = self.get_wallet_history()
        if wh["status"] != "ok":
            return wh

        amount = 0
        msg = ""
        price = self.last_price_seen 
        for trans in wh["msg"]:
            t = trans["timestamp"]
            r1 = t1 <= t and t <= t2
            r2 = r1 and trans["transactType"] == "RealisedPNL"
            if r2:
                btc = trans["amount"] * 10**-8
                t = "{} {:9.6f} BTC ({:10.3f} {})\n"
                msg += t.format(
                   human(trans["timestamp"]),
                   btc, 
                   #  bitmex always uses BTC, also when using other bc
                   btc * price,
                   self.qc
                )
                amount += btc
        
        r = amount * self.last_price_seen

        if verbose:
            t = "                  Total   {:9.6f} BTC ({:10.3f} USD)\n"
            msg += t.format(
                amount,
                r
            )
            mprint(msg)
            self.log.write(msg)

        return r


    def format_wallet_history(self, wh):
        if wh["status"] != "ok":
            msg = wh["status"]
        else:
            msg =""
            for trans in wh["msg"]:
                msg += self.format_wh_transaction(trans)
        return msg


    def format_wallet_summary(self,ws):
        if ws["status"] != "ok":
            msg = ws["status"]
        else:
            msg =  "transact type : "
            msg += ws["transactType"]+"\n"
            msg += "margin balance: "
            msg += self.format_satoshi( ws["marginBalance"])
            msg += "wallet balance: "
            msg += self.format_satoshi( ws["walletBalance"])
            msg += "amount        : "
            msg += self.format_satoshi( ws["amount"])
            msg += "realised PL   : "
            msg += self.format_satoshi( ws["realisedPnl"])
            msg += "unrealised PL : "
            msg += self.format_satoshi( ws["unrealisedPnl"])
            msg += "pending debit : "
            msg += self.format_satoshi( ws["pendingDebit"])
        return msg
       

    def format_satoshi(self, satoshi):
        price = self.last_price_seen
        btc = satoshi * 10 **-8
        usd = btc * price
        t = "{:9.6f} BTC ({:10.2f} USD)\n"
        msg = t.format(
            btc,
            usd
        )
        return msg


    def satoshi_to_usd(self, satoshi, price = None):
        if price == None:
            price = self.last_price_seen
        btc = satoshi * 10 **-8
        usd = btc * price
        return usd
     

    def format_wh_transaction(self, trans):
        """
        Returns a string, pretty printing one transaction of wallet history
        """
        price = self.last_price_seen
 
        t = "timestamp    : {}\ntransactID   : {}\n"
        msg = t.format(
            human(trans["timestamp"]),
            trans["transactID"]
        )

        t = "transactType : {} transactStatus: {}\n"
        msg += t.format(
            trans["transactType"],
            trans["transactStatus"]
        )

        marginBalance = trans["marginBalance"]
        if marginBalance == None:
            marginBalance = 0

        walletBalance = trans["walletBalance"]
        if walletBalance == None:
            walletBalance = 0

        # We say "BTC" and not bc, because bitmex uses BTC, even when
        # bc is other (crappy) coin.
 
        t =  "marginBalance: {:11.8f} BTC ({:10.2f} USD)\n"
        t += "amount       : {:11.8f} BTC ({:10.2f} USD)\n"
        t += "walletBalance: {:11.8f} BTC ({:10.2f} USD)\n\n"
        msg += t.format(
            marginBalance * 10**-8,
            marginBalance * 10**-8 * price,
            trans["amount"] * 10**-8,
            trans["amount"] * 10**-8 * price,
            walletBalance * 10**-8,
            walletBalance * (10**-8) * price
        )

        return msg


    def get_wallet_summary(self, complete = False):
        # When 'complete' is True, we return the whole summary,
        # not only the dicts related to current symbol
        symbol = self.symbol_bitmex_notation
 
        verb = "GET"
        path = "/api/v1/user/walletSummary"
        params = {}
        params["symbol"] = symbol

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
        
        if r["status"] != "ok" or complete:
            pass   # returns this r
        else:
            for item in r["msg"]:
                if item["symbol"] == symbol:
                    r = item
                    r["status"] = "ok"
        return r


    def get_position(self, verbose=False):
        if self.symbol == "BTCUSD":
            symbol = "XBTUSD"
        else:
            t = "unknown symbol: {}\n"
            msg = t.format(self.symbol)
            self.log.write(msg)
            raise Exception(msg)

        verb = "GET"
        path = "/api/v1/position"
        params = {}
        params["symbol"] = symbol

        p = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
        
        if p["status"] == "error":
            pass
        else:
            positions = []
            for position in p["msg"]:
                position["timestamp"] = misc.iso_to_unix(position["timestamp"])
                position["currentTimestamp"] = \
                    misc.iso_to_unix(position["currentTimestamp"])
                position["openingTimestamp"] = \
                    misc.iso_to_unix(position["openingTimestamp"])

                if position["isOpen"]:
                    parsed_pos = self._parse_position(position)
                    norm_pos = self._normalize_position(parsed_pos)
                    positions.append( norm_pos )

                    if verbose:
                        print "raw position"
                        misc.superprint(position)

                        print "parsed position:"
                        misc.superprint(parsed_pos)

                        print "formated position"
                        print self.format_position(parsed_pos)

                        print "normalized pos"
                        misc.superprint(norm_pos)
          
            r = {}
            r["status"] = "ok"
            r["msg"] = positions
            r = self._compute_position(r, verbose)
        
        return r

    
    def _parse_position(self,i):
        #if i["status"] != "ok":
        #    return i
        #i = i["msg"]
        r = {}
        r["status"] = "ok"
        r["last_price"] = i["lastPrice"]
        r["entry_price"] = i["avgEntryPrice"]
        r["last_price"] = i["lastPrice"]
        r["prev_close_price"] =  i["prevClosePrice"]
        r["mark_price"] = i["markPrice"]
        r["mark_value_sato"] = i["markValue"]
        r["pos_cost_sato"] = i["posCost"]
        r["opening_qty"] =  i["openingQty"]
        r["opening_cost_sato"] = i["openingCost"]
        r["current_cost_sato"] = i["currentCost"]
        r["last_value_sato"] = i["lastValue"]
        r["unrealizedPL_sato"] =  i["unrealisedGrossPnl"]
        r["unrealizedCost_sato"]= i["unrealisedCost"]

        r["rebalancedPL_sato"] = i["rebalancedPnl"]
        #  The value of realised PNL that has transferred to our wallet 
        #  for this position.

        r["realized_PL_sato"] =i["realisedPnl"]
        # Fees, rebates and funding costs due to that position, exluding
        # the fees, rebates and funding costs that are already accounted
        # to the wallet. Realized PL are accounted to the wallet daily.

        # In other words: fees, rebates and funding costs for today.
        # Previous fees, rebatess and funding costs are already accounted
        # to the wallet. 

        # Remember that funding costs can be negative (rebates, negative
        # costs = an income)

        r["leverage"] = i["leverage"]
        r["breakEvenPrice"] = i["breakEvenPrice"]

        r["current_qty"] = i["currentQty"]
        r["liquidation_price"] = i["liquidationPrice"]

        return r


    def _normalize_position(self, i):
        """
        Gets position in bitmex format, returns a position in bitfinex
        format (the one used by mbot)

        We return PL as qc (USD), as traditional brokers (including
        bitfinex) use to do.
        
        The oddity is that bitmex express that in bc (BTC), and also mbot
        So for this broker (bitmex) might seem a bad choice to get the data 
        in BTC, convert to USD and then convert to BTC again.

        But we do it anyway, to keep interfaces consistent between brokers.
        """
        r = {}
        r["symbol"] = self.symbol
        r["current_qty"] = i["current_qty"]  # USD contracts. Bitmex only
        r["base_price"] = i["entry_price"]

        r["amount"] = i["current_qty"] / float( i["entry_price"] )
        # in standard brokers we buy bc, this is the amount of bc
        # bought. In bitmex we buy contracts, we compute this as the 
        # equivalent bc we are buying

        r["leverage"] = i["leverage"]
        r["liq_price"] = i["liquidation_price"]
        r["last_price"] = i["last_price"]

        r["mark_price"] = i["mark_price"] # Bitmex only
        # Bitmex uses a "mark price". Is the price really used to valuate our
        # position. "last price" is the raw price, but the price used
        # (the mark price) is a "fair price". When nothing special happens in
        # the market, both are very close (or equal, I'm not sure). But
        # when extreme movement happen, fair price is a rectified
        # price, because bitmex has certaing engine, including certain 
        # insurances, that close our position achieving better price.
       
        r["realized_PL_sato"] =i["realized_PL_sato"] #  Bitmex only
        r["pl_already_in_balance"] = \
            self.satoshi_to_usd( 
                i["rebalancedPL_sato"] 
            )
        return r

    def format_position(self, i):
        if i["status"] == "error":
            return i

        print "Format Position"

        line = ""
        price = i["last_price"]
        for key in i:
            line += key + " : "
            key_label = key
            if key.find("sato") == -1:
                line += str(i[key]) + "\n"
            else:  # value expresed in satoshi
                value_as_btc = i[key] * 10 ** (-8)
                value_as_usd = value_as_btc * price
                t = "{:.6f} BTC ({:.2f} USD)\n" 
                line += t.format(
                    value_as_btc,
                    value_as_usd
                )

        return line


    def _normalize_orderbook(self, bitmex_obook):
        """
        Our software uses for the orderbook bitfinex format.
        'bids' and 'asks' are arrays of dicts like this:

        {'timestamp': 1529319757.0, 'price': 6423.7, 'amount': 4.52875884}
   
        While bitmex returns a single array of dicts like

        {u'price': 6420, u'symbol': u'XBTUSD', u'size': 239670, 
         u'id': 8799358000, u'side': u'Sell'},
     
        (Sell=ask   Buy=bid)

        Bitfinex returns amount specified in bc (BTC), while bitmex
        size means 1 USD contracts

        Bitfinex asks: ascending order
        Bitfinex bids: descending order

        The most relevan one is the first element (cheapest sell
        price, highest buy price)
        To present data human friendly, asks are inverted. But only
        to show them to humans, internaly still in increasing order

        Before Jan 2020, Bitmex orderbooks used to come in "human friendly"
        format: that is, decreasing asks and decreasing bids.
        In Jan 2020, orderbook of the testnet become not sorted, so this
        method sorts it.

        """

        asks = []
        bids = []
        current_time = time.time()
        for bitmex_tick in bitmex_obook:
            bitfinex_tick = {}
            bitfinex_tick["timestamp"] = current_time
            price = bitmex_tick["price"]
            bitfinex_tick["price"] = price
            bitfinex_tick["amount"] = bitmex_tick["size"] / float(price)
            if bitmex_tick["side"] == "Sell":
                asks.insert(0, bitfinex_tick)
            elif bitmex_tick["side"] == "Buy":
                bids.append(bitfinex_tick)
     
        #print("asks pre-sorting\n")
        #print(self._trace_prices(asks))
        #print("\n\nbids pre-sortig\n")
        #print(self._trace_prices(bids))
        #print("\n")

        asks = self._sort_prices(prices = asks, ascending = True)
        bids = self._sort_prices(prices = bids, ascending = False)

        #print("asks post-sort\n")
        #print(self._trace_prices(asks))
        #print("\n\bids post-sort\n")
        #print(self._trace_prices(bids))
        #print("\n")
    
        r={}
        r["status"] = "ok"
        r["asks"] = asks
        r["bids"] = bids

        return r

    def _trace_obook(self,obook):
        rval = "asks\n\n"
        for item in obook["asks"]:
            rval += self._trace_obook_item(item)
        rval += "\nbids\n\n"
        for item in obook["bids"]:
            rval += self._trace_obook_item(item)
        return rval

    def _trace_obook_item(self,item):
        t = "{} {:7.3f} {:.2f}\n"
        rval = t.format(
            human(item["timestamp"]),
            item["amount"],
            item["price"])
        return rval
 
    def _trace_prices(self, prices):
       rval = ""
       for x in prices:
           rval += str(x["price"]) + " "
       return rval

    def _sort_prices(self,  prices, ascending):
        if ascending:
            prices.sort(self._ascending_criterium)
        else:
            prices.sort(self._descending_criterium)
        return prices
    
  
    def _ascending_criterium(self, a,b):
        if a["price"] < b["price"]:
            return -1
        elif a["price"] > b["price"]:
            return 1
        else:
            return 0


    def _descending_criterium(self,a,b):
        return self._ascending_criterium(b,a)



    def format_orderbook(self,obook, maxorders=15):
        if maxorders < 2:
            t = "Wrong value for maxorders: {}\n"
            msg = t.format(maxorders)
            self.log.write(msg)
            raise Exception(msg)
        elif maxorders > len(obook["asks"]):
            maxorders = len(obook["asks"])
        msg = ""
        try:
            msg = ""
            i = maxorders - 1  # We count 'c style', starting in 0
            while True:
                if i < 0:
                    break
                ask = obook["asks"][i]

                t = "{:.1f} {:8.5f}\n"
                msg += t.format(
                    ask["price"],
                    ask["amount"]
                )
                i -=1
                
            msg += "     >--<\n"
            for bid in obook["bids"][:maxorders]:
                t = "{:.1f} {:8.5f}\n"
                msg += t.format(
                    #human(float(bid["timestamp"])),
                    bid["price"],
                    bid["amount"]
                )

        except TypeError:
            msg = "Wrong order format:"+str(obook)+"\n"
        except IndexError:
            msg = "Wrong order format, index related:"+str(obook)+"\n"
        return msg


    def open_position(
            self, size, base_price, direction, date, 
            trail_value_per=None    # for trailing stop
        ):

        """
        Wrapper to place_smart_order(), using the same interface
        than simulations interface
        """


        if size <= 0 :
            t = "Wrong size: {}. Size must be greater than 0\n"
            msg = t.format(size)
            self.log.write(msg)
            raise Exception(msg)

        if direction != 1 and direction != -1:
            t = "Expecting 1 or -1 , found {}"
            msg = t.format(direction)
            self.log.write(msg)
            raise Exception(msg)
      
        if direction == 1:
            side = "buy"
            self.trade.direction = 1
        elif direction == -1:
            side = "sell"
            self.trade.direction = -1
        else:
            t = "Expecting 1 or -1 , found {}"
            msg = t.format(direction)
            self.log.write(msg)
            raise Exception(msg)

        self.cancel_all_orders()
        # To remove previous trailing stops

        wallet = "trading"

        purpose = "open"
        r1 = self.place_smart_order(
            wallet=wallet, 
            amount=size,
            side=side,
            purpose=purpose
        ) 

        # For security reasons, when we open position we place at the
        # same time a trailing stop loss order

        if trail_value_per == None:
            trail_value_per = DEFAULT_TRAILING

        if trail_value_per < 0 :
            t = "Ignoring negative sign for trail_value_per ({})\n"
            msg = t.format(trail_value_per)
            mprint(msg)
            self.log.write(msg)
            trail_value_per = abs(trail_value_per)

        if base_price == None:
            base_price = self.last_price_seen

        trail_value = \
            int( trail_value_per * .01 * float(base_price) * direction * -1 )

        t = "Placing traling stop order. Trail value: {} USD ({:3.1f}%)\n"
        msg = t.format(trail_value, trail_value_per)
        if self.sg.run_mode != "backtesting":
            mprint(msg)
        self.log.write(msg)
        r2 = self._set_trailing_stop(
            trail_value = trail_value
        )

        return str(r1) + str(r2) + "\n"


    def close_several_positions(self, price, date, verbose=False):
        """
        NOT USED ANYMORE, we don't allow more that one position
   
        Earlier versions of this function used 'price' parameter. Also
        the exchange used for simulations. But in this version, parameter 
        'price' is ignored, we use smart orders.
        """
        r = self.get_position()
        if r["status"] != "ok":
            return r["status"]
        else:
            msg = "No positions opened"
            for position in r["msg"]:
                amount = position["amount"]
                # Bitfinex uses negative values to show that the position 
                # was short. But to place the order, we must always use 
                # positive values.
                if amount > 0:
                    opening_side = "buy"
                else:
                    opening_side = "sell"
                    amount = amount * -1

                msg = self.place_order_to_close(amount, opening_side) + "\n"
        return msg


    def positions(self, verbose=False):
        """
        We don't allow more than 1 opened positions. In the first exchange
        that we programmed (bitfinex), we returned a list with all the 
        positions, in case that there were more than one, the trader
        raised an exception.

        In the second exchange programmed (bitmex), get_positions just
        returns the position, not a "msg" with the list of positions.

        To keep the interface, we add here a "positions" method, that
        inserts the position in a list, with only 1 element.
        Or 0 elements if there is no opened position.

        Yes, it is ugly fast hack :(
        """
        pos = self.get_position(verbose=verbose)
        if pos["status"] != "ok":
            return r["status"]
        else:
            r = {}
            r["status"] = "ok"
            r["positions"] = []
            if pos["any_position"]:
                r["positions"].append(pos)
        return r


    def close_position(self, price, date, verbose=False):
        """
        Earlier versions of this function used 'price' parameter. Also
        the exchange used for simulations. But in this version, parameter 
        'price' is ignored, we use smart orders.
        """

        self.trade.direction = 0
        pos = self.get_position()
        if pos["status"] != "ok":
            msg =  r["status"]
        elif not pos["any_position"]:
            msg = "No position openend\n"
            mprint(msg)
            self.log.write(msg)
            msg = "ok"
        else:
            amount = pos["amount"]
            # Bitfinex uses negative values to show that the position 
            # was short. But to place the order, we must always use 
            # positive values.
            if amount > 0:
                opening_side = "buy"
            else:
                opening_side = "sell"
                amount = amount * -1

            msg = self.place_order_to_close(amount, opening_side) + "\n"
        return msg


    def claim_position(self, price, date, verbose=False):
        """
        In standard brokers, like bitfinex, closing the position means selling
        the bc (BTC) or buying the bc, but keeping the profit (if any) as bc,
        not qc. Standard brokers keep P/L in qc (USD).
        Bitmex does not manage USD or any other fiat. So closing a position
        is the same than claiming it
        """

        return self.close_position(price, date, verbose)


    def _reverse_side(self,opening_side):
        if opening_side == "buy":
            side = "sell"  # was long, we close selling
        elif opening_side == "sell":
            side = "buy"
        else:
            t = "wrong opening_side: {}\n"
            msg = t.format(opening_side)
            self.log.write(msg)
            raise Exception(msg)

        return side


    def place_order_to_close(self, amount, opening_side):
        wallet = "trading"

        side = self._reverse_side(opening_side)

        r = self.place_smart_order(
            wallet=wallet,
            amount = amount,
            side = side,
            purpose = "close"
        )
        #self.close_residual()
        return r


    def place_market_order_to_close(self, amount_as_qc, opening_side):
        side = self._reverse_side(opening_side)
        t = "{} Placing market order to {} {} {} to close position\n"
        msg = t.format(
            misc.human(time.time()),
            side,
            amount_as_qc,
            self.qc
        )
        mprint(msg)
        self.log.write(msg)
  
        r = self._place_basic_order(
            amount = None,
            price = None,
            side = side,
			ord_type = "market",
            purpose="close", 
            amount_as_qc = amount_as_qc
        )
        return r


    def place_order(self, amount, price, side, ord_type, 
            purpose="open", amount_as_qc = None, last_order = False
        ) :

        amount_reduced = None
        # if this value is != None, means that we reduced the requested
        # amount in the order because the exchange said that we have
        # not enough balance

        if purpose == "open" and last_order:
            msg = "last_order only can be True for close orders!"
            self.log.write(msg)
            raise Exception(msg)
        # last_order True means that our closing order it is supposed to 
        # be the last suborder, the one who completely closes the position.
        # This parameter is useful only for bitmex, where amounts are USD
        # contracts. 
        # So, we do not consider the amount (as bc) and convert it to 
        # contracts, that probably will have rounding issues.
        # We just check out position in contracts, and place the order
        # for that amount of contracts.

        remaining_qc, opening_side = self._get_position_as_qc()

        r1 = (purpose == "close") and last_order
  
        if r1 and remaining_qc == 0:
            t = "[bitmex] {} Receiving order to completely close position.\n"
            t += "Position already closed. Ignoring it\n"
            msg = t.format( human(time.time()))
            mprint(msg)
            self.log.write(msg)
            r = {}
            r["status"] = "completed"
            return r
        elif r1 and amount != None:
            original_amount = amount
            amount = None
            amount_as_qc = remaining_qc
            original_side = side
            side = self._reverse_side(opening_side)
            t = "[bitmex] Original order: {} {:.6f} {}. Its purpose is "
            t += "to completely close the position, forcing to  "
            t += "{} {:.1f} {}\n"
            # Our bot gives order amount in bc (as most brokers expect)
            # Bitmex expects order amount as qc. Most of times we just compute
            # the equivalent amount. But not for last order: we check the 
            # remaining qc amount and express the order as exactly this amount
            # of qc, otherwise expresing amount in bc and the convertion would
            # lead to rounding issues, probably would let our position in 
            # +-1 USD.
            msg = t.format(
                original_side,
                original_amount,
                self.bc,
                side,
                amount_as_qc,
                self.qc
            )
           
            mprint(msg)
            self.log.write(msg)

        while True:

            r = self._place_basic_order(
                amount = amount,
                price = price,
                side = side,
                ord_type =  ord_type, 
                purpose = purpose, 
                amount_as_qc = amount_as_qc
            )

            if r["status"] == "ok":
                r["amount_reduced"] = amount_reduced
                break
            elif r["status"] == "no_balance":
                amount, amount_as_qc = self._reduce_amounts(
                    amount, amount_as_qc
                ) 
                if amount != None:
                    amount_reduced = amount
                else:
                    amount_reduced = amount_as_qc / float(price)
                
            elif r["status"] == "too_small":
                r["amount_reduced"] = amount_reduced
                r["order_id"] = None
                break
        if r["status"]=="ok":
            t = "{} Placed order {}\n"
            msg = t.format(
                misc.human(time.time()),
                r["order_id"]
            )
            self.log.write(msg)
            mprint(msg)
        return r


    def _get_position_as_qc(self):
        pos = self.get_position()
        current_qty = pos["current_qty"]
        
        if current_qty >= 0:
            side = "buy"
        else:
            current_qty = current_qty * -1
            side = "sell"

        return current_qty, side


    def _reduce_amounts(self, amount, amount_as_qc):
        if amount != None:
            amount = amount * REDUCE_SOMETHING
            t = "Not enough balance. Reducing the order to {:.8f} {}\n"
            msg = t.format(
                amount,
                self.bc
            )
            mprint(msg)
            self.log.write(msg)
                    
        elif amount_as_qc != None:
            amount_as_qc = int(amount_as_qc * REDUCE_SOMETHING)
            t = "Not enough balance. Reducing the order to {:.8f} {}\n"
            msg = t.format(
                amount_as_qc,
                self.qc
            )
            mprint(msg)
            self.log.write(msg)
        else:
            msg = "amount or amount_as_qc must be None"
            self.log.write(msg)
            raise Exception(msg)
        return amount, amount_as_qc


    def _place_basic_order(self, amount, price, side, ord_type, 
            purpose="open", amount_as_qc = None
        ):

        """
        This function interface is designed for spot trading at any exchange,
        where the order size is usually denominated in bc (BTC). 'amount'
        is bc.

        But this is the bitmex version, where the order is denominated in
        contracts, equivalent to qc (USD).
        So, we keep the 'amount' parameter, when used, we just calculate
        equivalent contracts. But we also add an extra parameter, 
        'amount_as_qc', to express contracts directly. Both parameters are
        excluyent.
        """

        if amount != None and amount_as_qc != None:
            msg = "You can not specify both amount and amount_as_qc"
            self.log.write(msg)
            raise Exception(msg)

        if amount == None and amount_as_qc == None:
            msg = "You must specify amount or amount_as_qc"
            self.log.write(msg)
            raise Exception(msg)
    
        if amount_as_qc != None and amount_as_qc < 1:
            t = "Amount_as_qc {} is below the minimum (1)\n"
            msg = t.format(
                amount_as_qc,
            )
            mprint(msg)
            self.log.write(msg)
            r = {}
            r["status"] = "too_small"
            r["msg"] = msg
            return r

        if amount != None and amount * price  < 1:
            t = "Amount {} is below the minimum (1 contract)\n"
            msg = t.format(
                amount
            )
            mprint(msg)
            self.log.write(msg)
            r = {}
            r["status"] = "too_small"
            r["msg"] = msg
            return r

        if amount_as_qc == None:
            amount_as_qc = int(amount * price)

        if ord_type == "market":
            ord_type = "Market"
        elif ord_type == "limit":
            ord_type = "Limit"
        else:
            t = "wrong order type: {}"
            msg = t.format(ord_type)
            self.log.write(msg)
            raise Exception(msg)

        if side == "buy":
            side = "Buy"
        elif side == "sell" :
            side = "Sell"
        else:
            t = "wrong side parameter: {}"
            msg = t.format(side)
            self.log.write(msg)
            raise Exception(msg)

        verb = "POST"
        path = "/api/v1/order"
        params = {}
        params["execInst"] = ""
        params["symbol"] = self.symbol_bitmex_notation
        if ord_type != "Market":
            params["price"] = price
            params["execInst"] = ",ParticipateDoNotInitiate"

        params["orderQty"] = amount_as_qc  # amount as qc  (contracts)
        params["side"] = side
        params["ordType"] = ord_type
        params["clOrdID"] = self._get_new_ord_id()  # client order id

        # ParticipateDonotInitiate aka postonly. 
        # For limit orders, this paremeter forces the order to be
        # limit. So that if our limit order happens to match
        # an existing order in the other side of the book, the order
        # is not placed

        if purpose == "open" or purpose =="half_close" or \
            purpose == "half_open":
            pass
        elif purpose == "close":
            params["execInst"] += ",Close"
        else:
            t = "Wrong purpose: {}"
            msg = t.format(purpose)
            self.log.write(msg)
            raise Exception(msg)

        params["execInst"] = params["execInst"][1:]
            # removes the first comma in the list of params
    
        t = "Placing {} {} order {}. {} 1 USD contracts\n"
        msg = t.format(
            ord_type.lower(),
            side.lower(),
            params["clOrdID"], 
            params["orderQty"]
        )
        self.log.write(msg)
        mprint(msg)

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )

        time.sleep(1.05)  # clOrdId includes the second when the order was 
        # placed. We wait 1 second to make sure that there are no duplicates
       
        if r["status"] != "ok":
            return r
        r = self._normalize_order(r["msg"])
        r["status"] = "ok"

        return r


    def _normalize_orders(self,orders):
        """
        Converts a packet with a list of orders info, from bitmex format to
        bitfinex format (as our bot expects)
        """
        if orders["status"] != "ok":
            return orders

        orders = orders["msg"]
        new_orders = []
        for o in orders:
            new_orders.append(self._normalize_order(o))
        r = {}
        r["status"] = "ok"
        r["msg"] = new_orders
        return r


    def _normalize_order(self,o):
        """
        Normalizes  (converts to bitfinex format) the data of an order that
        is in the exchange, wating to be fired. 
        """
        #try:
        new_order = {}  
        new_order["cl_order_id"] = o["clOrdID"]
        new_order["timestamp"] = misc.iso_to_unix(o["transactTime"])
        new_order["order_id"] = o["orderID"]
        new_order["side"] = o["side"].lower()
        new_order["symbol"] = o["symbol"]
        new_order["type"] = o["ordType"].lower()
        price = o["price"]
        if price != None:
            price = float(price)
        new_order["price"] = price
        new_order["avg_execution_price"] = o["avgPx"]

        if price == None:
            original_amount = None
        else:
            original_amount = o["orderQty"] / price

        if original_amount == None:
            new_order["original_amount"] = 0
        else:
            new_order["original_amount"] = float(original_amount)
           
        if price != None:
            price = float(price)
            new_order["executed_amount"] = o["cumQty"] / price
            new_order["remaining_amount"] = o["leavesQty"] /  price
        else:
            new_order["executed_amount"] = 0

        new_order["is_live"] = o["workingIndicator"]
        new_order["is_cancelled"] = not o["workingIndicator"]
        new_order["is_hidden"] = False
        #except KeyError:
            #t = "Can not normalize order\n{}\n"
            #msg = t.format(o)
            #raise Exception(msg)
           
        return new_order


    def format_order(self,o):
        """
        Receives a single order, returns a string pretty printing it
        """

        #if o["status"] == "not_found":
        #    return "Order not found\n"
        #if o["status"] == "too_small":
        #    return "Order too small, not placed\n"

        try:
            t = "cl_order_id: {} Id:{}\n"
            t += "Timestamp: {}\nSide:{}. Symbol:{}. Type:{}. "
            t += "Price: {} {}."
            t += "Avg ex price: {} {}.\n"

            msg = t.format(
                o["cl_order_id"],
                o["order_id"],
                human(float(o["timestamp"])),
                o["side"],
                o["symbol"], 
                o["type"],
                o["price"],
                self.qc,
                o["avg_execution_price"],
                self.qc
            )

            t = "Amount: {:.6f} {}. Executed : {:.6f} {}. "
            t += "Remaining : {:.6f}. {}\n"

            msg += t.format(
                o["original_amount"],
                self.bc,
                o["executed_amount"],
                self.bc,
                o["remaining_amount"],
                self.bc
            )

            t = "Live:{}. Cancelled:{}.  Hidden:{}.\n"

            msg += t.format(
                o["is_live"],
                o["is_cancelled"],
                o["is_hidden"]
            )
    
        except KeyError:
            msg = "Wrong order format:"+str(o)+"\n"
        return msg

    def _get_new_ord_id(self):
        year, month, day, hour, minutes, secs = \
            misc.unix_time_to_ints(time.time())
        t = "{}.{}.{}.{}.{}.{}.{}"
        r = t.format(
            self.botid,
            year,
            month,
            day,
            hour,
            minutes,
            secs
        )
        
        return r


    def _send_request(self, verb, path, params, data=""):
        i = 0
        while True:
            r = self._ll_send_request(
                verb = verb,
                path = path,
                params = params,
                data = data
            )
       
            if r["status"] == "ok":
                break
            elif r["msg"].find("insufficient") != -1:
                r["status"] = "no_balance"
                break
            else:
                t = "[bitmex      ]{} Error sending request\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        return r


    def _ll_send_request(self, verb, path, params, data=""):

        expires =  int(round(time.time()) + 5)

        params_encoded = urllib.urlencode(params)

        path_with_params = path
        if len(params_encoded) != 0:
            path_with_params += "?" + params_encoded

        full_url = self.base_url + path_with_params

        signature = self._get_signature(
            verb = verb,
            path_with_params = path_with_params,
            expires = expires,
            data = data
        )
        headers = self._get_headers(
            expires,
            signature
        )
      
        try:
            if verb == "GET":
                r = requests.get(
                    full_url,
                    headers = headers
                )
            elif verb == "DELETE":
                r = requests.delete(
                    full_url,
                    headers = headers
                )
            elif verb == "POST":
                r = requests.post(
                    full_url,
                    headers = headers
                )
            else:
                t = "Wrong verb: {}\n"
                msg = t.format(verb)
                self.log.write(msg)
                raise Exception(msg)
           
            r = self._check_response(r)
        except Exception as e:
            r = {}
            r["status"] = "error"
            r["msg"] = str(e)
       
        return r


    def ticker(self):
        current_time = time.time()
        if (current_time - self.last_time_checked_ticker) < TIME_INTER_CHECK_TICKER:
            return self.last_ticker_seen
        else:
            self.last_time_checked_ticker = current_time

        r = self.orderbook(
            depth = 1,
            normalized = False
        )

        ask = None
        bid = None
        volume = None
        if r["status"] != "ok":
            status = r["status"]
        else:
            for tick in r["msg"]:
                if tick["side"] == "Sell":
                    ask = tick["price"]
                elif tick["side"] == "Buy":
                    bid = tick["price"]
                volume = tick["size"] / float(tick["price"])
                
            if ask == None and bid == None:
                status = "error"
                msg = "Wrong orderbook format"
            else:
                status = "ok"

            if ask == None and bid != None:
                ask = bid
            elif bid == None and ask != None:
                bid = ask
            
        r = {}
        r["status"] = status
        r["ask"] = ask
        r["bid"] = bid
        r["volume"] = volume

        self._update_min_order_size(r["bid"])
        # It is not necessay to update min_order_size so often, but doing it
        # now is free. Cheaper than launching a tick from time to time to 
        # update it

        self.last_price_seen = r["bid"]

        self.last_ticker_seen = r
        return r


    def get_wallet(self, verbose = False):

        current_time = time.time()
        if (current_time - self.last_time_checked_wallet) < TIME_INTER_CHECK_WALLET:
            return self.last_wallet_seen
        else:
            self.last_time_checked_wallet = current_time

        verb = "GET"
        path = "/api/v1/user/wallet"
        params = {}

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
         
        if r["status"] != "ok":
            return r

        amount_satoshis = r["msg"]["amount"]
        r = r["msg"]
        r["status"] = "ok"

        r["timestamp"] = misc.iso_to_unix(r["timestamp"])
        r["prevTimestamp"] = misc.iso_to_unix(r["prevTimestamp"])

        r["wb"] = self.satoshi_to_usd(amount_satoshis)
        # Those are the BTC that we have in or wallet, expressed as USD.
        # Excludes:
        # - Obviously, Floating PL 
        # - Not so obvious, a bitmex oddity: Realized PL that stay in the
        # 'daily record', but not moved yet to the wallet. Those realized PL
        # are moved daily to the wallet.
  
        self.last_wallet_seen = r
        return r


    def wallet_value(self, wallet = None, price = None, verbose = False):

        """
        Total value of margin wallet, expressed as qc.
        Parameters 'wallet' and  'price' are not used here,
        but they are necessary to keep the same interface than the
        synthetic exchange used in simulations.
        """

        w = self.get_wallet(verbose)
        if w["status"] != "ok":
            r = w["status"]
        else:
            r = w

        return r


    def max_position_size(self):
        """
        We don't compute this value in a very acurate way, just multiply
        wallet value by leverage. We do not consider the case that free
        margin != wallet_value, because our bot only opens 1 position 
        at time, with 100% of avalaible margin.

        In other words: when our bot opens a position, there is no other
        position opened. So all margin in wallet is available for the
        trade. No reserved margin for other trades. And also, there will be
        no floating profits (corresponding to a opened trade) that allow us
        to open a position with a collateral beyond the wallet. (Any broker 
        allows the trader to use floating profits as collateral for new
        leveraged trades)

        Another thing to consider is that if we try to open a position
        bigger that allowed, our code will react to whe error returned
        by the broker and just start reducing it a 10% or 15% until the size
        is right. So, even with several positions opened, our code will work.

        """

        wb = self.wallet_value()
        price = self.last_price_seen
        r = wb * self.leverage / price
        return r


    def _get_orders(self):
        verb = "GET"
        path = "/api/v1/order"
        params = {}
        params["filter"] = '{"open": true}'

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
  
        if r["status"] == "ok":
            r = self._normalize_orders(r)
        return r


    def _get_specific_order(self, order_id):
        verb = "GET"
        path = "/api/v1/order"
        params = {}
        id_filter = '{"orderID": "XXX"}'
        id_filter = id_filter.replace("XXX", str(order_id))
        # I do not use format because I shoud escape string
       
        params["filter"] = id_filter

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
  
        if r["status"] == "ok":
            r = self._normalize_orders(r)
        return r


    def get_my_orders(self, order_id = None):
        """
        Returns a list of orders with cliOrdID beginning with our botid.
        If order_id is specified, returns that order
        """
        orders = self._get_orders()
        if orders["status"] != "ok":
            return orders
        else:
            orders = orders["msg"]

        my_orders = []
        for order in orders:
            cl_order_id = order["cl_order_id"]
            order_botid = cl_order_id.split(".")[0]
            r1 = self.botid == order_botid #  requirement 1
            r2 = str(order["order_id"]).strip() == str(order_id).strip()
            r3 = order_id == None
            if r1 and (r2 or r3):
                my_orders.append(order)

        r = {}
        r["status"] = "ok"
        r["msg"] = my_orders
        return r


    def order_status(self, order_id):
        p = self._get_specific_order( order_id)
        if p["status"] != "ok":
            return p

        if len(p["msg"]) == 0:
            p["status"] = "not_found"
            return p
     
        r = p["msg"][0]
        r["status"]="ok"
   
        return r


    def order_status_ok(self, packet):
        """
        Checks if the order status that we have received from bitmex is ok.
        Where 'ok' means 'well formed order status'. In other words, 
        bitmex did not sent a corrupt response.
        """
 
        required_keys = [
            'status', 'remaining_amount', 'order_id', 'timestamp', 'price',
            'is_cancelled', 'is_hidden', 'cl_order_id', 'avg_execution_price',
            'executed_amount', 'is_live', 'original_amount', 'type', 'symbol',
             'side'
        ]


        if "status" in packet.keys() and packet["status"] != "ok":
            return True
            # has status, the status is not ok --> legal response

        if not misc.dict_has_keys(packet, required_keys):
            t = "order_status_ok : wrong keys in packet {}"
            msg = t.format(packet)
            print msg
            return False

        return True


    def cancel_my_orders(self):
        orders = self.get_my_orders()
        if orders["status"] != "ok":
            return orders
        else:
            orders = orders["msg"]

        if len(orders) == 0:
            msg ="No active order to cancel"
            print msg
            r = {}
            r["status"] = "ok"
            r["msg"] = msg
            return r

        orders_as_string = ""
        for order in orders:
            clOrdID = order["cl_order_id"]
            orders_as_string += ","
            orders_as_string += clOrdID
        orders_as_string = orders_as_string[1:]

        print "Cancelling orders ", orders_as_string
        verb = "DELETE"
        path = "/api/v1/order"

        params = {}
      
        params["clOrdID"] = orders_as_string

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )

        return r




    def margin_balance(self, price, date, verbose=False):
        """
        Initial deposit plus realized PLs plus floating PLs

        In our code, margin_balance and check_margins are the two balance
        related methods that all exchange objects must include.
  
        This version ignores 'price' and 'date' parameters, we keep
        then to keep the same interface than the dummy exchange used
        in backtesting.

        'realized_pl' means fees + rebates + funding costs + PL from closed
        positions.
        Excludes floating profits and losses derived from the price's change
        in opened positions.

        Always expressed as quote currency (qc, USD) because this what 
        standard brokers do and our mbot expects.

        Yes, considering bitmex alone, this is a little weird, expressing 
        margin as qc when bitmex uses bc and our bot finally works in bc. 
        But we want a unified interface between our bot and different
        exchange plugins, so this makes sense.
        """

        mc = False   
        # True if we are margin called
        # Very important for the dummy exchange used in backtesting, but
        # not necessary here.
  
        # mb 
        # margin balance. Initial deposits + realized PLs + floating PLs

        # mp 
        # margin percentage. mb / initial position value
        
        # lp 
        # liquidation price

        wallet = self.get_wallet()
        balance_in_wallet = wallet["wb"]
        # includes most fees and rebates

        pos = self.get_position(
            verbose = verbose
        )

        raw_pl = float(pos["raw_pl"] )
        # floating PL, excluding any fee

        today_realized_pl = pos["today_realized_pl"] 
        # fees, costs and rebates of today, not yet moved to the wallet.
        # bitmex oddity: if we closed an operation today, the pl is not
        # included in today_realized_pl
    

        any_position = pos["any_position"]
        initial_pos_value = pos["initial_pos_value"]

        price = pos["price"] 
        # usually mark price (fair price), not last seen price. Even though
        # the function can change it

        lp = pos["liquidation_price"]
        mb = balance_in_wallet + raw_pl + today_realized_pl
        mb_as_btc = mb / price
            
        status = "ok"

        if any_position and initial_pos_value != 0:
            mp = abs(100 * mb / initial_pos_value)
        else:
            mp = 100

        if verbose:
            t  = "Balance in wallet : {:9.2f} {} ({:11.3f} BTC)\n"
            msg = t.format(
                balance_in_wallet,
                self.qc,
                balance_in_wallet / price
            )

            t = "Margin balance    : {:9.2f} {} ({:11.3f} {})\n"
            msg += t.format(
                mb,
                self.qc,
                mb / price,
                self.bc
            )

            t = "Margin percentage: {:5.2f} %\n"
            msg += t.format(
                mp
            )

            mprint(msg)
            self.log.write(msg)

        return mb, mp, lp,  status


    def wb_mb(self):
        """
        Wallet balance, margin balance,  as btc

        """

        x = self.get_wallet_summary(complete=True)
        if x["status"] != "ok" :
            msg = x
            balance = 0
            return msg, balance

        msg = ""
        try:
            for a in x["msg"]:
                if a["transactType"] == "Total":
                    wb_satoshi = a["walletBalance"]   # wallet balance as satoshi
                    wb = wb_satoshi * 10 **-8   # wallet balance as BTC

                    mb_satoshi = a["marginBalance"] # margin balance
                    mb = mb_satoshi * 10 **-8   # margin balance as BTC

        except KeyError:
            wb = 0
            mb = 0
            msg = "TransactType not found, wrong format in get_wallet_summary\n"
            msg += str( x["msg"])
  
        if msg == "":
            msg = "ok"
    
        return msg, wb, mb   # balance as BTC


    def get_today_realized_pl(self):
        """
        Today realized profits and losts, not transfered to wallet yet.
        This value is usually considered it methods returning position.

        But when we have just closed position, there is no position,
        this method returns it so aggregator can consider it.
        """

        verbose = False
        pos = self.get_position(
            verbose = verbose
        )

        rval = pos["today_realized_pl"] 
        # fees, costs and rebates of today, not yet moved to the wallet
        # As always, expressed as qc

        # any_position = pos["any_position"]

        return rval


    def _compute_position(self, pos,  verbose=False):

        # Realized PL for a opened trade : 
        # fees, rebates, fundings costs. Excludes floating PL
  
        # When the position is closed, the "main" PL is moved to the
        # realized costs

        # Realized PL are stored in a kind of daily record,
        # and moved to the wallet once a day. 

        # - pl_already_in_balance 
        #   Realized PL due to this trade, already moved to the wallet 

        # - position_today_realized_pl
        #   The PL for today that are not moved to the wallet yet


        if len(pos["msg"]) > 1:
            msg = "We should have only one position opened"
            self.log.write(msg)
            raise Exception(msg)
        if len(pos["msg"]) == 0:
            any_position = False
        else:
            any_position = True
            pos = pos["msg"][0]

        if not any_position :
            pl_already_in_balance = 0
            initial_amount = 0
            amount = 0
            initial_pos_value = 0
            mark_price = self.last_price_seen
            base_price = mark_price
            liquidation_price = 0
            who = 0
            current_qty = 0
            leverage = self.leverage
            last_price = self.last_price_seen
            leverage = 1
        else:
            mark_price = pos["mark_price"]
            pl_already_in_balance = pos["pl_already_in_balance"] 
            initial_amount = pos["amount"]
            amount = pos["amount"]
            base_price = pos["base_price"]
            initial_pos_value = initial_amount * base_price
            liquidation_price = pos["liq_price"]
            leverage = pos["leverage"]
            last_price = pos["last_price"]
            if initial_amount == 0:
                who = 0  # indeed this is not possible, but don't care
            elif initial_amount > 0:
                who = 1 #  bull
            else:
                who = -1 #  bear
            current_qty = pos["current_qty"]
        price = mark_price

        price_increment = 100 * (price - base_price) /base_price

        current_pos_value = price * initial_amount
        raw_pl =   (current_pos_value - initial_pos_value) 
        
        margin_spent = initial_pos_value / leverage

        if any_position and margin_spent != 0 :
            raw_pl_per = who * 100 *  (raw_pl / margin_spent) 
        else:
            raw_pl_per = 100 

        #ws = self.get_wallet_summary()
        #realized_pl_sato = ws["realisedPnl"]
        #realized_pl = self.satoshi_to_usd(realized_pl_sato, price)

        # Today realized PL for current position, up to present time.
        # The day is not over, so there could be more costs in the nexts hours.
        # 'realized pl' are fees / rebates / funding costs.
        # Positive means an income, a rebate. Negative, an expense, a fee.
        # Excludes floating PL for current position. Expressed as USD

        if any_position:
            today_realized_pl = self.satoshi_to_usd(
                pos["realized_PL_sato"],
                price
            )
        else:
            today_realized_pl = 0 
            pl_already_in_balance = 0

        position_realized_pl = pl_already_in_balance + \
            today_realized_pl 

        net_pl = raw_pl + position_realized_pl

        if any_position and margin_spent != 0:
            roe_per = who * 100 * (net_pl) / margin_spent
        else:
            roe_per = 0
        # roe_per: Return of equtity, as percentage. Is the net profit.
        # raw_pl minus realized pl is the gross pl. Including leveraged
        # money. The money that WE really spent, excluding money borrowed from
        # the broker is the margin spent
 
        if verbose:
            t =  "Initial position  :  {:11.6f} {} @ {:9.2f} {}\n"
            msg = t.format(
                initial_amount,
                self.bc,
                base_price,
                self.qc
            )

            t =  "Initial position value :  {:11.2f} {}\n"
            msg += t.format(
                initial_pos_value,
                self.qc
            )

            t = "Last price: {:7.2f} Mark price (Fair price): {:7.2f}"
            t += " Dif: {:4.2f}%\n"
            msg += t.format(
                self.last_price_seen,
                mark_price,
                100 *(1- (mark_price / self.last_price_seen))
            )
            
            t= "Liquidation price: {:7.2f}\n"
            msg += t.format(liquidation_price)

            t = "Price increment: {:4.2f} % Leverage: {}\n"
            msg += t.format(
                price_increment,
                self.leverage
            )
 
            t = "Current position value :  {:11.2f} {}\n"
            msg += t.format(
                current_pos_value,
                self.qc
            )
            

            t = "Position's realized PL (fees and rebates):\n"
            t += "\tToday             : {:8.2f} USD\n"
            t += "\tAlready in balance: {:8.2f}\n"
            t += "\tTotal             : {:8.2f} USD\n"
            msg += t.format(
                today_realized_pl,
                pl_already_in_balance, 
                position_realized_pl
            )

            t = "Raw PL (excl. fees and rebates):\n{:11.2f} {} {:11.6f} {}"
            t += " {:5.2f}%\n"
            msg += t.format(
                raw_pl,
                self.qc,
                raw_pl / price,
                self.bc,
                raw_pl_per
            )

            t = "Net PL (including all fees and rebates):\n"
            t += "{:11.2f} {} {:11.6f} {}  ROE: {:5.2f}\n"
            msg += t.format(
                net_pl, 
                self.qc,
                net_pl / price,
                self.bc,
                roe_per
            )

            self.log.write(msg)
            mprint(msg)
            self.log.write(msg)
        r = pos
        r["symbol"] = self.symbol
        r["leverage"] = leverage
        r["amount"] = amount
        r["base_price"] = base_price
        r["current_qty"] = current_qty
        r["status"] = "ok"
        r["any_position"] = any_position
        r["price_increment"] = price_increment
        r["roe_per"] = roe_per
        r["raw_pl"] = raw_pl
        r["position_realized_pl"] = position_realized_pl
        r["today_realized_pl"] = today_realized_pl
        r["initial_pos_value"] = initial_pos_value
        r["price"] = price
        r["last_price"] = last_price
        r["liquidation_price"] = liquidation_price
        r["pl"] = net_pl
        r["mark_price"] = mark_price

        return r
           

    def current_trade_status(self, price, date, verbose=False):
        """
        This function returns the status of the current trade,
        (the profits and losses) and the fees spent.

        This version ignores 'price' and 'date' parameters, we keep
        then to keep the same interface than the dummy exchange used
        in backtesting.
        """

        pos = self.get_position(
            verbose = verbose
        )

        pl = pos["raw_pl"] + pos["today_realized_pl"]
        # We exclude previous realized PL (fees), that are already in balance
        fees = pos["today_realized_pl"] + pos["position_realized_pl"]

        funding_costs = 0 
        #  In bitmex, funding costs and fees are accounted together as
        #  'realized_costs'. It would be hard to isolate them, so we don't
        #  care. Fees includes everything, funding costs are 0.
        msg = ""  #  get_position() trace is already in logs
         
        return pl, fees, funding_costs, msg


    def check_margins(self, price, date, verbose=False):
        if (time.time() - self.last_time_checked_margin) < self.time_inter_checks_margin:
            mc = False
            mb = self.last_mb
            mp = self.last_mp
            lp = self.last_lp
            msg =""
            return mc, mb, mp, lp, msg
        else:
            self.last_time_checked_margin = time.time()

        mb, mp, lp ,  msg = self.margin_balance(price, date, verbose)

        mc = False  
        #  Margin Call.
        #  We can be sure that if a real exchange MCs us, we will know by other
        #  means. This parameter is only important in the dummy exchange
        #  (the simulations exchange)

        if mp > .30:
            self.time_inter_checks_margin = 60 * 30
        elif mp >= .20:
            self.time_inter_checks_margin = 60 * 15
        elif mp > .07:
            self.time_inter_checks_margin = 60 * 10
        else: 
            self.time_inter_checks_margin = 60 * 3

        self.last_mc = mc
        self.last_mb = mb
        self.last_mp = mp
        self.last_lp = lp
        return mc, mb, mp, lp, "ok"


    def place_smart_order(self, wallet, amount, side, purpose):
        self.smart_ex.place_smart_order(wallet, amount,side, purpose)

        return "smart order placed"


    def deposit_margin(self, deposit_qc, deposit_bc):
        """
        Not used in real exchange, keeped here to keep the same interface
        than dummy exchange used in simulations.
        """
        return


    def transfer_all(self, wfrom, wto):
        """
        In bitfinex, before start trading,  we wanted to transfer all funds 
        from exchange wallet to trading wallet.
        Bitmex has no trading wallet, so we keep this method, as an empty
        one.
        """

        return


    def fetch_trades(
        self, 
        t0,    # first trade date we ask 
        output_file_name ,   # including path
        max_requests_per_interval = 10, 
        interval =  60
        ):
        """
        Gets historical trade data from bitmex. Taking care of not exceeding
        allowed requests per time interval. 
        (bitmex allows 150 each 5 min if we are not logged, or 300 each 5 
        minutes if we are logged)

        max_processing_time is the maximum amount of second that this
        function runs (getting trades takes a lot of time)
        """
        
        real_time_t0 = time.time()

        output_file = mlogs.log(
            logname = output_file_name, 
            buffer_size = 0,   # messages, not bytes or lines
            append = False
        )

        output_file.reset()
        number_of_requests = 0
        t_i = time.time()
        while True:
            new_trades = self._ll_get_trades(
                start_time = t0
            )
            number_of_requests += 1
            for trade in new_trades["msg"]:
                output_file.write(trade)
            last_trade_time = self._last_trade_time(new_trades)

            t0 = last_trade_time + 1
           
            if number_of_requests >= max_requests_per_interval:
                number_of_requests = 0
                current_time = time.time()
                ellapsed = current_time - t_i
                t_i = time.time()
                if ellapsed < interval :
                    sleeping_time = (interval - ellapsed) * 1.01 
                    sleeping_time = max(0,sleeping_time)
                    print "Sleeping " + str(sleeping_time) + " secs"
                    time.sleep(sleeping_time)
    
            real_time = time.time()
            if real_time - last_trade_time < 30:  
                print "Done!"
                break

        output_file.close()

        return 


    def _last_trade_time(self, trades):
        """
        Receives a dict with the trades, returns the timsetamp of 
        the last trade included
        """
        last_line = trades["msg"][-1]
        last_time = last_line.split(",")[0]
        return int(last_time)


    def _ll_get_trades(self, start_time):
        """
        low level get trades
        Receives start time as unix time
        """
        
        t = "Getting trades since {}\n"
        msg = t.format(human(start_time))
        mprint(msg)

        start_time = misc.unix_time_to_iso(start_time)
      
        symbol = self.symbol_bitmex_notation
        verb = "GET"
        path = "/api/v1/trade"
        params = {}
        params["symbol"] = symbol
        params["startTime"] = start_time
        params["count"] = 500 #  max value is 500

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )

        if r["status"] != "ok":
            return r
     
        r["status"] = "ok"
        msg = []
        for item in r["msg"]:
            msg.append( self.format_trade(item))
        r = {}
        r["stat"] = "ok"
        r["msg"] = msg

        return r


    def format_trade(self, trade):
        time = trade["timestamp"]
        time = misc.iso_to_unix(time)
        price = trade["price"]
        volume = trade["homeNotional"]
        t = "{:.0f},{},{:.6f}\n"
        line = t.format(
            time,
            price,
            volume
        )
        return line

    def _set_trailing_stop(self, trail_value):

        if trail_value > 0 :
            side = "Buy"
        else:
            side = "Sell"

        verb = "POST"
        path = "/api/v1/order"
        params = {}
        params["symbol"] = self.symbol_bitmex_notation
        params["ordType"] = "Stop"
        params["execInst"] = "Close"
        params["pegOffsetValue"] = trail_value
        params["side"] = side

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
         
        return r

  
    def cancel_all_orders(self):
        """
        When leave_stop = true, cancels market and limit orders, leaving 
        stop orders.
        """

        verb = "DELETE"
        path = "/api/v1/order/all"
        params = {}
  
        #if leave_stop:
        #    # Only removes orders matching the filter
        #    params["filter"] = '{"ordType":"Market,Limit"}'

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
 
        return r
         

    def set_leverage(self, leverage):
        verb = "POST"
        path = "/api/v1/position/leverage"
        params = {}
        params["symbol"] = self.symbol_bitmex_notation
        params["leverage"] = leverage

        r = self._send_request(
            verb = verb,
            path = path,
            params = params
        )
         
        return r
