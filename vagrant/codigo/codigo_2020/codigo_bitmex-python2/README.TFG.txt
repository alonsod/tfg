mbot code to work with bitmex.
Minimal set of code needed for TFG
Miguel, Nov 2019


The main file is
mbitmex.py

It is checked in test_bitmex.py

Other files contain auxilary code.



Dependences:

Tested on ubuntu xenial64
Does not work on ubuntu bionic, because of a python OpenSSL bug
https://github.com/ccxt/ccxt/issues/2522


apt-get install -y python python-pip 
pip install --upgrade pip
pip install pyyaml requests python-telegram-bot pytz six


Need to add environment variable MBOT_DATA_DIR
For instance
mkdir $HOME/mbot_data
export MBOT_DATA_DIR=$HOME/mbot_data

Also need your bitmex private key, as specified in key_file
at test_bitmex.py



---------------------------------


#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789



This sytem is called mbot, it is a set of bitcoin trading bots 
running in its own platform , including data gathering, backtesting
and real trading. Developed by Miguel between 2014 and 2019, interrupted 
a couple of years in 2015. The programming language is python 2 and also cython
(compiled python).
It has been programmed from scratch, because
main bitcoin exchanges lack an algorithmic trading platform. Originally, mbot
design is inspired in Metatrader 4 and its language MQL, the leader platform 
for forex trading. But as mbot design evolves, its similarities with MQL are
geting less and less relevant.

Mbot is developed in Linux and macOS, works in both plattforms and can change 
from one to another seamlessly. Even though the servers are supposed to 
work in a cloud like AWS or Google cloud, so servers are not fully tested on
macOS, it would have no sense.

Currently (2019) trades BTC / USD in the exchanges Bitfinex and Bitmex. It has been working
since Ag 2018, with only 1 serious bug. It also includes support for Huobi exchange,
not deeply tested, and Okex tickers and data gathering, in case we were interested
in a future Okex support. Designed to work with any other forex or cryptocurrency
pair.




-- Basic terms --

This is standard trading vocabulary:


bc. Base currency. (BTC if trading BTCUSD)

qc. Quote currency. (USD if trading BTCUSD)

Going long. aka bull operation. We think price is going up, we buy bc
hoping to sell it later at higher price.

Going short. aka bear operation. We think price is going down, we sell bc
(usually borrowed from the exchange) hoping to re-buy it later at lower price.

Orderbook. Public data structure keeped by the exchange, where current
limit orders, buy and sell, are shown.

Leverage. Risk level. Leverage 3x means that for each USD of our "real money",
 we borrow 2 USD from the exchange. Maximum leverage at bitfinex is 3.3x. 
Maximum leverage at bitmex is 100x (almost suicide). For btc and our algorithms, 
a leverage between 2.5x and 5x seems right.
 

position.
When you buy or sell, you "open a position". When you sell what you bought
or buy what you sold, you "close the position".


-- Orders --
Any good exchange/broker will allow you to place several kind of orders:

Market order. Also known as "taker" order.
Buy or sell bc at current market price. 

Limit order. Also known as "maker order"
You establish a price, different than current market price. You order
to buy or sell when market price reaches your order price.
Imagine current price is 11500 USD. A limit buy order would be
"buy if prices falls to 11000 USD". A limit sell order would be
"sell if price rises to 12000 USD"

Taker and maker fees.

When you place a "maker" order (limit), you add btc or usd to the exchange's
orderbook, establishing a price. And you wait to someone to accept your
price, your funds are frozen meanwhile. If someone accepts the price, the 
deal is done. You are adding liquidity to the exchange, you provide the
exchange the merchandise to sell. From this point of view, the maker always 
sells: when sells bc (btc) is obviously selling, but when buys bc, is also
selling, maker ir selling qc (fiat money) in exchange for bc.

When you place a "taker" order (market), you buy or sell inmediately at 
market price, you acept the limit order that someone placed in the past. 
You do not wait, you do not freeze any currency, you remove liquidity 
from the exchange

That's why the fees for maker orders are usually lower then taker orders,
even though there are scrooge exchanges that charge the same fee for market 
orders or taker order. But it is not usuall. And there are very generous 
exchanges, like Bitmex, that do not charge any fee to maker orders: they pay 
you a rebate, they share with you part of the fee paid by the taker.
Probably this is one of the reasons of Bitmex great liquidity and success.

smart_ex

This is not an standard term, it is a system developed by Miguel.
Mbot always tries to place maker orders, limit orders. But with a price
very close to market price. Most of times, mbot limit orders are executed
just a few seconds after placed. Does not matter: it is a maker order 
anyway, getting the rebate or the low fee. If the limit order is not filled
in a few seconds, mbot cancells it and places a new one.
This is done by smart_ex object.


Take profit

This is easy to understand. Imagine price is 11000, you go long (buy)
and also place a take profit order at 12000 "if/when price gets there, sell
and take profit". If you go short (sell), you can place a take profit buy
order at 10000.

This orders are very useful for human traders, because are placed in advance,
so the human can sleep and have (sort of) life. But mbot works 24/7/365,
so does not need take profit orders. When present situation fits the requirements,
places a limit order to close position.


Stop Order

Similar to take profit, but place to cover the posibility that our trade
goes wrong. It is an order with a price "worst" than current one,
could be executed now but only will be executed when price goes
"as bad as there".

Example:
Current price is 11000, you thing price will go up so you buy (you
"go long"), an perhaps place a Take Profit order at 11500. But you also place a 
stop order to sell at 10700, if
price goes against you, you stop loss there.

If you go short, you buy at 11000, perhaps place take profit at 10000 but you
place a stop for instance at 11300.

Stop order can be also placed to open position, not only to close.
Imagine price is 11000, you think that 10000 is a strong support
so if price goes below i.e 9800, will fall much more.
So you place a stop sell order at 9800.

As take profit orders, stop ordersthey are great for humans but mbot does not
need them.

Trailing Stop

Is a stop loss measured relatively, first as a distance from current price. As
price moves in the direction of your trade, the stop loss also moves.

Imagine price is 9400. You open long trade. A standard stop loss is a fixed
price to sell. Say 9300. But if price goes up, say 9500, the 9300 stop is
no more a good exit point, you would loose your gains if trend reverses.  
Trailing stop allows you to lock in gains. 

If price is 9400, you can place a trailing stop with
a trail value of -100, meaning "if price reverses 100 usd from any point,
sell".  If price falls from 9400 to 9300, will sell.  If price rises
to 9500, and then falls, will sell at 9400 (9500-100)

Imagine you are short at 9400. Yoy place a positive trail value. I.e 100.
If price reaches 9500, it will buy (exit at loss). If price falls to 9300, the stop will
move to 9400.

Negative trail implies selling. (We were long)

Positive trail implies buying. (We were short)

Why?  Becasuse if you wanted to buy after prices goes down, a simple limit
order below current price would do.  If you wanted to sell after a postive
moment, a simple limit order above current price would do.


Quantity. The number of contracts you will buy or sell. Always a positive
amount.

Trail value. Negative means selling. Positive means buying.

This order is mostly used to stop loss and lock gains, but it can be used
also to open new positions.

Mbot simulation algorithm does not use not need trailing stops to trader19, 
It is not needed because we continously track price and decide.

But real world is different: our bot might have bugs (as the terrible June
2019 one), or system could be down or the the exchange might be overloaded
and ignoring requests. So, as an extra layer of security, we add trailing 
stop orders. And include them in mbot simulator, to analyze them. 
But if bot works fine, is able to send
orders to the exchange and the exchange execs them, trailing stop orders will
not be executed, the bot will close the position with a limit order.



