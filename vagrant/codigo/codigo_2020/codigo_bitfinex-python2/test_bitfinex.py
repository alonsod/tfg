#!/usr/bin/env python

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789


# test_bitfinex
import bitfinex
import misc
from misc import superprint
import time
import os
import mlogs
#import superglobal

def main():

    key_file='alonsod-bitfinex.key'
    log = get_log()
    p5 = 90
    # p5: timeout to stop placing limit order and start placing market orders

    #sg = get_superglobal()
    bf = bitfinex.Bitfinex(
        key_file = key_file,
        log = log,
        sg = None,
        p5 = p5
    )

    ################# dra-tests #################
    # dra_test_readkey(bf, key_file)
    ################# dra-tests #################
    # test_cancel_all_orders(bf) #OK
    # test_place_order(bf) #OK
    # test_active_orders(bf) #OK
    # test_positions(bf) #OK

    # test_order_status(bf) #OK

    # test_wallets(bf) #OK
    # test_margin_symbol(bf) #OK
    # test_margin_base(bf) #OK

    # test_sleep(bf) #OK
    # test_raw_orderbook(bf) #OK
    # test_orderbook(bf) #OK


    # test_ticker(bf) #OK


    # test_transfer_wallets(bf) #OK
    # test_transfer_all(bf) # OK
    # test_smart_order(bf) #OK
    # trading_qc_to_bc(bf) # NOT OK
    # test_open_position(bf) #OK
    # test_close_position(bf) #OK
    # test_get_wallet(bf) # NOT OK
    # test_sell_all_in_trading(bf) # NOT OK
    # test_buy_all_in_trading(bf) # OK
    return

def dra_test_readkey(bf, filename):
    print("Reading filename "+str(filename))
    key, secret = bf._read_key(filename)
    print("key: "+key+", secret: "+secret)
    return

def test_get_wallet(bf):
    print "bitfinex get_wallet()"
    print bf.get_wallet()
    return

def test_buy_all_in_trading(bf):
    bf.buy_all_in_trading()
    return

def test_sell_all_in_trading(bf):
    bf.sell_all_in_trading()
    return

def test_open_position(bf):
    size = 0.001
    size = .06
    base_price = None
    direction = 1
    direction = -1
    date = None
    r = bf.open_position(size, base_price, direction, date)

def test_close_position(bf):

    price = None  # ignored
    date = None # ignored
    verbose = True
    r = bf.close_position(price, date, verbose)
    print r

    return


def test_transfer_wallets(bf):

    amount = 0.005
    currency = "USD"
    currency = "BTC"

    wfrom = "trading"
    wfrom = "exchange"

    wto = "exchange"
    wto = "trading"
    bf.transfer_wallets(amount, currency, wfrom, wto)


    return


def trading_qc_to_bc(bf):
    """
    We have qc (USD) in the trading wallet. We want bc (BTC). So we tranfer
    everything to trading, buy all, transfer again to exchange
    """

    wfrom = "trading"
    wto = "exchange"
    bf.transfer_all(wfrom, wto)

    bf.cancel_all_orders()
    wallet = "exchange"
    purpose = "open"
    order_amount = "all"
    bf.place_smart_order(wallet, order_amount, side, purpose)

    wfrom = "exchange"
    wto = "trading"
    bf.transfer_all(wfrom, wto)

    return


def test_transfer_all(bf):

    wfrom = "trading"
    wfrom = "exchange"

    wto = "exchange"
    wto = "trading"


    bf.transfer_all(wfrom, wto)

    return


def test_smart_order(bf):
    bf.cancel_all_orders()
    wallet = "exchange"
    wallet = "trading"
    side = "sell"
    side = "buy"

    purpose = "open"
    purpose = "close"

    order_amount = 6.551395
    order_amount = 6.222182
    order_amount = 3.401020
    order_amount = "all"
    order_amount = 0.003

    print side, purpose

    bf.place_smart_order(wallet, order_amount, side, purpose)

    return


def test_order_status(bf):
    id = 104885879
    id = 10488599879
    o = bf.order_status(id)
    print bf.format_order(o)

    return


def test_ticker(bf):
    print bf.ticker()
    return

def test_orderbook(bf):
    o = bf.orderbook()
    print bf.format_orderbook(o,10)
    return

def test_raw_orderbook(bf):
    o = bf.orderbook()
    #misc.superprint(o)
    print "o vale",o

    return





def test_sleep(bf):
    for i in range(0,100):
        bf.sleep(i)
    return
def test_margin_symbol(bf):
    superprint(bf.margin_symbol("BTCUSD"))
    return

def test_margin_base(bf):
    r = bf.margin_base()
    print bf.format_margin_base(r)
    return


def test_wallets(bf):
    w = bf.wallets()
    print bf.format_response(w, "wallets")
    return

def test_positions(bf):
    p = bf.positions()
    print bf.format_response(p, "positions")
    return


def test_active_orders(bf):
    ao = bf.active_orders()
    print bf.format_response(ao,"orders")
    return

def test_cancel_all_orders(bf):
    bf.cancel_all_orders()
    return


def test_place_order(bf):
    ord_type = "market"
    ord_type = "limit"

    price = 7000
    amount = 0.01
    side = "buy"
    side = "sell"


    o = bf.place_order(
        amount = amount,
        price = price,
        side = side,
        ord_type = ord_type
    )

    print  bf.format_order(o)

    x = bf.active_orders()
    print("active orders")
    print(x)

    return



def get_log():
    t= "/tmp/log.test_bitfinex.{}.txt"
    dlogname= t.format(os.getpid())
    return mlogs.log(dlogname)


def place_order(bf):
    o = bf.place_order(
        amount = 0.002,
        price = 9000.0,
        side = "sell",
        ord_type = "limit",
        symbol = "btcusd")
    print "order:",o

    return



#def get_superglobal():
#    config_dir = "~/mbot_config"
#    config_dir = misc.expand_tilde(config_dir)
#
#    sg = superglobal.Superglobal(
#        config_dir = config_dir,
#        run_mode = "real"
#    )
#    return sg


if __name__ == '__main__':
    main()
