#!/usr/bin/python -tt
# mlogs Miguel Jan 2015 Jul 2018

# This module writes text files, with or without buffer

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

import sys
import os
import stat


class log(object):
    def __init__(self, logname, buffer_size=0, append = False):

        if logname == None:
            self.disabled = True
            return
        else:
            self.disabled = False
            # the file fill not be used

        self.logname = logname

        self.buffer_size = buffer_size
        self.append = append
        # will flush the buffer to disk every buffer_size messages

        self.buf = ""
        self.counter = 0
        self.file = None
 
        if buffer_size == 0:
            self.always_open_close = True
            # open and close the file for every message
        else:
            self.always_open_close = False

        if not self.always_open_close:
            self.open_file()
        return

    def chmod(self, mode):
        if self.disabled:
            return

        if mode == 755:
            mode = stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP \
            | stat.S_IROTH | stat.S_IXOTH

        os.chmod(self.logname, mode)
        return

    def write(self, line):
        if self.disabled:
            return

        if self.always_open_close:
            self._write_unbuffered(line)
        else:
            self._write_buffered(line)
        return

    def _write_buffered(self, line):

        self.buf = self.buf + line
        self.counter = self.counter + 1
        if self.counter == self.buffer_size:
            self.flush()
            self.counter = 0
        return

    def _write_unbuffered(self, line):
        self.open_file()
        self.file.write(line)
        self.file.close()
        return

    def reset(self):
        if self.disabled:
            return

        # Erases log file
        self.file = open(self.logname, "w")
        self.file.close()
        return

    def flush(self):
        if self.disabled:
            return
        self.file.write(self.buf)
        self.buf = ""
        return

    def open_file(self):
        if self.disabled:
            return

        if self.always_open_close or self.append:
            mode = "a"
        else:
            mode = "w"

        try:
            self.file = open(self.logname, mode)
        except IOError:
            msg = "can't open log " + self.logname + " mode " + mode

            sys.stderr.write('Error: \n')
            sys.stderr.write(msg + "\n")
            raise SystemExit
        return

    def close(self):
        if self.disabled:
            return

        if not self.always_open_close:
            self.flush()
            self.file.close()
        return

    def __del__(self):
        if self.disabled:
            return

        self.close()
        return
