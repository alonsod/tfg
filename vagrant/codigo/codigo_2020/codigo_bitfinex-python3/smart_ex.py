#!/usr/bin/env python 
# -*- coding: utf-8 -*-

# smart_ex   (smart exchange)    Miguel Apr  2018

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

# The main purpose of this class smart exchange is to be able to manage
# what we call "smart order"

# What is a 'smart order'?
# Exchanges offer us:
# 1) market orders
# We buy or sell, at market price

# 2) limit orders
# We place a sell order price with higher than current market price, 
# or a buy order priced lower than current market price. In the
# hope that waiting some time, the market will move in the right 
# direction and someone will accept our order.
# When we place a limit order, the exchanges freezes the margin in our account
# (the 'money') needed to close the deal (if eventually the deal
# is closed). We can always cancell a limit order before market reaches that
# price.

# Limit orders are the more conveniente ones, exchanges call them "adding
# liquidity to the orderbook". Limit orders provide the exchange
# the 'raw material' that they sell or buy. That's why many exchanges,
# i.e. Bitfinex, offer much lower fees for limit orders. Market orders have
# higer fees. Our purpose is to pay the fees or fhe limit orders.
# Bitmex goes one step forward: for limit orders the "fee" in negative:
# we get a rebate. In other words, the one taking the market order pays the
# fee to the exchange and also a fee to the one who placed the limit order.

# But limit orders are not straightforward to place: What price
# do we ask/bid? How long should we wait the market to reach our price?
# What should be do otherwise?

# Our main trading algorithm does not want to worry about this: when our 
# main trading algorithm triggers an order, we want it to be executed ASAP. 
# But paying the (smaller) fee of the limit order

# Smart orders solve this problem.

# When our algorithmic trader places an ask or buy order as "smart order", 
# the code that we develope here:
# - Places a limit order slighty above / below market price
# - Waits some time
# - If the order was not completely filled, repeats

# But smart orders solve another problem, too.
# Maybe the size of our order is very big for current orderbook
# and could impact very negatively the price (this is called 'slippage', 
# 'recotización' in spanish).
# So, to reduce slippage, smart order places a limit order whith a 'reasonably',
# size, a partial order (we name it "suborder", and if needed, waits and puts 
# another one, and another, until the full amount is sold/bought.
# Obviously slipagge can not be avoided completely, our purpose is to 
# reduce it.
  
# This is our algorithm for smart orders. It uses 5 parameters: p1 to p5.

# We will split our position size in several subpositions. In other
# words, we will split the smart order in several 'dumb' (limit)
# suborders. 

# We consider the p1 (parameter 1) first orders in the orderbook.
# We will call them "the first page", "the page" or  "our page".
# p1 might be 15, for instance.

# We settle the volumen of the first suborder so that its size
# is not higher than p2 *  the volume of the full page.
# p2 might be 0.2, for instance (20% of the volume of the whole page)

# What price do we choose? The price of the p4 (parameter 4) th element
# of the page. 
# p4 might be 1, meaning that the price will be the same as the price
# of the 2nd element of our page  (counting 'C style', the
# first element has ordinal 0)

# After placing the order, we wait p3 seconds. p3 might be 15 or 30 seconds.

# Perhaps the order was filled and we bought/sold all the bc (base currency)
# we wanted to buy/sell. So, we have finished.

# Otherwise, we repeat the whole process.
# There are 3 possible causes, not excluyent, causing the need of the
# repetition:

# 1) The market did not reach our price, the order was not filled at all.
# 2) The market did reach our price, but our order size was too big, so
# it was parcially filled
# 3) The suborder size was smaller than the full smart order size, to reduce
# slippage


# If the liquidity in the market is not enough, it will not be possible to 
# execute the orders as limit. 
# After a timeout of p5 seconds, we place the order as a market order.

import sys
import misc
import time
from misc import superprint
from misc import mprint
from misc import human

P1_DEFAULT = 15 
# asks/bids in the 'page' of the orderbook that we consider

P2_DEFAULT = 0.000001  
P2_DEFAULT = 0.2  
# max volume of each suborder, as fraction of page's volume

P3_DEFAULT = 25
# seconds waiting to the suborder to be filled

P4_DEFAULT = 0
# ordinal of the element in the page whose price we will use for the suborder.
# c style, first one is 0.


P5_DEFAULT = 1500
# we try to place limit orders. But if this timeout expires, we place a market order


class SmartEx(object):
    def __init__(
            self, 
            exchange,
            p1 = P1_DEFAULT,
            p2 = P2_DEFAULT,
            p3 = P3_DEFAULT,
            p4 = P4_DEFAULT,
            p5 = P5_DEFAULT
        ):
        self.ex = exchange
        self.log = exchange.log
        self.side = None
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4
        self.p5 = p5
       
        #t= "[smart_ex] p5 : {}\n"
        #msg = t.format(self.p5)
        #mprint(msg)
        #self.log.write(msg)

        self.obook_report = ""
        # last orderbook report

        return


    def compute_all(self, wallet_name, side):
        if wallet_name != "exchange":
            t = "'all' amount only available for exchange trades, not {}\n"
            msg = t.format(wallet_name)
            self.log.write(msg)
            raise Exception(msg)

        tk = self.ex.ticker()
        if tk["status"] != "ok":
            msg = "can not get ticker\n"
            self.log.write(msg)
            raise Exception(msg)

        ask = float(tk["ask"])
        bid = float(tk["bid"])

        if side == "buy":
            price = ask
        elif side == "sell":
            price = bid
        else:
            t = "Wrong side:{}\n"
            msg = t.format(side)
            self.log.write(msg)
            raise Exception(msg)

        b = self.ex.exchange_wallet_value() 
        if b["status"] != "ok":
            t = "Can not get exchange wallet balance\n{}"
            msg = t.format(b)
        
        bc_balance = b["bc_balance"]
        qc_balance = b["qc_balance"]
       
        if side == "sell":
            amount = bc_balance
            t = "All we have to sell is {} {}\n"
            msg = t.format(bc_balance, self.ex.bc)
            self.ex.log.write(msg)
            mprint(msg)
              
        elif side == "buy":
            fee = .003
            just_in_case = .999 
            amount = just_in_case * ( qc_balance * (1-fee) / float(price) )
            t = "ask: {}. We have {} {}. Fee: {:.3f}\n"
            t += "We can buy {:.6f} {}"
            msg = t.format( 
                ask,
                qc_balance,
                self.ex.qc,
                fee,
                amount,
                self.ex.bc
            )
            self.ex.log.write(msg)
            mprint(msg)
        else: 
            t = "Wrong side: {}"
            msg = t.format(side)
            self.log.write(msg)
            raise Exception(msg) 

        return amount

   
    def place_smart_order(self, wallet, order_amount, side, purpose):

        if order_amount == "all":
            order_amount = self.compute_all(wallet, side)

        rem_amount = order_amount
        # Initially, the remaining amount is the amount of the whole smart
        # order

        t = "[smart_ex] {} smart order to {} {:.4f} {} to {} position\n"
        msg = t.format(
            misc.human(time.time()),
            side,
            order_amount,
            self.ex.bc,
            purpose
        )
        mprint(msg)
        self.log.write(msg)
       
        t0 = time.time()
        while True:
            try:
                self.ex.sg.mbot.update_candles()
                # only to say "hello" to the server, to keep the watcher happy
            except AttributeError:
                pass  
                #  If we are using this method outside mbot (like in 
                #  interface_bitfinex.py, then there is no mbot, and no need
                #  call the server to keep watcher quiet

            r1 =  abs(rem_amount) < self.ex.min_order_size
            r2 = rem_amount * order_amount <= 0
            # remaining amount and original amount have opposite sign
            # (we sell/bough something more than the asked, probably
            # because price changed in our benefit)
            # Or any amount is 0
            if r1 or r2:
                t = "[smart_ex] Remaining  {:.6f} {} < min_order_size "
                t += "({:.6f} {})\n"
                msg = t.format(
                    rem_amount,
                    self.ex.bc,
                    self.ex.min_order_size,
                    self.ex.bc
                )
                mprint(msg)
                self.log.write(msg)
                break

            obook=self.ex.orderbook()
            
            f = self.ex.format_orderbook(obook,7)
            print(f, end=' ')
            self.log.write(f)

            suborder_amount, price = \
                self._volume_price(obook, rem_amount, side)

            t = "[smart_ex] Original order: {:.3f} {}. Remaining: {:.3f} {}."
            t += " Suborder: {:.3f} {}\n"
            msg = t.format(
                order_amount,
                self.ex.bc,
                rem_amount,
                self.ex.bc,
                suborder_amount,
                self.ex.bc
            )
            mprint(msg)
            self.log.write(msg)

            if wallet == "trading":
                order_type = "limit"
            elif wallet == "exchange":
                order_type = "exchange limit"
            else:
                t = "wrong wallet: {}"
                msg = t.format(wallet)
                self.log.write(msg)
                raise Exception(msg)

            ellapsed_time = time.time() - t0
            if ellapsed_time >= self.p5:
                order_type = order_type.replace("limit","market")
                # We tried to place the order as limit but we quit. 
                # Let's place the order and market and pay the fee 
                t = "[smart_ex] Ellapsed ({}) >= Timeout ({})."
                t += "Changing to market order\n"    
                msg = t.format(
                    misc.timedelta(ellapsed_time),
                    misc.timedelta(self.p5)
                )
                mprint(msg)
                self.log.write(msg)
                forced_market = True
            else:
                forced_market = False
                pass

            last_order = purpose == "close" and rem_amount == suborder_amount

            o = self.ex.place_order(
                amount = suborder_amount,
                price = price, 
                side = side, 
                ord_type = order_type,
                purpose = purpose,
                last_order = last_order
            )

            if o["status"] == "completed":
                break
        
            if o["amount_reduced"] != None:
                rem_amount = float( o["amount_reduced"])
                # The exachange says that we have no enough balance, so
                # we reduce the remaining amount to the amount that
                # the exchange allowed us to place

            if o["status"] == "too_small":
                break
            
            if o["status"] == "no_balance":
                break

            time.sleep(1)

            #t = "{} We have just placed the order\n{}"
            #msg = t.format( 
            #    human(time.time()),
            #    self.ex.format_order(o)
            #)
            #mprint(msg)
            #self.log.write(msg)

            order_id =  o["order_id"]

            if self.order_well_placed(order_id):
                time.sleep(self.p3) 
            else:
                obook=self.ex.orderbook()
                msg += self.ex.format_orderbook(obook)
                mprint(msg)
                self.log.write(msg)
             
                time.sleep(3)
            
            self.ex.cancel_all_orders()
            time.sleep(3)
            self.ex.cancel_all_orders()
            # in bitfinex sometimes this seems to fail, that's why we do 
            # it twice

            if forced_market:
                rem_amount -= suborder_amount
                # when we force market order, executed_amount does not work.
                # But we know that the whole suborder is executed
            else:
                rem_amount -= self.executed_amount(order_id)
      
        # We need the order with the is_postonly parameter, so if the
        # price is not above/below market, it is cancelled
       

        t = "Smart order completed in {}!\n"
        msg = t.format(
            misc.timedelta(time.time() - t0)
        )
        mprint(msg)
        self.log.write(msg)

        return


    def _see_orders(self):
        o = self.ex.get_my_orders()
        r = ""
        for order in o["msg"]:
            r += self.ex.format_order(order)
        return r

    def check_order_amount(self, order_amount,  side):
        """
        We compute the order size according to the current state of the market.
        But when we split a smart order in several dumb limit order, market
        can change against so that we do not have enough margin to that order
        size.
        This method detects that situation, changing the amount to the maximum
        possible, if neccesary.

        This method is only called when opening a position, only in that case
        we need to check margin. 
        """

        # This function does not seem to work in bitfinex because we sometimes get 
        # a bizarre 0 value from exchange.
        # Seems better to place the order anyway, detect the "not enough 
        # balance" value and reduce the amount

        r = self.ex.max_position_size()

        if r["status"] != "ok":
            t = "Can not check max position size.\n"
            t = "We'll just suppose that current size is ok\n{}"
            msg = t.format(r["msg"])
            mprint(msg)
            self.log.write(msg)
            return order_amount


        max_long = float(r["max_long"])
        max_short = float(r["max_short"])

        if side == "buy":
            max_size = max_long
        elif side == "sell":
            max_size = max_short
        else:
            t = "Wrong side: {}\n"
            msg = t.format(side)
            self.log.write(msg)
            raise Exception(msg)
     
        t = "max_size:{} order_amount:{}\n"
        msg = t.format(max_size, order_amount)
        mprint(msg)
        self.log.write(msg)

        if  order_amount > max_size:
            t = "Amount {} {} became too big for our margin.\n"
            t += "Reducing it to {} {}\n"
            msg = t.format(
                order_amount,
                self.ex.bc,
                max_size,
                self.ex.bc
            )
            order_amount = max_size
        else :
            t = "Amount {} {} is ok, under maximum ({} {})\n"
            msg = t.format(
                order_amount,
                self.ex.bc,
                max_size,
                self.ex.bc
            )
               
        mprint(msg)
        self.log.write(msg)

        return order_amount


    def _volume_price(self, obook, rem_amount, side):
        """
        Receives the orderbook, returns the size and price of the suborder
        """

        if side == "buy":
            max_amount, price = self._bvp(obook["bids"][:self.p1])
        elif side == "sell":
            max_amount, price = self._bvp(obook["asks"][:self.p1])
        else:
            t = "wrong side parameter: {}"
            msg = t.format(side)
            self.log.write(msg)
            raise Exception(msg)
         
        suborder_amount = min(rem_amount, max_amount)
        # We do not place an order bigger than needed, even though
        # we were allowed to do so.

        suborder_amount = max(self.ex.min_order_size, suborder_amount)
        # We do not place an order smaller than minimum order.
        # (This could happen when we test the algorith and tweak
        # the parameters to force very small suborders)

        return(suborder_amount, price)


    def _bvp(self, ab):
        """
        bvp: basic volume price
        Receives a list of asks or bids, returns the volume of the
        list, the size and price of our suborder
        """
        total_vol = 0
        for x in ab:
            total_vol += x["amount"]
        max_amount = total_vol * self.p2
        price = ab[self.p4]["price"]

        t = "{} [smart_ex] Vol: {:.0f} {}. Max suborder: {:.1f} @ {:.0f}\n"
        msg = t.format(human(
            time.time()), 
            total_vol, 
            self.ex.bc,
            max_amount, 
            price
        )
        self.log.write(msg)
        self.obook_report = msg

        return  max_amount, price


    def order_well_placed(self,order_id):
        """
        We convert a 'smart sell' order into a 'limit sell' suborder,
        priced slighty above current market price.
      
        We convert a 'smart buy order' into a 'limit buy' suborder, priced
        slighty below current market price.

        But some time happens between the moment we check the orderbook
        and the moment we place the order. So the price might have moved
        in our direction. So the limit order would not be a limit order.
        In bitfinex we use is_postonly parameter. So, in this case, the
        exchange would cancel the order. This function checks if the 
        order was cancelled for that reason
        """
 
        #print "Let's see the orders"
        #print self._see_orders()
        o = self.ex.order_status(order_id)
       
        #t = "Let's check this order\n{}"
        #msg = t.format( self.ex.format_order(o))
        #mprint(msg)
        #self.log.write(msg)

        if o["status"] == "not_found":
            well_placed = False
        else:
            well_placed = not o["is_cancelled"]
        if not well_placed:
            t = "{} [smart_ex] Order cancelled{}\n"
            msg = t.format(
                human(time.time()), 
                self.ex.format_order(o)
            )
            self.log.write(msg)
            mprint(msg)

        else:
            t = "{} order well placed\n"
            msg = t.format( human(time.time()))
            mprint(msg)
            self.log.write(msg)
            
        return well_placed

    def executed_amount(self,order_id):
        o = self.ex.order_status(order_id)
        if o["status"] == "not_found":
            ea = 0
            msg = "Order not found, executed amount equivalent to 0\n"
            mprint(msg)
            self.log.write(msg)

            return ea


        ea = float(o["executed_amount"])
        t = "{} [smart_ex] order id {}. Executed amount: {:.6f} \n"
        msg = t.format(
                human(time.time()), 
                order_id,
                ea
        )
        mprint(msg)
        self.log.write(msg)
       
        return ea



