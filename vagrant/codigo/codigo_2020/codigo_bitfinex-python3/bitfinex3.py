#!/usr/bin/python3

# bitfinex.pyx   Miguel Feb 2018    David March 2020

# v2 api code based on official documentations
# v1 api code based on 
# https://github.com/dawsbot/bitfinex/blob/master/FinexAPI/FinexAPI.py

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

import sys
import requests
import json
import misc
import time
import base64
import hashlib
import hmac
from misc import superprint
from misc import human
from misc import mprint

import smart_ex

MIN_ORDER_SIZE = 0.004

TIME_INTER_CHECKS_MARGIN = 60 * 5  # seconds. 
# This value is dynamically updated, check the source

TIME_INTER_CHECK_TICKER = 60
TIME_INTER_CHECK_WALLETS = 40
# To avoind asking the same thing too often

MAX_SLEEP = 60 * 15
# When we get an error from bitfinex, we wait 3, 6, 12, 24...
# seconds until sending a new request. Doubling the sleep time.
# Until this sleep time reaches this maximum, after that we always
# sleep this maximum, without increasing it.

JUST_IN_CASE = 0.995
# We apply this factor to computed  max_position_size, to avoid
# different rounding making our order too big.
# Indeed it is not very relevant, for some reason bitfinex does
# not honor the max_position_size that gave us a moment before,
# we always get an "not enough" (tradable balance/exchange balance), 
# so we reduce  something and try again, until the order is accepted.

REDUCE_SOMETHING = 0.9
# If we get a "not enough" error, we try again,
# multiplying the order by this factor

P1_DEFAULT = 15 
# asks/bids in the 'page' of the orderbook that we consider

P2_DEFAULT = 0.000001  
P2_DEFAULT = 0.2  
# max volume of each suborder, as fraction of page's volume

P3_DEFAULT = 25
# seconds waiting to the suborder to be filled

P4_DEFAULT = 0
# ordinal of the element in the page whose price we will use for the suborder.
# c style, first one is 0.

P5_DEFAULT = 1500
# When this timeout expires, we assume that it is not possible to place the 
# order as limit, so places remaining orders as market order.
# (For a new smart order, we try limit order again)

INITIAL_ATTEMPTS = 12
# When client can not connect to bitfinex, waits some time and tries again
# This time is shorter in initial attempts, longer after. Check source


class Trade(object):
    direction = 0
    # 0: no trade, out of market
    # 1:  going long, bull trade, buying
    # -1: going short, bear trade, selling


class Bitfinex(object):
    def __init__(
            self,
            sg,
            key_file, 
            log,  
            symbol='BTCUSD',
            p1 = P1_DEFAULT,
            p2 = P2_DEFAULT,
            p3 = P3_DEFAULT,
            p4 = P4_DEFAULT,
            p5 = P5_DEFAULT
        ):

        self.sg = sg
        self.name = "bitfinex"
        self.exchange = self.name
        self.log = log
        self.key, self.secret = self._read_key(key_file)
        self.symbol = symbol
        if symbol == "BTCUSD":
            self.bc = "BTC"  # Base Currency
            self.qc = "USD"  # Quote Currency
        else:
            t = "Unknown symbol:{}\n"
            msg = t.format(symbol)
            self.log.write(msg)
            raise Exception(msg)

        self.decimals_bc = 8
        self.decimals_qc = 8
        self.min_order_size = MIN_ORDER_SIZE
        self.base_url ='https://api.bitfinex.com/'
        self.trade = Trade()

        self.last_time_checked_margin = 0
        self.time_inter_checks = TIME_INTER_CHECKS_MARGIN
        # We do not check the margins continuously, it would mean to
        # ask the server the status of our account each few seconds,
        # it would have no sense. Perhaps we could decrease this time
        # if our position is in 'bad shape'

        self.last_time_checked_ticker = 0
        self.last_ticker_seen = None

        self.last_time_checked_wallets = 0
        self.last_wallets_seen = None

        self.smart_ex = smart_ex.SmartEx(
            self,
            p1 = p1,
            p2 = p2,
            p3 = p3,
            p4 = p4,
            p5 = p5
        )

        return


    def _read_key(self, key_file):
        key_file = misc.expand_tilde(key_file)
        try:
            f = open(key_file,'r')
        except IOError:
            msg = "Can not open "+key_file
            self.log.write(msg)
            raise Exception(msg)
        lines  = f.readlines()
        key = lines[0]
        secret = lines[1]
        key = key.strip()
        secret = secret.strip()
        f.close()
        
        t = "Using key {}\n"
        msg = t.format(key_file)
        self.log.write(msg)

        return key, secret


    def _nonce(self):
        """
        Returns a nonce
        Used in authentication
        """
        n=str(int(round(time.time() * 1000)))
        return n


    def _check_response(self,resp):
        try:
            rval = json.loads(resp.text)
        except ValueError:
            rval = {}
            status =  "No JSON object could be decoded"
            rval["status"] = status
            return status, rval

        if resp.status_code == 200 or resp.status_code == 201:
            status = "ok"
        else:  
            status = "error"
            t = "{} status code {}\n"
            msg = t.format(human(time.time()), resp.status_code) 
            self.log.write(msg)
            print(msg)
        return status, rval


    def _payloadPacker(self, payload): 
        # packs and signs the payload of the request.
        # API v1
    
        j = json.dumps(payload)

        j_encoded = j.encode()
        data = base64.standard_b64encode(j_encoded)

        secret_encoded = self.secret.encode()
        h = hmac.new(secret_encoded, data, hashlib.sha384)
        signature = h.hexdigest()

        return {
            "X-BFX-APIKEY": self.key,
            "X-BFX-SIGNATURE": signature,
            "X-BFX-PAYLOAD": data
        }

    
    def _headers(self, path, nonce, body):
        # API v2

        signature ="/api/" + path + nonce + body

        secret=self.secret.encode('utf-8')
        signature = signature.encode('utf-8')

        h = hmac.new(secret, signature, hashlib.sha384)
        signature = h.hexdigest()

        return {
            "bfx-nonce": nonce,
            "bfx-apikey": self.key,
            "bfx-signature": signature,
            "content-type": "application/json"
        }


   

    def logarithmic_sleep(self,i):
        s = min( MAX_SLEEP,  2 * (2 ** i) )
        t = "I will retry in {}\n"
        msg = t.format(misc.timedelta(s))
        mprint(msg)
        time.sleep(s)
        return

    def arithmetical_sleep(self,i):
        s = 2
        t = "I will retry in {}\n"
        msg = t.format(misc.timedelta(s))
        mprint(msg)
        time.sleep(s)
        return


    def sleep(self,i):
        if i < INITIAL_ATTEMPTS:
            self.arithmetical_sleep(i)
        else:
            self.logarithmic_sleep(i-INITIAL_ATTEMPTS)
        return


    def open_position(
        self, size, base_price, direction, date, 
        trail_value_per=None  # TODO currently not implemented
        ):
        """
        Wrapper to place_smart_order(), using the same interface
        than simulations interface
        """

        if size <= 0 :
            t = "Wrong size: {}. Size must be greater than 0\n"
            msg = t.format(size)
            self.log.write(msg)
            raise Exception(msg)

        amount = size
        price = base_price
        if direction == 1:
            side = "buy"
        elif direction == -1:
            side = "sell"
        else:
            t = "Expecting 1 or -1 , found {}"
            msg = t.format(direction)
            self.log.write(msg)
            raise Exception(msg)
        wallet = "trading"
      
        purpose = "open"
        r = self.place_smart_order(wallet, amount, side, purpose) 

        # For some reason, bitfinex does not allow us to place an order for
        # the max size allowed for the leverage. But after that, recognizes
        # that we have margin available. Probably is something related with
        # delays in updating account margin status. 
        # So we just wait a few seconds and place the order again, with 
        # reduced amount. Our software will try to place all avaliable 
        # margin in the order

        time.sleep(90)
        amount = 0.4 * amount
        msg = "Placing smart order again\n"
        self.log.write(msg)
        mprint(msg)
        r += self.place_smart_order(wallet, amount, side, purpose) 

        return str(r) + "\n"
        

    def claim_position(self, price, date, verbose=False):
        r = self.close_position(price, date, verbose)
        t = "Buying {} with {} in trading wallet\n"
        r += t.format(
            self.bc,
            self.qc
        )
        self.buy_all_in_trading()
        return r
 

    def close_position(self, price, date, verbose=False):
        r = self.positions()
        if r["status"] != "ok":
            return r["status"]
        else:
            msg = "No positions opened"
            for position in r["positions"]:
                amount = position["amount"]
                # Bitfinex uses negative values to show that the position 
                # was short. But to place the order, we must always use 
                # positive values.
                if amount > 0:
                    opening_side = "buy"
                else:
                    opening_side = "sell"
                    amount = amount * -1

                msg = self.place_order_to_close(amount, opening_side) + "\n"
        self.buy_all_in_trading()
        return msg


    def place_order_to_close(self,amount, opening_side):
        if opening_side == "buy":
            side = "sell"  # was long, we close selling
        elif opening_side == "sell":
            side = "buy"
        else:
            t = "wrong opening_side: {}\n"
            msg = t.format(opening_side)
            self.log.write(msg)
            raise Exception(msg)
        wallet = "trading"
        purpose = "close"
        self.place_smart_order(wallet, amount, side, purpose)
        return "ok"


    def place_order(self, amount, price, side, ord_type, \
            purpose="open", last_order=None
        ):
        """
        The wallet is implicit in the ord_type
  
        This function is called by place_smart_order (that is called
        from open_position)
        """

        # last_order is needed for bitmex, unused here in bitfinex

        if amount <= 0 :
            t = "Wrong amount: {}. amount must be greater than 0\n"
            msg = t.format(amount)
            self.log.write(msg)
            raise Exception(msg)

        symbol = self.symbol.lower()
        exchange = self.exchange

        t = "{} Placing {} {} order. {} {} @ {} {}\n"
        msg = t.format(
            human(time.time()),
            ord_type,
            side,
            amount,
            self.bc,
            price,
            self.qc,
        )
        mprint(msg)
        self.log.write(msg)

        amount_reduced = None
        # if this value is != None, means that we reduced the requetested
        # amount in the order because the exchange said that we have
        # not enough balance
    
        i = 0
        while True:
            r = self._ll_place_order(amount, price, side, ord_type, \
                symbol, exchange)
            if r["status"] == "ok":
                r = r["msg"]
                r["status"] = "ok"
                r["amount_reduced"] = amount_reduced
                break
            elif r["msg"].find("not enough") != -1:
                r["status"]= "no_balance"
                amount = amount * REDUCE_SOMETHING
                t = "Not enough balance. Reducing the order to {:.8f} {}\n"
                msg = t.format(
                    amount,
                    self.bc
                )
                mprint(msg)
                self.log.write(msg)
                    
                amount_reduced = amount
                self.sleep(0)
            elif r["status"] == "too_small":
                r["amount_reduced"] = amount_reduced
                r["order_id"] = None
                break
            else:
                t =  "{} {}  Can not place order\n"
                msg = t.format(human(time.time()), r["status"])
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        return r


    def cancel_all_orders(self):
        t = "{} Cancelling all orders\n"
        msg = t.format( human(time.time()))
        self.log.write(msg)
        mprint(msg)
        i = 0
        while True:
            r = self._ll_cancel_all_orders()
            if r["status"] == "ok":
                break
            else:
                t = "{} Can not cancel all orders\n"
                msg = t.format( human(time.time()))
                self.log.write(msg)
                mprint(msg)
                self.sleep(i)
                i += 1
        return r


    def active_orders(self):
        """
        Fetch active orders. Using API v1
        """
        #t = "{} Getting active orders\n"
        #msg = t.format( human(time.time()))
        #mprint(msg)
        i = 0
        while True:
            r = self._ll_active_orders()
            if r["status"] == "ok":
                break
            else:
                t = "{} Can not get active orders\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.sleep(i)
                i += 1
        return r


    def order_status(self, order_id):
        """
        Gets order status. Using API v1
        """
        if order_id == None:
            r = {}
            r["status"] = "not_found"
            return r
        order_id = int(order_id)
        i = 0
        while True:
            r = self._ll_order_status(order_id)
            if r["status"] == "ok":
                break
            if r["status"] == "not_found":
                break
            else:
                t = "{} Can not get order status\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.sleep(i)
                i += 1
        return r


    def positions(self):
        """
        Fetch positions
        """
        i = 0
        while True:
            r = self._ll_positions()
            if r["status"] == "ok":
                break
            else:
                t = "{} Can not get positions\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        return r


    def number_of_positions(self):
        r = self.positions()
        npos = len(r["positions"])
        return npos

    def wallets(self):
        """
        Fetch wallets info
        """
        i = 0
        
        current_time = time.time()
        if (current_time - self.last_time_checked_wallets) < TIME_INTER_CHECK_WALLETS:
            return self.last_wallets_seen
        else:
            self.last_time_checked_wallets = current_time
        while True:
            r = self._ll_wallets()
            if r["status"] == "ok":
                break
            else:
                t = "{} Can not get wallets\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        self.last_wallets_seen = r
        return r


    def check_wallet_name(self,purpose, wallet):
        """
        Bitfinex does not use a consistent naming for the wallets,
        the name of the wallets changes if we are checking funds or
        moving balance.
        """

        if purpose == "transfers":
            names = ["trading", "deposit", "exchange"]
        elif purpose == "info":
            names = ["margin", "deposit", "exchange"]
        else:
            t = "Wrong purpose: {}\n"
            msg = t.format(purpose)
            self.log.write(msg)
            raise Exception(msg)

        if not  wallet in names:
            t = "Wrong wallet: {}\n"
            
            t += "Expecting: "
            for x  in names:
                t += x
                t += " "
            t += "\n"
           
            msg = t.format(wallet)
            self.log.write(msg)
            raise Exception(msg)

        return


    def transfer_all(self, wfrom, wto):
        self.check_wallet_name("transfers", wfrom)
        self.check_wallet_name("transfers", wto)

        r = self.wallets()
        wallets = r["wallets"]

        if wfrom == "trading":
            wfrom_info = "margin"
        else:
            wfrom_info = wfrom

        if wto == "trading":
            wto_info = "margin"
        else:
            wto_info = wto

        amount_bc = self.get_wallet(wallets, wfrom_info, self.bc)
        amount_qc = self.get_wallet(wallets, wfrom_info, self.qc)

        if amount_bc >  10 ** (-1 * self.decimals_bc ):
            self.transfer_wallets(amount_bc, self.bc, wfrom, wto)
        else:
            t = "No {} to transfer from {} wallet to {} wallet\n"
            msg = t.format(
                self.bc,
                wfrom,
                wto
            )
            mprint(msg)
            self.log.write(msg)
   
        time.sleep(15)

        if amount_qc > 10 ** (-1 * self.decimals_qc):
            self.transfer_wallets(amount_qc, self.qc, wfrom, wto)
        else:
            t = "No {} to transfer from {} wallet to {} wallet\n"
            msg = t.format(
                self.qc,
                wfrom,
                wto
            )
            mprint(msg)
            self.log.write(msg)
        return


    def transfer_wallets(self, amount, currency, wfrom, wto):
        """
        Transfer inter wallets.
        """
        self.check_wallet_name("transfers", wfrom)
        self.check_wallet_name("transfers", wto)
  
        if wfrom == wto:
            t = "'wallet from' and 'wallet to' can not be the same ({})\n"
            msg = t.format(wfrom)
            self.log.write(msg)
            raise Exception(msg)

        if currency != self.bc and currency != self.qc:
            t = "Wrong currency ({}). Expecting {} or {}\n"
            msg = t.format(
                currency,
                self.bc,
                self.qc
            )
            self.log.write(msg)
            raise Exception (msg)

        t = "{} Transfering {} {} from {} to {}\n"
        msg = t.format(
            human(time.time()),
            amount,
            currency,
            wfrom,
            wto
        )
        self.log.write(msg)
        mprint(msg)

        i = 0
        while True:
            r = self._ll_transfer_wallets(amount, currency, wfrom, wto)
            if r["status"] == "ok":
                break
            else:
                t = "{} Can not transfer inter wallets\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        return r


    def orderbook(self):
        """
        Fetch orderbook
        """
        i = 0
        while True:
            r = self._ll_orderbook()
            if r["status"] == "ok":
                break
            else:
                t = "{} Can not get orderbook\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        return r


    def _ll_place_order(self, amount, price, side, ord_type, 
        symbol, exchange): 
        """
        Low level place order
        """

        if amount < self.min_order_size:
            t = "Amount {} is below the minimum ({})\n"
            msg = t.format(
                amount,
                self.min_order_size
            )
            mprint(msg)
            self.log.write(msg)
            r = {}
            r["status"] = "too_small"
            r["msg"] = msg
            return r

        if ord_type != "market" and \
            ord_type != "limit" and \
            ord_type != "stop" and \
            ord_type != "trailing-stop" and \
            ord_type != "fill-or-kill" and \
            ord_type != "exchange market" and \
            ord_type != "exchange limit" and \
            ord_type != "exchange stop" and \
            ord_type != "exchange trailing-stop" and \
            ord_type != "exchange fill-or-kill" :
            t = "wrong order type: {}"
            msg = t.format(ord_type)
            self.log.write(msg)
            raise Exception(msg)

        # type starting by “exchange ” are exchange orders, others are 
        # margin trading orders)

        if ord_type == "market" :
            price = 1
        # price does not matter for market orders, but the
        # system expects a positive value

        if side != "buy" and side != "sell":
            t = "wrong side parameter: {}"
            msg = t.format(side)
            self.log.write(msg)
            raise Exception(msg)

        amount = misc.number_to_string_number(amount)
        price = misc.number_to_string_number(price)
        payload = {
    		"request":"/v1/order/new",
    		"nonce": self._nonce(),
    		"symbol":symbol,
    		"amount":amount,
    		"price":price,
    		"exchange":exchange,
    		"side":side,
    		"type":ord_type,
            "is_postonly": True
    	}

        # is_postonly True:
        # For limit orders, this paremeter forces the order to be
        # limit. So that if our limit order happens to match
        # an existing order in the other side of the book, the order
        # is not placed
    
        signed_payload = self._payloadPacker(payload)
        url = self.base_url + "v1/order/new" 

        rval = self._post( 
            url,
            "v1",
            signed_payload
        )

        return rval


    def _post(self, url, v, signed_payload, raw_body=""):
        """
        Calls requests to post the order, using bitfinex API v1 or v2
        """
        r = ""
        rval={}
        try:
            if v == "v1":
                r = requests.post(
                        url,
                        headers=signed_payload, 
                        verify=True
                )
            elif v == "v2": 
                r = requests.post(
                        url,
                        headers=signed_payload, 
                        data=raw_body,
                        verify=True
                )
            else:
                t = "Wrong v:{}\n"
                msg = t.format(v)
                self.log.write(msg)
                raise Exception(msg)

            status, r = self._check_response(r)
            msg =  r

        except  requests.exceptions.ConnectionError:
        #except  Exception as e:
            status =  "Connection refused\n"
            status += str( sys.exc_info()[1])
            msg = "" 

        if status != "ok":
            t = "{} {} {}\n"
            msg = t.format(
                human(time.time()),
                status,
                r
            )
            mprint(msg)
            self.log.write(msg)

        rval["status"] = status
        rval["msg"] = msg
        return rval


    def _ll_active_orders(self):
        """
        low level active orders 
        """
        payload = {
    		"request":"/v1/orders",
    		"nonce": self._nonce()
    	}
    
        signed_payload = self._payloadPacker(payload)
        url = self.base_url + "v1/orders" 
        rval = self._post( 
            url,
            "v1",
            signed_payload
        )
        
        if rval["status"]=="ok":
            orders = rval["msg"]
            rval={}
            rval["status"] = "ok"
            rval["orders"] =  orders

        return rval


    def _extract_message(self, p):
        try:
            msg = p["message"]
        except:
            msg = p
        return msg


    def _ll_transfer_wallets(self, amount, currency, wfrom, wto):
        """
        low level transfer wallets
        """

        amount = str(amount)
 
        payload = {
    		"request":"/v1/transfer",
            "amount": amount,
            "currency": currency,
            "walletfrom": wfrom,
            "walletto": wto,
    		"nonce": self._nonce()
    	}

        signed_payload = self._payloadPacker(payload)
        url = self.base_url + "v1/transfer" 
        rval = self._post( 
            url,
            "v1",
            signed_payload
        )

        high_level_status = None
        low_level_status = None
        try:
            high_level_status = rval["status"]
            # we were able to place the request

            low_level_status = rval["msg"][0]["status"]
            # the request was completely executed

        except (KeyError, TypeError):
            t = "{} KeyError or TypeError in transfer wallet order\n{}\n"
            msg = t.format(
                human(time.time()),
                rval
            )
            self.log.write(msg)
            mprint(msg)
            rval["status"] = "error"

        if high_level_status == "ok" and low_level_status == "success":
            msg = str(rval["msg"][0]["message"])+"\n"
            rval={}
            rval["status"] = "ok"
            rval["msg"] = msg
            self.log.write(msg)
            mprint(msg)
        elif high_level_status == "ok" and  str(rval).find("balance insufficient") != -1:
            msg = "Insufficient balance, ignoring order.\n"
            self.log.write(msg)
            mprint(msg)
            rval["status"] = "ok"
            rval["msg"] = msg

            # if the error is due to no balance, we don't want to try and try 
            # again. We consider transfer done, higher levels should fix it,
            # if needed.
        else:
            t = "{} Error in transfer wallet order:{}\n"
            error_msg =  self._extract_message(rval["msg"][0])
            msg = t.format(
                human(time.time()),
                error_msg
            )
            self.log.write(msg)
            mprint(msg)
            rval["status"] = "error"

        return rval


    def _ll_order_status(self, order_id):
        """
        low level order status
        """
        payload = {
    		"request":"/v1/order/status",
    		"nonce": self._nonce(),
            "order_id": order_id
    	}
    
        signed_payload = self._payloadPacker(payload)
        url = self.base_url + "v1/order/status" 
        rval = self._post( 
            url,
            "v1",
            signed_payload
        )

        if rval["status"]=="ok":
            rval= rval["msg"]
            rval["status"] = "ok"
        elif rval["msg"].find("No such order") != 1:
            rval["status"] = "not_found"
            t = "Order id {} not found"
            rval["msg"] = t.format(order_id)
        return rval


    def _ll_positions(self):
        """
        Low level positions
        """
        nonce = self._nonce()
        body = {}
        raw_body =json.dumps(body)
        path = "v2/auth/r/positions"

        headers = self._headers(path, nonce, raw_body)

        url = self.base_url + path 

        #r = requests.post(
        #    url,
        #    headers=headers, 
        #    data=raw_body, 
        #    verify=True
        #)

        r = self._post( url, "v2", headers, raw_body)

        rval={}
        if r["status"] == "ok":
            rval["status"] = "ok"
            positions = []
            self.trade.direction = 0 
            for position in r["msg"]:
                parsed = self._parse_position(position)
                positions.append(parsed)

                if parsed["amount"] > 0 :
                    self.trade.direction = 1
                else:
                    self.trade.direction = -1
                # FIXME  This fails if we have positions in different side,
                # only considers the last one.
                # (But we should not have positions oppend in different side
                # at the same time)
                # The fix: add a function here to check all positions's side
            rval["positions"] = positions
        else:
            rval["status"]= r
        return rval


    def _ll_wallets(self):
        """
        Low level wallets 
        """
        nonce = self._nonce()
        body = {}
        raw_body =json.dumps(body)
        path = "v2/auth/r/wallets"

        headers = self._headers(path, nonce, raw_body)

        url = self.base_url + path

        r = self._post( url, "v2", headers, raw_body)
     
        status = r["status"]
        rval={}
        rval["status"] = status
        if status == "ok":
            wallets = []
            for wallet in r["msg"]:
                parsed = self._parse_wallet(wallet)
                wallets.append(parsed)
            rval["wallets"] = wallets
            rval["status"] = "ok"
        else:
            rval["msg"]= r
        return rval


    def _ll_orderbook(self):
        """
        Low level orderbook
        """
        url = self.base_url+'v1/book/'+self.symbol.lower()
      
        try:
            r = requests.get(url)
            status, r = self._check_response(r)
            asks = r["asks"]
            bids = r["bids"]
             
        except :
            status = "error"
            r = ""
            asks=[]
            bids=[]
       
        rval = {}
        rval["status"] = status
        rval["asks"] = asks
        rval["bids"] = bids
        rval = self._parse_orderbook(rval)

        return rval


    def _ll_margin_symbol(self, symbol):
        """
        Low level margin_symbol
        """
        if symbol != "BTCUSD":
            t = "unknown symbol: {}\n"
            msg = t.format(symbol)
            self.log.write(msg)
            raise Exception(msg)

        nonce = self._nonce()
        body = {}
        raw_body =json.dumps(body)
        bf_symbol = "t"+symbol
        path = "v2/auth/r/info/margin/" + bf_symbol

        url = self.base_url + path
        headers = self._headers(path, nonce, raw_body)

        r = self._post( url, "v2", headers, raw_body)

        status = r["status"]

        if status == "ok":
            rval = self._parse_margin_symbol(r["msg"])
            rval["status"] = "ok"
        else:
            rval = r
        return rval


    def _ll_margin_base(self):
        """
        low_leve margin_base 
        """
        nonce = self._nonce()
        body = {}
        raw_body =json.dumps(body)
        path = "v2/auth/r/info/margin/base"

        headers = self._headers(path, nonce, raw_body)
        url = self.base_url + path

        r = self._post( url, "v2", headers, raw_body)
      
        status = r["status"]
        rval = {}
        if status == "ok":
            rval = self._parse_margin_base(r["msg"])
            rval["status"] = "ok"
        else:
            rval["msg"]= r
        return rval


    def _ll_cancel_all_orders(self):
        """
        low level cancel all orders
        """
        payload = {
    		"request":"/v1/order/cancel/all",
    		"nonce": self._nonce()
    	}
    
        signed_payload = self._payloadPacker(payload)
        url =   self.base_url + "v1/order/cancel/all"

        rval = self._post( 
            url,
            "v1",
            signed_payload
        )

        return rval


    def _ll_ticker(self):
        """
        Low level ticker
        """
        if self.symbol == "BTCUSD":
            symbol = "tBTCUSD"
        else:
            t = "unknown symbol: {}\n"
            msg = t.format(self.symbol)
            self.log.write(msg)
            raise Exception(msg)
        url = self.base_url+'v2/ticker/'+symbol
      
        ctime = 0
        try:
            r = requests.get(url)
            status, r = self._check_response(r)
            bid = float( r[0] )
            bid_size = float( r[1] )
            ask = float( r[2] )
            ask_size = float( r[3] )
            #daily_change = r[4]
            #daily_change_percen = r[5]
            #last_price = r[6]
            #daily_volume = r[7]
            #daily_high = r[8]
            #daily_low = r[9]
            ctime = int(time.time())
            volume = bid_size + ask_size  # TODO check that this is ok
        except :
            status = "error"
            bid = None
            ask = None
            ask_size = None
            bid_size = None
            volume = None
       
        rval = {}
        rval['status'] = status
        rval['bid'] =  bid 
        rval['ask'] =  ask 
        rval['ask_size'] =  ask_size 
        rval['bid_size'] =  bid_size 
        rval['volume'] =  volume 
        rval['ctime'] = ctime
        return rval


    def margin_symbol(self, symbol):
        """
        Fetch margin info about symbols
        """
        i = 0
        while True:
            r = self._ll_margin_symbol(symbol)
            if r["status"] == "ok":
                break
            else:
                t = "Can not get margin_symbol\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        return r


    def tradable_balance(self,symbol):
        ms = self.margin_symbol(symbol)
        rval = {}
        if ms["status"] == "ok":
            rval["status"] = "ok"
            raw = ms["tradable_balance"]
            try:
                rval["tb"] = float(raw)
            except ValueError:
                rval["tb"] = 0
                rval["status"] = "ValueError: "+raw
        else:
            rval["status"] = "error"
            rval["tb"] = 0
        return rval


    def margin_base(self):
        """
        Fetch margin base
        """
        i = 0
        while True:
            r = self._ll_margin_base()
            if r["status"] == "ok":
                break
            else:
                t =  "{} Can not get margin_base\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        return r


    def _parse_margin_symbol(self,l):
        rval = {}
        rval["tradable_balance"] = float( l[2][0])
        rval["gross_balance"] = float( l[2][1])
        rval["buy"] = float( l[2][2] )
        rval["sell"] = float( l[2][3] )
        return rval


    def _parse_orderbook(self,o):
        rval = {}
        try:
            rval["status"] = "ok"
            rval["asks"] = []
            for x in o["asks"]:
                timestamp = float( x["timestamp"])
                price = float( x["price"])
                amount = float(x["amount"])
                new_x = {}
                new_x["timestamp"] = timestamp
                new_x["price"] = price
                new_x["amount"] = amount
                rval["asks"].append(new_x)

            rval["bids"] = []
            for x in o["bids"]:
                timestamp = float( x["timestamp"])
                price = float( x["price"])
                amount = float(x["amount"])
                new_x = {}
                new_x["timestamp"] = timestamp
                new_x["price"] = price
                new_x["amount"] = amount
                rval["bids"].append(new_x)

        except :
            rval["status"] = "Wrong orderbook format"
            rval["msg"] = o
        
        r1 = len(rval["asks"]) >=  self.smart_ex.p1
        r2 = len(rval["bids"]) >=  self.smart_ex.p1
        if not r1 or not r2:
            status = "Wrong length in orderbook"
            rval["status"] = status
            rval["msg"] = o
            print(status)
            print(o)
       
        return rval


    def _parse_margin_base(self,l):
        rval = {}
        rval["user_pl"] = float( l[1][0] )
        rval["user_swaps"] = float( l[1][1] )
        rval["margin_balance"] = float( l[1][2] )
        rval["margin_net"] = float( l[1][3] )
        return rval


    def _parse_position(self,l):
        rval = {}
        rval["symbol"] = l[0]
        rval["status"] = l[1]
        rval["amount"] = float( l[2] )  # negative for shorts
        rval["base_price"] = float( l[3] )
        rval["margin_funding"] = l[4]
        rval["margin_funding_type"] = l[5]
        rval["pl"] = float( l[6] )  # profits losses
        rval["pl_perc"] = float( l[7] )  # pl percentage
        rval["liq_price"] = float( l[8] ) 
        rval["leverage"] =  float( l[9] )
        return rval


    def _parse_wallet(self,l):
        rval = {}
        rval["wallet_type"] = l[0]  # exchange | margin | funding
        rval["currency"] = l[1]
        rval["balance"] = float( l[2] )
        rval["unsettled_interest"] = float( l[3] )
        balance_available = l[4]
        if balance_available == None:
            rval["balance_available"] = 0
            # null if not fresh enough
        else:
            rval["balance_available"] = balance_available

        return rval


    def _parse_active_order_v2(self,l):
        rval = {}
        rval["id"] = l[0]
        rval["gid"] = l[1]  # group id
        rval["cid"] = l[2]  # client order id
        rval["symbol"] = l[3]  # negative for shorts
        rval["creation_time"] = l[4]  # ms
        rval["update_time"] = l[5]  #
        rval["amount"] = float( l[6] )  # negative for selling
        rval["amount_orig"] = float( l[7] ) # original amount
        rval["type"] = l[8]  
        rval["type_prev"] = l[9]  # previous type
        rval["flags"] = l[10]  # liquidation price
        rval["status"] = l[11]  
        rval["price"] = float( l[12] )
        rval["avg_price"] = float( l[13] )
        return rval


    def max_position_size(self):

        r = self.tradable_balance(self.symbol)
        rval={}
        rval["max_long"] = 0
        rval["max_short"] = 0
        if r["status"] != "ok":
            rval["status"] = r["status"]
            rval["msg"] = r
            tb = 0
            return rval
        else:
            rval["status"] = "ok"
            tb = r["tb"]
        
        r = self.ticker()
        if r["status"] != "ok":
            rval["status"] = r["status"]
            return rval

        try:
            ask = float(r["ask"])
            bid = float(r["bid"])
        except ValueError:
            rval["status"]="ValueError"
            return rval

        if ask == 0 or bid == 0:
            t = "Null ask or bid. Ask:{} Bid:{}"
            msg = t.format(ask, bid)
            rval["status"] = msg
            return rval

        rval["max_long"] = JUST_IN_CASE * (tb / ask)
        rval["max_short"] = JUST_IN_CASE * (tb / bid)

        t = "[bitfinex] max position size. long:{:.3f} short:{:.3f}\n"
        msg = t.format( rval["max_long"], rval["max_short"])
        mprint(msg)
        self.log.write(msg)

        return rval


    def ticker(self):
        current_time = time.time()
        if (current_time - self.last_time_checked_ticker) < TIME_INTER_CHECK_TICKER:
            return self.last_ticker_seen
        else:
            self.last_time_checked_ticker = current_time
        i = 0
        while True:
            r = self._ll_ticker()
            if r["status"] == "ok":
                break
            else:
                t = "{} Can not get ticker\n"
                msg = t.format( human(time.time()))
                mprint(msg)
                self.log.write(msg)
                self.sleep(i)
                i += 1
        self.last_ticker_seen = r
        return r


    def current_trade_status(self, price, date, verbose=False):
        """
        This function returns the status of the current trade, 
        (the profits and losses) and the fees spent.

        This version ignores 'price' and 'date' parameters, we keep
        then to keep the same interface than the dummy exchange used
        in backtesting.
        """
     
        r = self.positions()
        rval = {}
        if r["status"] != "ok":
            msg = "Can not get current positions"
            return 0, 0, 0, msg
     
        pl = 0
        self.ipv = 0  # initial position value
        if len(r["positions"]) > 1  :
            t = "There should be at most 1 position opened!\npositions:{}"
            msg = t.format(r["positions"])
            self.log.write(msg)
            raise Exception(msg)
            
        for pos in r["positions"]:
            try:
                pl += float(pos["pl"])
                self.ipv += float(pos["amount"]) * float(pos["base_price"])
                self.liq_price = float(pos["liq_price"])
            except ValueError:
                rval["status"] = "ValueError"
                return rval

        fees = 0  # FIXME
        funding_cost = 0   #FIXME
        t = "{} pl: {} {} fees: {} {} funding_cost: {} {}\n"
        msg = t.format(
            human(time.time()),
            pl,
            self.qc,
            fees,
            self.qc,
            funding_cost,
            self.qc,
        )
        self.log.write(msg)
        mprint(msg)
        return pl, fees, funding_cost, msg


    def deposit_margin(self, deposit_qc, deposit_bc):
        """
        Not used in real exchange, keeped here to keep the same interface
        than dummy exchange used in simulations.
        """
        return


    def get_wallet(self, wallets,  wtype, currency):

        purpose = "info"
        self.check_wallet_name(purpose, wtype)

        r = 0.0
        for wallet in wallets:
            if wallet["currency"] == currency \
                and wallet["wallet_type"] == wtype:
                r = float( wallet["balance"])

        return r


    def exchange_wallet_value(self):
        r = self.wallets()
        if r["status"] != "ok":
            print("Can not fetch wallets info")
            return r
    
        bc_balance = 0  # base currency. i.e. BTC
        qc_balance = 0   # quiote currency. i.e. USD
        for wallet in r["wallets"]:
            if wallet["currency"] == self.qc \
                and wallet["wallet_type"] == "exchange":
                   qc_balance = wallet["balance"]
            if wallet["currency"] == self.bc \
                and wallet["wallet_type"] == "exchange":
                   bc_balance = wallet["balance"]

        try: 
            bc_balance = float(bc_balance)
            qc_balance = float(qc_balance)
        except ValueError:
            t = "wrong balance: {} {}"
            msg = t.format(bc_balance, qc_balance)
            self.log.write(msg)
            raise Exception(msg)

        rval = {}
        rval["status"] = "ok"
        rval["bc_balance"] = bc_balance
        rval["qc_balance"] = qc_balance

        tk = self.ticker()
        if tk["status"] != "ok":
            print("can not get ticker")
            return r
        
        ask = float(tk["ask"])
        bid = float(tk["bid"])

        price = bid
         
        rval["tot_value"] = bc_balance * price + qc_balance

        return rval

        
    def wallet_value(self, wallet = None, price = None, verbose = False):
        
        """
        Total value of margin wallet, expressed as qc.
        Parameters 'wallet' and  'price' are not used here,
        but they are necessary to keep the same interface than the
        synthetic exchange used in simulations.
        attention: THIS METHOD ONLY CONSIDERS MARGIN WALLET
        """
        r = self.wallets()
        if r["status"] != "ok":
            print("Can not fetch wallets info")
            return r

        time.sleep(1)
        tk = self.ticker()
        if tk["status"] != "ok":
            print("can not get ticker")
            print(tk["status"])
            return r
        
        ask = float(tk["ask"])
        bid = float(tk["bid"])

        price = bid

        tot_value = 0
        for wallet in r["wallets"]:
            if wallet["currency"] == self.qc \
                and wallet["wallet_type"] == "margin":
                    balance = float(wallet["balance"])
                    t = "We have {:.2f} {} in margin wallet\n"
                    msg = t.format(balance, self.qc)
                    tot_value += balance
                    self.log.write(msg)
                    if verbose:
                        mprint(msg)

            if wallet["currency"] == self.bc \
                and wallet["wallet_type"] == "margin":
                    balance = float(wallet["balance"])
                    value = balance * price
                    
                    tot_value += value
                    t = "We have {} {} in margin wallet @ {} {}\n"
                    t += "Value: {:.2f} {}\n"
                    msg = t.format( 
                        balance , 
                        self.bc,
                        price, 
                        self.qc,
                        value,
                        self.qc
                    )
                    self.log.write(msg)
                    if verbose:
                        mprint(msg)
               
        t = "Total value:{} {} \n"
        msg = t.format(tot_value, self.qc)
        
        rval={}
        rval["status"] = "ok"
        rval["wb"] = tot_value
        return rval


    def margin_balance(self, price, date, verbose=False):
        """
        Initial deposit plus realized PLs plus floating PLs, expressed
        as qc (USD)

        This version ignores 'price' and 'date' parameters, we keep
        then to keep the same interface than the dummy exchange used
        in backtesting.
        """
   
        r = self.wallet_value()
        if r["status"] != "ok":
            print("can not get wallet value")
            return r
        else:
            wallet_value = r["wb"]

        cts, fees, funding_cost, status = \
            self.current_trade_status(price, date, verbose)

        if cts == 0:
            lp = 0
        else:
            lp = self.liq_price
        mb = wallet_value + cts
        # margin balance

        # FIXME
        # self.ipv is calculated in current_trade_status()
        # Should be a parameter returned by current_trade_status()
        # but at this moment we don't want to modify this interface 
 
        if self.ipv != 0:
            mp = 100.0 *  mb / abs( self.ipv)
            status = "ok"
        else:
            status = "No trade opened"
            mp = 100
        # margin percentage
 
        t = "{} mb:{:8.2f} {}  mp:{} lp:{} status:{}\n"
        msg = t.format(
            human(time.time()),
            mb,
            self.qc,
            mp,
            lp,
            status
        )
 
        self.log.write(msg)
        
        return mb, mp, lp,  status


    def check_margins(self, price, date, verbose=False):
        if (time.time() - self.last_time_checked_margin) < self.time_inter_checks:
            mc = False
            mb = self.last_mb
            mp = self.last_mp
            lp = self.last_lp
            msg =""
            return mc, mb, mp, lp, msg
        else:
            self.last_time_checked_margin = time.time()

        mb, mp, lp ,  msg = self.margin_balance(price, date, verbose)
        mc = False  
        # If we were margin called, we would know it by other means

        if mp > .40:
            self.time_inter_checks = 60 * 30
        elif mp > .25:
            self.time_inter_checks = 60 * 15
        elif mp > .20:
            self.time_inter_checks = 60 * 10
        else: 
            self.time_inter_checks = 60 * 3

        self.last_mc = mc
        self.last_mb = mb
        self.last_mp = mp
        self.last_lp = lp
        return mc, mb, mp, lp, "ok"
    

    def format_response(self,r, data_type):
        if data_type != "orders" \
            and data_type !="positions" \
            and data_type !="wallets":
                t = "wrong data_type:{}\n"
                msg = t.format(data_type)
                self.log.write(msg)
                raise Exception(msg)
    
        msg = "\t\t\t"+ data_type.upper()+"\n"
        try:
            if r["status"] != "ok":
                msg = r
            else:
                the_data = r[data_type]
                if len(the_data) == 0:
                    msg = "No active "+data_type+"\n"
                else:
                    for d in the_data:
                        d["status"] = "ok"
                        if data_type =="orders":
                            msg += self.format_order(d) 
                        elif data_type =="positions":
                            msg += self.format_position(d) 
                        elif data_type =="wallets":
                            msg += self.format_wallet(d) 
        except TypeError:
             msg = "Wrong format in response containing " + data_type
        return msg


    def format_order(self,o):
        if o["status"] == "not_found":
            return "Order not found\n"
        if o["status"] == "too_small":
            return "Order too small, not placed\n"

        try:
            order_id = o["id"]
        except KeyError:
            order_id = o["order_id"]
        except:
            t =  "Unrecognized order format:\n{}\n"
            msg = t.format(o)
            self.log.write(msg)
            raise Exception(msg)
        # For some reason, Bitfinex sometimes uses the key "id",
        # others "order_id"

        try:
            t = "Id:{}. Timestamp: {}. Side:{}.\nSymbol:{}. Type:{}. "
            t += "Price: {} {}."
            t += "Avg ex price: {} {}.\n"
            t += "Amount: {} {}. "
            t += "Executed amount: {} {}. "
            t += "Remaining amount: {}. {}\n"
            t += "Live:{}. Cancelled:{}.  Hidden:{}.\n"
            msg = t.format(
                order_id,
                human(float(o["timestamp"])),
                o["side"],
                o["symbol"], 
                o["type"],
                o["price"],
                self.qc,
                o["avg_execution_price"],
                self.qc,
                o["original_amount"],
                self.bc,
                o["executed_amount"],
                self.bc,
                o["remaining_amount"],
                self.bc,
                o["is_live"],
                o["is_cancelled"],
                o["is_hidden"]
            )
    
        except TypeError:
            msg = "Wrong order format:"+str(o)+"\n"
        return msg


    def format_position(self,p):
        msg = ""
        try:
            t = "Status: {}. Symbol: {}\n"
            t += "Amount: {:.8} {}. Base price: {:.2f} {}. "
            t += "PL: {:.2f} {} ({:.2f}%)\n"
            t += "Leverage: {:.2f}. Liq price: {:.2f} {}.\n"
            t += "Margin funding type:{}. Margin funding:{}.\n"
            
            msg = t.format(
                p["status"],
                p["symbol"],
                p["amount"],
                self.bc,
                p["base_price"],
                self.qc,
                p["pl"],
                self.qc,
                p["pl_perc"],
                p["leverage"],
                p["liq_price"],
                self.qc,
                p["margin_funding_type"],
                p["margin_funding"]
 
            )
        except TypeError:
            msg = "Wrong position format:"+str(p)+"\n"
        return msg


    def format_margin_base(self,p):
        msg = ""
        try:
            t = "Margin_net: {:.2f} {}. Margin balance: {:.2f} {}.\n"
            t += "User swaps: {:.2f} {}. User pl: {:.2f} {}.\n"
            msg = t.format(
                p["margin_net"],
                self.qc,
                p["margin_balance"],
                self.qc,
                p["user_swaps"],
                self.qc,
                p["user_pl"],
                self.qc
            )
        except TypeError:
            msg = "Wrong position format:"+str(p)+"\n"
        return msg


    def format_wallet(self,w):
        msg = ""
        try:
            t = "Wallet type: {}. Currency: {}.\n"
            t += "Balance: {}. Balance available: {}. "
            t += "Unsettled interest: {}.\n"
            
            msg = t.format(
                w["wallet_type"],
                w["currency"],
                w["balance"],
                w["balance_available"],
                w["unsettled_interest"]
 
            )
            if w["currency"] == "BCH":
                msg = ""
        except TypeError:
            msg = "Wrong order format:"+str(w)+"\n"
        return msg


    def format_orderbook(self,o, maxorders=15):
        if maxorders < 2:
            t = "Wrong value for maxorders: {}\n"
            msg = t.format(maxorders)
            self.log.write(msg)
            raise Exception(msg)
        msg = ""
        try:
            msg = ""
            i = maxorders - 1  # We count 'c style', starting in 0
            while True:
                if i < 0:
                    break
                ask = o["asks"][i]

                t = "{:.1f} {:8.5f}\n"
                msg += t.format(
                    ask["price"],
                    ask["amount"]
                )
                i -=1
                
            msg += "     >--<\n"
            for bid in o["bids"][:maxorders]:
                t = "{:.1f} {:8.5f}\n"
                msg += t.format(
                    #human(float(bid["timestamp"])),
                    bid["price"],
                    bid["amount"]
                )

        except TypeError:
            msg = "Wrong order format:"+str(o)+"\n"
        except IndexError:
            msg = "Wrong order format, index related:"+str(o)+"\n"
        return msg


    def place_smart_order(self, wallet, amount, side, purpose):
        self.smart_ex.place_smart_order(wallet, amount,side, purpose)

        return "smart order placed"


    def buy_all_in_trading(self):
        """
        We have qc (USD) in the trading wallet. We want bc (BTC). So we tranfer
        everything to exchange, buy all, transfer again to trading
        """
        r = self.wallets()
        wallets = r["wallets"]
        wtype = "margin"
        currency = "USD"
        amount_qc =  self.get_wallet(wallets,  wtype, currency)

        wfrom = "trading"
        wto = "exchange"
        if amount_qc > 10 ** (-1 * self.decimals_qc):
            self.transfer_wallets(amount_qc, self.qc, wfrom, wto)
        else:
            t = "No {} to transfer from {} wallet to {} wallet\n"
            msg = t.format(
                self.qc,
                wfrom,
                wto
            )
            mprint(msg)
            self.log.write(msg)
    
        self.cancel_all_orders()
        wallet = "exchange"
        side = "buy"
        purpose = "open"
        order_amount = "all"
        self.place_smart_order(wallet, order_amount, side, purpose)
    
        wfrom = "exchange"
        wto = "trading"
        self.transfer_all(wfrom, wto)
     
        return
    

def save(self,  amount):
        """
        Adds specified amount of bc to savings.
        Not implemented yet
        """
        r={}
        r["status"] = "ok"
        r["msg"] = "Not implemented"
        return r
