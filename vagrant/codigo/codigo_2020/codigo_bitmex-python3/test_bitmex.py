#!/usr/bin/env python3

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

import mbitmex3
import misc
from misc import superprint
from misc import mprint
import time
import os
import mlogs

BULL = 1
BEAR = -1

class Superglobal(object):
    def __init__(self, run_mode):
        self.run_mode = run_mode
        return


def main():

    # key_file='~/mbot_config/bitmex_miguel.key'
    key_file='./bitmex_alonsod_testnet.key'
    # key_file='~/mbot_config/bitmex_miguel_testnet.key'

    testnet = "auto"
    # If your bitmex key belongs to the testnet, must include the string
    # 'testnet' in the file name. So, when 'testnet' values 'auto', this
    # object will use the URI of testnet bitmex, not the real one

    log = get_log()
    symbol = "BTCUSD"
    leverage = None
    botid = "botid-test"

    p1 = 15 # the size of the "page" we consider in the orderbook
    p2 = 0.2  # max amount of our order related to the volume in page
    p3 = 25  # seconds waiting for the suborder to complete
    p4 = 0  # ordinal of our order in the page

    p5 = 20
    p5 = 0
    p5 = 1500
    p5 = 30
    # p5: timeout to stop placing limit orders and start placing market orders
    # If 0, all order are market orders

    sg = Superglobal(run_mode = "real")

    bm = mbitmex3.Bitmex(
        sg = sg,
        key_file=key_file,
        log=log,
        leverage = leverage,
        symbol=symbol,
        testnet = testnet,
        botid = botid,
        p1 = p1,
        p2 = p2,
        p3 = p3,
        p4 = p4,
        p5 = p5
    )

    #botid = "botid-XXXX"
    #bm2 = bitmex.Bitmex(
    #    key_file=key_file,
    #    log=log,
    #    symbol=symbol,
    #    testnet = testnet,
    #    leverage = leverage,
    #    botid = botid
    #)

    test_orderbook(bm)
    # test_margin_balance(bm)
    #test_close_position(bm)
    # test_open_position(bm)
    # test_cancel_all_orders(bm)
    #test_positions(bm)

    #test_wallet_value(bm)
    # test_current_trade_status(bm)
    #test_ticker(bm)
    #test_get_wallet(bm)
    #test_wallet_summary(bm)
    #test_get_position(bm)
    #test_fetch_trades(bm)

    #test_get_leverage(bm)

    #test_place_order_almost_immediate(bm)
    #test_get_position_as_qc(bm)
    #test_place_basic_order(bm)
    #test_place_order(bm)
    #test_get_orders(bm)

    #test_wallet_history(bm)

    #test_trade_history(bm)
    #test_execution(bm)
    #test_get_my_orders(bm)
    #test_order_status(bm)
    #test_order_status_ok(bm)
    #test_interval_realized_pl(bm)
    #test_trailing_stop(bm)

    return


def test_trailing_stop(bm):

    direction = BULL
    direction = BEAR

    trail_value_per = 5  # Percentaje

    p = bm.ticker()
    if direction == BULL:
        base_price = p["ask"]
    else:
        base_price = p["bid"]

    trail_value = \
            int( trail_value_per * .01 * float(base_price) * direction * -1 )

    t = "Placing traling stop order. Trail value: {} USD ({:3.1f}%)\n"
    msg = t.format(trail_value, trail_value_per)
    print(msg)


    l = bm._set_trailing_stop(trail_value)
    superprint(l)
    return


def test_get_leverage(bm):
    l = bm.get_leverage()
    print("Leverage:",l)
    return


def test_fetch_trades(bm):
    print("trades:")

    t0=time.time() - 3600 * 1;
    # 1 hours ago
    output_file_name = "/tmp/bitmex_data.csv"

    t = bm.fetch_trades(
        t0=t0,
        output_file_name=output_file_name,
    )
    superprint(t)
    return


def test_positions(bm):
    p = bm.positions()
    print("positions:")
    superprint(p)
    return


def test_current_trade_status(bm):
    verbose = True
    price = None
    date = None
    print(bm.current_trade_status(
        price = price,
        date = date,
        verbose = verbose
    ))
    return


def test_wallet_value(bm):
    verbose = True
    print(bm.wallet_value(verbose))
    return


def test_margin_balance(bm):
    price = None
    date = None  # ignored in real exchanges
    verbose = True
    print(bm.margin_balance(
        price = price,
        date = date,
        verbose = verbose
    ))
    return


def test_interval_realized_pl(bm):
    t1 = misc.my_format_to_unix("2018-07-12")
    t2 = time.time()
    verbose = True
    bm.interval_realized_pl(
        t1=t1,
        t2=t2,
        verbose=verbose
    )

    return


def test_get_position_as_qc(bm):
    p = bm._get_position_as_qc()
    t = "Position: {} {}\n"
    msg = t.format(p[0] , bm.qc)
    mprint(msg)



def test_order_status(bm):
    r = bm.order_status("0f2dddf3-713e-6ddd-b075-45ab26f8358c")
    #r = bm.order_status("xxxx0f2dddf3-713e-6ddd-b075-45ab26f8358c")
    print( r)
    return


def test_order_status_ok(bm):

    p = {'status': 'ok', 'original_amount': 0.0051, 'avg_execution_price': 6671, 'remaining_amount': 0.0, 'order_id': u'0f2dddf3-713e-6ddd-b075-45ab26f8358c', 'timestamp': 1531079724.063, 'symbol': u'XBTUSD', 'executed_amount': 0.005246589716684155, 'price': 6671.0, 'is_live': False, 'is_cancelled': True, 'is_hidden': False, 'cl_order_id': u'botid-test.2018.7.8.19.55.23', 'type': u'limit', 'side': u'sell'}


    wrong_p = {'status': 'ok',  'avg_execution_price': 6671, 'remaining_amount': 0.0, 'order_id': u'0f2dddf3-713e-6ddd-b075-45ab26f8358c', 'timestamp': 1531079724.063, 'symbol': u'XBTUSD', 'executed_amount': 0.005246589716684155, 'price': 6671.0, 'is_live': False, 'is_cancelled': True, 'is_hidden': False, 'cl_order_id': u'botid-test.2018.7.8.19.55.23', 'type': u'limit', 'side': u'sell'}



    #p["status"] = "something_happend"

    print(bm.order_status_ok(wrong_p))

    return


def test_close_position(bm):

    price = None  # ignored
    date = None # ignored
    verbose = True
    r = bm.close_position(price, date, verbose)
    print(r)

    return


def test_execution(bm):
    r = bm.get_execution()
    misc.superprint(r)
    return


def test_trade_history(bm):
    r = bm.get_trade_history()
    misc.superprint(r)
    return


def test_cache(bm):

    bm.get_wallet_history()
    return


def test_place_order(bm):

    price = 6000
    amount = 0.01
    side = "sell"
    side = "buy"

    r = bm.place_order(
        amount = amount,
        price = price,
        side = side,
        ord_type = "limit"
    )

    time.sleep(1)
    return


def test_get_orders(bm):
    r = bm._get_orders()
    misc.superprint(r)
    return


def test_get_my_orders(bm):
    print("My orders:")
    r = bm.get_my_orders()
    for order in r["msg"]:
        print(bm.format_order(order))

    return


def test_open_position(bm):
    size = 0.001
    size = .001    # bc
    base_price = None
    direction = 1
    direction = -1
    date = None
    r = bm.open_position(size, base_price, direction, date)
    return


def test_cancel_all(bm):
    r = bm._cancel_all()
    superprint(r)
    return



def test_place_order_almost_immediate(bm):
    r = bm.cancel_all_orders()

    r = bm.ticker()
    amount = .001
    amount = 0.002

    price = r["ask"]
    side = "sell"

    price = r["bid"]
    side = "buy"

    r = bm.place_order(
        amount = amount,   # BTC
        price = price,
        side = side,
        ord_type = "limit"
    )

    return


def test_place_basic_order(bm):
    amount_as_bc = None
    amount_as_bc = 0.001
    amount_as_bc = 1

    amount_as_qc = 3
    amount_as_qc = None

    price = None
    price = 6300
    side = "sell"
    side = "buy"
    ord_type = "market"
    ord_type = "limit"

    purpose = "close"
    purpose = "open"

    r = bm._place_basic_order(
        amount = amount_as_bc,
        price = price,
        side = side,
        ord_type = ord_type,
        purpose = purpose,
        amount_as_qc = amount_as_qc
    )

    print(r)
    return


def test_close_position_dump(bm):
    # does not consider that there can be several positions, neither
    # partial closes to avoid slippage

    r = bm.get_position()
    if r["status"] != "ok":
        print(r)
        raise SystemExit

    amount = 0.1
    amount_as_qc = None

    amount = None
    amount_as_qc = 30834

    amount_as_qc = r["msg"][0]["current_qty"]
    print("vendemos ", amount_as_qc)
    raise SystemExit

    price = r["ask"]
    side = "sell"

    price = r["bid"]
    side = "buy"

    purpose = "close"

    r = bm.place_order(
        amount = amount,
        price = price,
        side = side,
        ord_type = "limit",
        purpose = purpose,
        amount_as_qc = amount_as_qc
    )

    print(r)
    return


def test_cancel_all_orders(bm):
    print("Probando \"test_cancel_all_orders\"")
    r = bm.cancel_all_orders(
    )
    print(r)
    return


def test_ticker(bm):
    r = bm.ticker()
    print(r)
    return


def test_get_position(bm):
    r = bm.get_position()
    misc.superprint(r)

    return


def test_orderbook(bm):
    #misc.superprint( bm.get_orderbook(depth=3))
    obook =  bm.orderbook(depth=10)
    print("\n\ntest_orderbook")
    print(bm._trace_obook(obook))
    print("\n\n\n\n")
    print(bm.format_orderbook(obook))
    return


def test_get_wallet(bm):
    print("get_wallet:")
    verbose = True
    r = bm.get_wallet(verbose = verbose)
    misc.superprint(r)
    return


def test_wallet_history(bm):
    r = bm.get_wallet_history()

    print(bm.format_wallet_history(r))
    return


def test_wallet_summary(bm):
    print("wallet summary:")
    r = bm.get_wallet_summary()

    print(bm.format_wallet_summary(r))
    return


def test02(bm):
    verb = "GET"
    path = '/api/v1/instrument'
    expires = 1518064236 # 2018-02-08T04:30:36Z
    params = ""
    data = ''
    print(bm._get_signature(
        verb = verb,
        path = path,
        params = params,
        expires = expires,
        data = data
    ))


def test03(bm):
    verb = "GET"
    path = '/api/v1/instrument'
    expires = 1518064237
    params = {}
    params["filter"] = '{"symbol": "XBTM15"}'
    data = ''
    print(bm._get_signature(
        verb = verb,
        path = path,
        params = params,
        expires = expires,
        data = data
    ))
    return


def test04(bm):
    verb = "POST"
    path = '/api/v1/order'
    expires = 1518064238
    params = {}
    data = '{"symbol":"XBTM15","price":219.0,"clOrdID":"mm_bitmex_1a/oemUeQ4CAJZgP3fjHsA","orderQty":98}'
    print(bm._get_signature(
        verb = verb,
        path = path,
        params = params,
        expires = expires,
        data = data
    ))



def get_log():
    t= "/tmp/log.test_bitmex.{}.txt"
    dlogname= t.format(os.getpid())
    return mlogs.log(dlogname)


if __name__ == '__main__':
    main()
