#!/usr/bin/env python

import misc
import pytz
import mbitmex

def main():
    # Prueba 1
    # Ver que hace misc.expand_tilde
    # key_file = "~/tfg/vagrant/codigo_bitmex/bitmex_alonsod_testnet.key"
    # key_file = misc.expand_tilde(key_file) # misc.expand_tilde cambia el ~ por el home en el caso de estar contenido en la ruta del fichero
    # print(key_file)

    # Prueba 2
    # probamos a leer la clave del fichero bitmex_alonsod_testnet.key
    # key, secret = mbitmex._read_key(key_file)
    # print("The key: ", key)
    # print("The secret: ", secret)

    # Prueba 3
    # intentamos HolaMundo en python3
    
    return 0



if __name__ == '__main__':
    main()
