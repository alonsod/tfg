mbot code to work with bitmex.
Miguel, Nov 2019


The main file is
mbitmex.py

It is checked in test_bitmex.py

Other files contain auxilary code.



Dependences:

Tested on ubuntu xenial64
Does not work on ubuntu bionic, because of a python OpenSSL bug
https://github.com/ccxt/ccxt/issues/2522


apt-get install -y python python-pip 
pip install --upgrade pip
pip install pyyaml requests python-telegram-bot pytz six


Need to add environment variable MBOT_DATA_DIR
For instance
mkdir $HOME/mbot_data
export MBOT_DATA_DIR=$HOME/mbot_data

Also need your bitmex private key, as specified in key_file
at test_bitmex.py



