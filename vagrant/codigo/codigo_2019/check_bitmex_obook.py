#!/usr/bin/env python 
# -*- coding: utf-8 -*-

#        1         2         3         4         5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

# check_bitmex_obook  Miguel, Ag 2018
# Checks that the orderbook we receive from bitmex has the right syntax
# (Sometimes it does not)



import misc
import six

from misc import mprint

class CheckBitmexObook(object):
    def __init__(
            self, 
            symbol = "XBTUSD"
        ):
        self.symbol = symbol

        self.sells = None
        self.buys = None
        return

    def check(self, obook, min_len):
        self.min_len = min_len
        # Minimum length of the list

        if not isinstance(obook, list):
            print "Not a list"
            return False


        self.sells = 0
        self.buys = 0

        self.last_buy = 1e9
        self.last_sell = 1e9

        for item in obook:
            if not self.item_ok(item):
                print "Wrong item"
                return False 
        
        if self.sells < self.min_len:
            print "Not enough sells",
            print self.sells
            return False
       
        if self.buys < self.min_len:
            print "Not enough buys",
            print self.buys
            return False

        return True

        
    def item_ok(self,item):

        if not isinstance(item, dict):
            print "Not a dict"
            return False

        keys = ["price", "symbol", "size", "side"]
        
        if not misc.dict_has_keys(item, keys):
            print "Wrong keys"
            return False

        r1 = isinstance( item["price"] , float)
        r2 = isinstance( item["price"] , int)
        if not r1 and not r2:
            print "Neither float not int"
            return False
         
        if item["symbol"] != self.symbol:
            print "Wrong symbol"
            return False

        if not isinstance(item["size"] , int):
            print "Not int"
            return False

        if item["side"] == "Buy" :
            self.buys += 1
            if item["price"] > self.last_buy:
                print "Out of sequence"
                return False
           
            self.last_buy = item["price"]
        elif item["side"] == "Sell":
            self.sells += 1
            if item["price"] > self.last_sell:
                print "Out of sequence"
                return False
            self.last_sell = item["price"]
        else:
            print "Neither buy not sell"
            return False

        return True


    def string_containing_float(self, s):
        """
        Checks if s is as string, containing a float.
        """ 
        
        if not isinstance(s, six.string_types):
            return False
        try:
            n = float(s)
            r = True
        except ValueError:
            r = False

        return r


def main():
    return

if __name__ == "__main__":
    main()



    

