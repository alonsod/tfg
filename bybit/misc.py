#!/usr/bin/env python3 

#        1         2         3         4        / 5         6         7
# 34567890123456789012345678901234567890123456789012345678901234567890123456789

# misc.py  Miguel, dic 2014
# pip install pytz

import sys
import datetime
import time
from pytz import timezone
import pytz
import math
import subprocess
import os, shutil
import json
import numbers
import iso8601

local_tz = pytz.timezone('Europe/Madrid')
human_date_output=True
# if true, date in files is wrote legible for humans. Else, unix time

BULL = 1
BEAR = -1

bcolor={
    'HEADER' : '\033[95m',
    'OKBLUE' : '\033[94m',
    'OKGREEN' : '\033[92m',
    'WARNING' : '\033[93m',
    'FAIL' : '\033[91m',
    'ENDC' : '\033[0m',
    'BOLD' : '\033[1m',
    'UNDERLINE' : '\033[4m'
}



SECS_IN_DAY = 86400  # 3600 * 24
def uptime():
    """
    Returns upttime as seconds.
    There are alternative libraries, but they do not work well for us.
    We want to work in linux and also MacOS, so we can not use
    linux options. Just call the shell command and parse the output.
    """
    s = subprocess.check_output('uptime')

    # test examples
    #s ="19:22:56 up 103 days,  8:43,  2 users,  load average...."
    #s = "19:28  up  2:42, 7 users, load averages...."
    #s = "20:33:30 up 57 min, 10 users,  load average: 0,85, 0,53, 0,39"


    s = s.split(" up ")[1]

    s = s.replace("days","day")
    i = s.find("day")
    if i != -1:
        days = int(s[0:(i-1)])
        s = s[i+5:]  # text after 'day, '
    else:
        days = 0

    s = s.split(",")[0]
    s = s.strip()

    if s.find("min") != -1:
        hours = 0
        minutes = int( s.split(" ")[0])
    else:
        hours, minutes = s.split(":")
        hours = int(round(float(hours)))
        minutes = int(round(float(minutes)))

    r = days * SECS_IN_DAY + hours * 3600 + minutes * 60
    t = "days:{} hours:{} min:{}\n"
    msg = t.format(days, hours, minutes)
    print(msg)

    return r

def system_uptime():
    uptime_seconds = uptime()
    uptime_string = str(datetime.timedelta(seconds = uptime_seconds))
    return uptime_string

def remove_directory_contents(folder):
    for f in os.listdir(folder):
        file_path = os.path.join(folder, f)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print(e)
    return

def add_color(text, color=None):
    # vim does not recognize colors (by default)
    if color==None:
        startcolor=""
        endcolor=""
    else:
        color=color.upper()
        if  not color in list(bcolor.keys()):
            raise NameError("Wrong color '"+color+"'")
        startcolor=bcolor[color]
        endcolor=bcolor["ENDC"]

    output=startcolor+text+endcolor
    return output


def age_add_color(text, age, client_name = ""):
    if client_name.find("watcher") == 0:
        limit = 1900
    else:
        limit = 180

    if age >= limit:
        #color =  "WARNING"
        color =  "FAIL"
    else:
        color = "OKGREEN"
    return add_color(text, color)


def utc_to_local(utc_dt):
    local_dt = utc_dt.replace(tzinfo=pytz.utc).astimezone(local_tz)
    return local_tz.normalize(local_dt)


def unix_time_to_iso(timestamp):
    tz = pytz.timezone('UTC')
    return (datetime.datetime.fromtimestamp(timestamp, tz).isoformat())


def mprint(arg1,*args):
    sys.stdout.write(str(arg1)),
    if len(args)>0:
        sys.stdout.write(" " )
    for arg in args:
        sys.stdout.write(str(arg)+" " ),

def mvprint(*args):  # verbatim print
    for arg in args:
        sys.stdout.write(str(arg)),

def humanize_time_if_needed(d):
    if human_date_output and isinstance(d["ctime"],float):
        d["ctime"]=unix_time_to_datetime(d["ctime"])
    return d

def dict_to_csv(d,*k):
    """    Receives a dict and a list of keys.
        Returns a csv line containing its values"""
    rval=""
    for j in k[:-1]:
        rval=rval+str(d[j])+", "
    rval=rval+str(d[k[-1]])
    return rval

def csv_to_dict(line,*k):
    """ Receives a line and a list of keys.
        Returns a dict containing the values"""
    rval={}
    i=0
    items=line.split(",")
    for j in k:
        if j=="ctime":
            rval[j]=items[i]
        else:
            rval[j]=float(items[i])
        i=i+1
    return rval

def datetime_to_my_date_format(dt):
    u=datetime_to_unix_time(dt)
    return unix_time_to_my_date_format(u)

def datetime_to_unix_time(dt):
    epoch = datetime.datetime(1970, 1, 1, 0, 0, tzinfo=pytz.utc)
    delta = dt - epoch
    return delta.total_seconds()

def my_format_to_unix(s):
    return datetime_to_unix_time( my_date_format_to_datetime(s))

def my_date_format_to_datetime(s):
    """ my date format is yyyy-mm-aa or yyyy-mm-aa hh:mm Considered as GMT"""
    items=s.split("-")
    if len(items) != 3 :
        raise Exception("Wrong format. Expecting yyyy-mm-dd or yyyy-mm-dd hh:mm, not "+s)

    year=int(items[0])
    month=int(items[1])
    hour=0
    minute=0
    foo=items[2].split()
    if len(foo)==2:   # date included hour and minute
        day=int(foo[0])
        hourmin=foo[1]
        hourmin_array=foo[1].split(":")
        if len(hourmin_array)!=2:
            raise Exception("Wrong format. Expecting yyyy-mm-dd or yyyy-mm-dd hh:mm, not "+s)
        hour=int(hourmin_array[0])
        minute=int(hourmin_array[1])
    else: # date did not include hour and minute
        day=int(items[2])
    if ( year < 1000 or year > 2100 or month <1 or   month > 12  \
        or  day <1 or day > 31 or hour<0 or hour>23 or minute<0 or minute >59):
        raise Exception("Wrong format or values. Expecting yyyy-mm-dd or yyyy-mm-dd hh:mm, not "+s)

    dt=datetime.datetime(year,month,day,hour,minute,tzinfo=pytz.utc)
    return dt

def ints_to_unix_time(year,month,day, hour,minute,second):
    dt=datetime.datetime(year,month,day,hour,minute,second, tzinfo=pytz.utc)
    return datetime_to_unix_time(dt)

def ints_to_datetime(year,month,day, hour,minute,second):
    dt=datetime.datetime(year,month,day,hour,minute,second , tzinfo=pytz.utc)
    return dt

def datetime_to_ints(dt):
    return dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second

def unix_time_to_ints(t):
    return datetime_to_ints( unix_time_to_datetime(t))

def ints_to_my_date_format(year,month,day, hour,minute,second):
    return str(year)+"-"+ts(month)+"-"+ts(day)

def my_date_format_to_ints(s):
    """ my date format is yyyy-mm-aa  Considered as GMT"""
    items=s.split("-")
    if len(items) != 3 :
        raise Exception("Wrong format. Expecting yyyy-mm-dd, not "+s)
    year=int(items[0])
    month=int(items[1])
    day=int(items[2])
    if ( year < 1000 or year > 2100 or month <1 or   month > 12  or  day <1 or day > 31):
        raise Exception("Wrong format. Expecting yyyy-mm-dd, not "+s)
    return year,month,day,0,0,0

def rounded_day( unix_time):
    """
    Receives unix_time.
    Returns unix time for the beginning of that day (00:00)
    """
    year, month, day, _, _, _ = unix_time_to_ints(unix_time)
    r = ints_to_unix_time( year, month, day, 0, 0, 0)
    return r

#def unix_time_to_human(date):
#    #return  time.strftime('%Y.%m.%d %H:%M:%S %Z',time.gmtime(date))
#    return  time.strftime('%Y-%m-%d %H:%M:%S %Z',time.localtime(date))

def parse_unix_time(date):

    year= time.strftime('%Y',time.gmtime(date))
    month= time.strftime('%m',time.gmtime(date))
    day= time.strftime('%d',time.gmtime(date))
    hour= time.strftime('%H',time.gmtime(date))
    minutes= time.strftime('%M',time.gmtime(date))
    seconds= time.strftime('%S',time.gmtime(date))
    return int(year),int(month),int(day),int(hour),int(minutes), int(seconds), "UTC"

def unix_time_to_datetime(date):
    year= time.strftime('%Y',time.gmtime(date))
    month= time.strftime('%m',time.gmtime(date))
    day= time.strftime('%d',time.gmtime(date))
    hour= time.strftime('%H',time.gmtime(date))
    minutes= time.strftime('%M',time.gmtime(date))
    seconds= time.strftime('%S',time.gmtime(date))

    year=int(year)
    month=int(month)
    day=int(day)
    hour=int(hour)
    minutes=int(minutes)
    seconds=int(seconds)

    dt=datetime.datetime(year,month,day,hour,minutes,seconds , tzinfo=pytz.utc)

    return dt

def unix_time_to_my_date_format(t):
    dt=unix_time_to_datetime(t)
    year=dt.year
    month=dt.month
    day=dt.day
    rval=str(year)+"-"+ts(month)+"-"+ts(day)
    return rval

def unix_time_to_my_date_format_houred(t):
    dt=unix_time_to_datetime(t)
    year=dt.year
    month=dt.month
    day=dt.day
    hour=dt.hour
    minute=dt.minute
    rval=str(year)+"-"+ts(month)+"-"+ts(day)+" "+str(hour)+":"+str(minute)
    return rval

def datetime_to_week(dt):
    week= (dt.timetuple().tm_yday  / 7.0) +1
    day= ((dt.timetuple().tm_yday)  % 7 )
    if day==0:
        week=week-1

    week=int(math.floor(week))
    dow= dt.timetuple().tm_yday  % 7
    if dow==0:
        dow=7
    return week, dow

def week_to_datetime(year,week,dow):
    day_of_year=(week-1)*7+(dow)

    fdoy=datetime.date(year,1,1) # first day of year
    #day_ordinal=fdoy.timetuple().tm_yday+day_of_year
    foo = datetime.date.fromordinal( day_of_year )
    month=foo.month
    day=foo.day
    rval = datetime.date(year, month,day)
    return rval

def ts(x,n=2):
    """To string. Adding n leading zeros if needed"""
    rval=str(x)
    len_x=len(rval)
    if len_x<n:
        zeros_to_add=n-len_x
        for i in range(zeros_to_add):
            rval="0"+rval

    return rval

def add_days(date_in, x):
    """    Receives a date as yyyy-mm-dd. Returns date corresponding to x days more.
        x can be negative."""
    d=my_format_to_unix(date_in)
    d=d+x*86400
    if x<1:
        rval=unix_time_to_my_date_format_houred(d)
    else:
        rval=unix_time_to_my_date_format(d)
    return rval

def get_ticks_filename(exchange_identifier,starting_time):
    year,month,day=starting_time.split("-")
    rval=exchange_identifier+"."+year+"-"+month+"."
    day=int(day)  # otherwise digits from 0 to 9 include the leading 0
    if day<15:
        rval=rval+"h1"
    else:
        rval=rval+"h2"
    rval=rval+".csv"
    return rval

def erase_hour_if_included(d):
    """Receives data in my date format. If it contains hh:mm, splits it"""

    d=d.split(" ")
    return d[0]

def tick_time(candles):
    return str(unix_time_to_datetime( candles["tick_time"]))

def round_fiat(sg,x):
    return round(float(x),sg.decimals_after_point_qc)

def round_btc(sg,x):
    return round(float(x),sg.decimals_after_point_bc)

def decrement_last_digit_btc(sg,x):
    return x - 10**-sg.decimals_after_point_bc

def decrement_last_digit_fiat(sg,x):
    return x - 10**-sg.decimals_after_point_qc

def almost_same_btc(x,y):
    if abs(x-y) <= 0.0015:
        return True
    else:
        return False

def almost_same_fiat(x,y):
    if abs(x-y) < 0.15:
        return True
    else:
        return False

def print_list(x):
    for y in x:
        superprint(y),

def print_dict(x):
    for key in list(x.keys()):
        mprint(key,":")
        superprint(x[key])
    print("\n")

def superprint(x):
    if isinstance(x,dict):
            print("\n")
            print_dict(x)
    elif isinstance(x,list):
            print_list(x)
    #else :
    #    if x==u'\u0141':
    #        print "<Litecoin>"
    #    elif x==u'\u0e3f':
    #        print "<BTC>"
    #    elif x==u'\xa5':
    #        print "<CNY>"
        # Unix time for 2010-01-01 00:00 and # 2020-01-01
        # We use this for debug, so no big deal if wrong
    elif isinstance(x, numbers.Number) \
        and x > 1262304000 and x < 1577836800:
        msg =  "[" + human(x) + "]"
        print(msg)
    else:
        print(x)

def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    # http://stackoverflow.com/questions/783897/truncating-floats-in-python
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])

def average(*x):
    return sum(x)/float(len(x))

def date_from_now(date):
    """ Express the difference between date and now, in a human-friendly format"""

    lapsus=time.time()-date   # seconds

    if lapsus>240*60:
        msg="at " +str(unix_time_to_datetime(date))
    elif lapsus>89:
        msg= str(  int(lapsus/60)) +" minutes ago "
    else:
        msg=str(lapsus)+" seconds ago"

    return msg

def check_platform():
    if sys.platform == "linux" or sys.platform == "linux2":
        rval="linux"
    elif sys.platform == "darwin":
        rval="macos"
    elif sys.platform == "win32":
        raise Exception("Windows is not supported")
    else:
        msg="unknown platform "+str(sys.platform)
        raise Exception(msg)

    return rval

def human(f, only_date=False):
    # Gets a unix date, returns a string with human legible date
    if only_date:
        y, m, d, _, _, _ =  unix_time_to_ints(f)
        rval = ints_to_my_date_format(y, m, d, 0, 0, 0)
    else:
        rval = str(unix_time_to_datetime(f))

    return rval

def shell_run(command):
    splitted=command.split()

    try:
        shell_output=subprocess.check_output(splitted)
    except subprocess.CalledProcessError:
        msg="error in the execution of "+str(command)
        #sys.stderr.write(msg)
        return msg

    lines=shell_output.split("\n")
    return lines

def who_to_text(who):
    rval = ""
    if who == BULL:
        rval = "bull"
    elif who == BEAR:
        rval = "bear"
    elif who == 0:
        rval = "sideway"
    else:
        template = "wrong who:{}"
        msg = template.format(who)
        raise Exception(msg)

    return rval

def sign(x):
    if x > 0:
        return 1
    elif x==0:
        return 0
    else:
        return -1

def pprint_json(x):
    return json.dumps(x, indent=4, sort_keys=True)

def expand_tilde(x):
    """A file name can start by tilde (~), meaning home. To make it
    possible, we replace a tilde at beginning of the string for the
    path of the home directory"""
    rval = x

    if x.find("~") == 0:
        home = os.path.expanduser("~")
        rval = os.path.join(home, x[2:])

    return rval

def get_home():
    home = os.path.expanduser("~")
    return home

def human_time():
    return human(time.time())

def split_port(x):
    host, port = x.split(':')
    return host, int(port)

def timedelta(x, lang="en"):
    """
    Accepts a time delta in seconds (global amount of seconds that some job
    lasted)

    Returns a string describing that time interval for humans.
    e.g. '9 days,15 h,43 mins,3 secs'
    """
    if lang != "en" and lang != "es" :
        t = "Wrong language {}."
        t += "Must be  en (english) or es (spanish)"
        msg = t.format(lang)
        raise Exception(msg)

    try:
        s = str(datetime.timedelta(seconds=x))

        s = s.replace("days","day")
        i = s.find("day")
        if i != -1:
            days = int(s[0:(i-1)])
            s = s[i+5:]  # text after 'day, '
        else:
            days = 0
        hours, minutes, seconds = s.split(":")
        hours = int(round(float(hours)))
        minutes = int(round(float(minutes)))
        seconds = int(round(float(seconds)))

        if days == 0:
            msg = ""
        else :
            msg = days_to_ymd(days)

        if hours !=0:
            t = "{}{} h,"
            msg = t.format(msg,hours)

        if minutes !=0:
            t = "{}{} mins,"
            msg = t.format(msg,minutes)

        t = "{}{} secs"
        msg = t.format(msg,seconds)
    except:
        msg = "unknown"

    if lang == "es":
        msg = msg.replace("day","dia")
    return msg

def days_to_ymd(x):
    days_per_month = 365/12.0  # we consider equal months of 30.41 days
    years = int (x / 365)
    remaining = x - years*365
    months = int (remaining / days_per_month)
    days = int(remaining - months * days_per_month)

    if years != 0:
        t = "{} years,"
        msg = t.format(years)
    else:
        msg = ""

    if months != 0:
        t = "{}{} months,"
        msg = t.format(msg, months)

    if days != 0:
        t = "{}{} days,"
        msg = t.format(msg, days)

    return msg

def iso_to_unix(s):
    dt=iso8601.parse_date(s)
    date=datetime_to_unix_time(dt)
    return date

def exec_order(order):
    order=order.split()
    try:
        shell_output=subprocess.check_output(order)
    except subprocess.CalledProcessError:
        msg="error in the execution of "+str(order)
        raise Exception(msg)

    output = shell_output
    string = output.split(b"\n")
    return string

def number_to_string_number(x):
    """
    Gets a number and returns a string containing the number,
    as expected by bitfinex v1 API
    """

    try:
        as_float = float(x)
    except ValueError:
        t = "Not a number: {}"
        msg = t.format(x)
        raise Exception(msg)

    rval = "{}".format(as_float)
    return rval

def string_to_bool(x):
    if x.lower() == "false":
        rval = False
    elif x.lower() == "true":
        rval = True
    else:
        msg = "String {} can not be evaluated to neither True nor False"
        raise Exception(msg)
    return rval


def dict_to_array(d):
    """
    Converts a dict to an array of subarrays where the first element of the
    subarray is the key and the second, the value.
    Useful to sort an array by its key
    """

    l = []
    for k in list(d.keys()):
        item = []
        item.append(k)
        item.append(d[k])
        l.append(item)

    return l

def my_sort(a,b):
    if a[1] < b[1]:
        r = 1
    elif  a[1] > b[1]:
        r = -1
    else:
        r = 0
    return r

def keys_sorted( dict):
    as_array = dict_to_array(dict)
    as_array.sort(my_sort)
    l = []
    for item in as_array:
        l.append(item[0])
    return l

def get_data_dir():
    variable_name = "MBOT_DATA_DIR"
    data_dir = os.getenv(variable_name)
    if data_dir == None:
        t = "{} environment variable not set"
        msg = t.format(variable_name)
        raise Exception(msg)
    return data_dir


def dict_has_keys(d, keys_to_check):
    """
    Checks if all keys are included in the dict d. It is ok if the dict has
    additional keys.
    """

    keys = list(d.keys())

    r = True
    for k in keys_to_check:
        if not k in keys:
            r = False
            msg = "Missing key "+k
            print(msg)
            break
    return r


def is_similar( right_example, candidate):
    """
    Checks if the second dictionary is similar to the first one.
    Where 'similar' means:
    - All the keys of right_example are included in candidate
      (if candidate has extra keys, is ok)
    - Values in candidate are the same data type than values in
      right_example

    If similar, returns "ok", else, a description of the problem
    """

    msg = ""

    if not isinstance(candidate, dict):
        msg = "Candidate is not a dict"
        return msg

    if not isinstance(right_example, dict):
        msg = "right_example is not a dict"
        return msg


    if dict_has_keys( candidate, list(right_example.keys()) ):
        pass
    else:
        t =  "Missing keys\nRight example:{}\nCandidate:{}\n"
        msg = t.format(right_example,candidate)

        return  msg

    for k in right_example:
        r1 = isinstance(right_example[k], float)
        r1 = r1 and isinstance(candidate[k], int)

        r2 = type(right_example[k]) == type(candidate[k])

        if not r1 and not r2:
            rval = False
            t = "Expecting {}, found {}\n"
            msg += t.format(type(right_example[k]), type(candidate[k]))

    if msg == "":
        msg = "ok"
    return msg




def per_to_factor(per):
    """
    Receives profit as percentage, returns it as factor.
    ie:  10 -> 1.1
    """
    return 1+per*0.01


def factor_to_per(factor):
    """
    Receives a factor, returns a percentage
    ie:  2 -> 100 (%)
    """
    return (factor - 1) * 100
