#!/usr/bin/env python3

from good_server_reponses import *

def dict_has_keys(d, keys_to_check):
    """
    Checks if all keys are included in the dict d. It is ok if the dict has
    additional keys.
    """

    keys = list(d.keys())

    r = True
    for k in keys_to_check:
        if not k in keys:
            r = False
            msg = "Missing key "+k
            print(msg)
            break
    return r

def compare_values_keys(candidate, right_example):
    """
    return "ok" if type values are equal or
    error msg in other case
    """
    for k in right_example:
        r1 = isinstance(right_example[k], float)
        r1 = r1 and isinstance(candidate[k], int)

        r2 = type(right_example[k]) == type(candidate[k])

        if not r1 and not r2:
            t = "Error in \"{}\" value: Expecting {}, found {}\n"
            msg = t.format(
                k, 
                type(right_example[k]), 
                type(candidate[k]))
            return msg
    return "ok"

def iter_over_dict_list(right_example, mylist):
    for elem in mylist:
        if isinstance(elem, dict):
            msg = is_similar(right_example[0], elem)
            if msg != "ok":
                return msg
    return "ok"

def check_values_dict(candidate, right_example):
    for key, value in candidate.items():
        if isinstance(value, dict):
            msg = is_similar(right_example[key], value)
            if msg != "ok":
                return msg
        if isinstance(value, list):
            msg = iter_over_dict_list(right_example[key], value)
            if msg != "ok":
                return msg
    return "ok"

def is_similar(right_example, candidate):
    """
    Checks if the second dictionary is similar to the first one.
    Where 'similar' means:
    - All the keys of right_example are included in candidate
      (if candidate has extra keys, is ok)
    - Values in candidate are the same data type than values in
      right_example

    If similar, returns "ok", else, a description of the problem
    """
    msg = ""

    if not isinstance(candidate, dict):
        msg = "Candidate is not a dict"
        return msg

    if not isinstance(right_example, dict):
        print(right_example)
        msg = "right_example is not a dict"
        return msg

    if dict_has_keys(candidate, list(right_example.keys())):
        pass
    else:
        t =  "Missing keys\nRight example:{}\nCandidate:{}\n"
        msg = t.format(right_example,candidate)
        return  msg

    cmp_values = compare_values_keys(candidate, right_example)
    if cmp_values != "ok":
        return cmp_values

    check_dicts = check_values_dict(candidate, right_example)
    if check_dicts != "ok":
        return check_dicts

    if msg == "":
        msg = "ok"
    return msg


def main():
    # reponse = {
    #     "ret_code": 0,
    #     "ret_msg": "OK",
    #     "ext_code": "",
    #     "ext_info": "",
    #     "result": {},
    #     "time_now": "1577444332.192859"
    # }
    # r_test = {
    #     "var_entero": 1,
    #     "var_dict": {
    #         "a":0,
    #         "b":2
    #     },
    #     "var_list": [
    #         {
    #             "esp": "jjj",
    #             "foo": True
    #         },
    #         {
    #             "esp": 0,
    #             "foo": True
    #         }
    #     ]
    # }
    obook = "ret_code": 0,
        "ret_msg": "OK",
        "ext_code": "",
        "ext_info": "",
        "result": [{"symbol":"BTCUSD","price":"9181","size":319471,"side":"Buy"}, {"symbol":"BTCUSD","price":"9181","size":319471,"side":"Buy"}],
        "time_now": "1577444332.192859"

    # print(is_similar(TIMESTAMP_SERVER_REPONSE, reponse))
    print(is_similar(TEST1, r_test))
    # print(check_server_reponse(TIMESTAMP_SERVER_REPONSE, reponse))
    return 0
if __name__ == "__main__":
    main()