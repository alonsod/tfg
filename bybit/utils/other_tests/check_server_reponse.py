#!/usr/bin/env python3

from good_server_reponses import *
from misc import is_similar

def check_result_value(desired_result, server_result):
    if isinstance(desired_result, list):
        desired_result = desired_result[0]
        for result in server_result:
            msg = check_server_reponse(desired_result, result)
            if msg != "ok":
                return msg
        return "ok"
    return check_server_reponse(desired_result, server_result)

def compare_keys(keys1, keys2):
    print(keys2)
    for key in keys1:
        print(key)
        if key not in keys2:
            msg = "Key \""+key+"\" not in server reponse: "+str(keys2)
            print(msg)
            return msg
    return "ok"

def compare_values(server_reponse, desired_reponse):
    for server_key, server_value in server_reponse.items():
        desired_value = desired_reponse[server_key]
        if type(desired_value) != type(server_value):
            msg = "Server key \""+server_key+"\", its value "+\
             str(server_value)+" should be "+str(type(desired_value))+\
             ", not "+str(type(server_value))
            print(msg)
            return msg
    return "ok"

def check_server_reponse(desired_reponse, server_reponse):
    msg = "ok"
    if not isinstance(desired_reponse, dict):
        msg = "Desired reponse is not a dict"
        print(msg)
        return msg

    if not isinstance(server_reponse, dict):
        msg = "Server reponse is not a dict"
        print(msg)
        return msg

    cmp_keys = compare_keys(desired_reponse.keys(), server_reponse.keys())
    if  cmp_keys != "ok":
        return cmp_keys

    cmp_values = compare_values(server_reponse, desired_reponse)
    if  cmp_values != "ok":
        return cmp_keys


    
    # for server_key, server_value in server_reponse.items():
    #     desired_value = desired_reponse[server_key]
    #     if type(desired_value) != type(server_value):
    #         msg = "Server key \""+server_key+"\", its value "+\
    #          str(server_value)+" should be "+str(type(desired_value))+\
    #          ", not "+str(type(server_value))
    #         print(msg)

    result_msg = check_result_value(
        desired_reponse["result"],
        server_reponse["result"]
        )
    if result != "ok":
        return result_msg
    return msg

def main():
    reponse = {
        "ret_code": 0,
        "ret_msg": "OK",
        "ext_code": "",
        "ext_info": "",
        "result": {},
        "time_now": "1577444332.192859"
    }
    print(is_similar(TIMESTAMP_SERVER_REPONSE, reponse))
    # print(check_server_reponse(TIMESTAMP_SERVER_REPONSE, reponse))
    return 0
if __name__ == "__main__":
    main()