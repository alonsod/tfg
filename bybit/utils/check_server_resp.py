#!/usr/bin/env python3

from misc import mprint
# from good_server_reponses import *

def dict_has_keys(d, keys_to_check):
    """
    Checks if all keys are included in the dict d. It is ok if the dict has
    additional keys.
    """

    keys = list(d.keys())

    r = True
    for k in keys_to_check:
        if not k in keys:
            r = False
            msg = "Missing key \""+str(k)+"\"\n"
            mprint(msg)
            break
    return r

def check_if_any_list_is_zero_len(len_example, len_candidate):
    """
    return True if one list is zero len and the other is not zero len
    return False if BOTH are zero len or BOTH are not zero len
    is like a XOR logical gate
    """
    return (len_example == 0 and len_candidate > 0) or \
        (len_example > 0 and len_candidate == 0)

def check_types_elements_list(right_example_list, candidate_list):
    """
    compares element by element all elements in both lists
    return "ok" if both lists are equal or error in other case
    nested for loops check if any candidate list element
    matches with any example element in list
    """
    len_example = len(right_example_list)
    len_candidate = len(candidate_list)
    
    if check_if_any_list_is_zero_len(len_example, len_candidate):
        msg = "Error lists: Expected any zero len list\n"
        msg += "Example: "+str(right_example_list)+"\n"
        msg += "Candidate: "+str(candidate_list)+"\n"
        return msg
    for elem in candidate_list:
        i = 1
        for right_elem in right_example_list:
            msg = types_comparator(right_elem, elem)
            if msg != "ok" and i > len(right_example_list):
                return msg
            if msg == "ok":
                break
            i += 1
    return "ok"

def compare_values_keys(candidate, right_example):
    """
    return "ok" if type values are equal or
    error msg in other case
    """
    for k in right_example:
        msg = types_comparator(right_example[k], candidate[k])
        if msg != "ok":
            t = "Error in key \"{}\": expected value: \"{}\", found \"{}\"\n"
            msg += t.format(str(k), str(right_example[k]), str(candidate[k]))
            return msg
    return "ok"

def types_comparator(elem1, elem2):
    """
    compares two variables' types
    if are equal and not list or dict, return ok
    if are equal and list, calls check_types_elements_list
    if are equal and dict, calls is_equal
    if not equal, returns error msg
    elem1 = example
    elem2 = candidate
    """
    r1 = isinstance(elem1, float)
    r1 = r1 and isinstance(elem2, int)

    r2 = type(elem1) == type(elem2)

    if not r1 and not r2:
        t = "Error in types_comparator: expected {}, found {}\n"
        msg = t.format(type(elem1), type(elem2))
        return msg
    # here we know values are same type
    if isinstance(elem1, dict):
        return is_similar(elem1, elem2)
    elif isinstance(elem1, list):
        return check_types_elements_list(elem1, elem2)
    return "ok"

def is_similar(right_example, candidate):
    """
    Checks if the second dictionary is similar to the first one.
    Where 'similar' means:
    - All the keys of right_example are included in candidate
      (if candidate has extra keys, is ok)
    - Values in candidate are the same data type than values in
      right_example

    If similar, returns "ok", else, a description of the problem
    """
    msg = ""

    if not isinstance(candidate, dict):
        msg = "Candidate is not a dict"
        return msg

    if not isinstance(right_example, dict):
        msg = "right_example is not a dict"
        return msg

    if not dict_has_keys(candidate, list(right_example.keys())):
        t =  "Missing keys\nRight example:{}\nCandidate:{}\n"
        msg = t.format(right_example,candidate)
        return  msg

    cmp_values = compare_values_keys(candidate, right_example)
    if cmp_values != "ok":
        return cmp_values

    if msg == "":
        msg = "ok"
    return msg


# def main():
#     mytest = {
#         "var_entero": 1,
#         "var_dict": {
#             "a":0,
#             "b":2,
#         },
#         "var_list": [
#             {
#                 "esp": "jjj",
#                 "foo": True,
#                 "another":[
#                     {"a":False}
#                 ]
#             },
#         ]
#     }
#     obook = {   # bueno
#         "ret_code": 0,
#         "ret_msg": "OK",
#         "ext_code": "",
#         "ext_info": "",
#         "result": [{"symbol":"BTCUSD","price":"9181","size":4,"side":"Buy"}, {"symbol":"BTCUSD","price":"9181","size":319471,"side":"Buy"}],
#         "time_now": "1577444332.192859"
#         }

#     obook = {
#         "ret_code": 0,
#         "ret_msg": "OK",
#         "ext_code": "",
#         "ext_info": "",
#         "result": [{"symbol":"BTCUSD","price":"9181","size":4,"side":"Buy"}, {"symbol":"BTCUSD","price":"9181","size":319471,"side":"Buy"}],
#         "time_now": "1577444332.192859"
#         }
#     print(is_similar(TEST2, mytest))
#     # print(is_similar(TIMESTAMP_SERVER_REPONSE, reponse))
#     # print(is_similar(ORDERBOOK_SERVER_RESPONSE, obook2))
#     # print(check_server_reponse(TIMESTAMP_SERVER_REPONSE, reponse))
#     return 0
# if __name__ == "__main__":
#     main()